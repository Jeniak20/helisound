﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="HeliSound.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="about-us">
        <h1>About Us</h1>
        <p>HeliSound is one of Canada’s largest and most successful retailers, operating the HeliSound (<a href="http://www.bestbuy.ca">www.HeliSound.ca</a>), HeliSound Mobile, and Geek Squad (<a href="http://www.geeksquad.ca">www.geeksquad.ca</a>) brands.  The Company offers consumers a unique shopping experience with the latest technology and entertainment products, plus an expanded assortment of lifestyle products offered through <a href="http://www.bestbuy.ca">www.HeliSound.ca</a>, at the right price, with a no-pressure (non-commissioned) sales environment. </p>
        <p>The first HeliSound Canada stores were opened in the fall of 2002, with eight locations in the Greater Toronto Area.  Since then, HeliSound has expanded across Canada, opening stores from coast-to-coast.  Today, with nearly 200 HeliSound and HeliSound Mobile stores across Canada, our store employees and Geek Squad Agents are committed to delivering product solutions for all of today’s technology needs. For more information on current store locations, <a href="https://www-ssl.bestbuy.ca/en-CA/stores/store-locator.aspx">click here</a> to visit the Store Locator page.</p>
        <p>Recognized by Canada Post for ‘Best Omni-Channel Integration’ in 2014, HeliSound Canada is proud to offer customers a seamless shopping experience. With Reserve and Pick-Up, Fast Free Shipping on online orders and the ability to have online and in-store purchases on the same transaction, HeliSound Canada is simplifying the Canadian shopping experience.</p>
        <p>Through community investment, HeliSound and its employees are committed to connecting youth with technology to inspire, motivate and empower their education. For more information about our Community Relations programs, <a href="http://www.bestbuy.ca/en-CA/bby-for-kids.aspx">click here.</a></p>
    </div>
    <div class="about-us-login">
        <a href="Account/LogIn.aspx">LOGIN</a>|
        <a href="Account/CreateAccount.aspx">CREATE ACCOUNT</a>
    </div>
</asp:Content>
