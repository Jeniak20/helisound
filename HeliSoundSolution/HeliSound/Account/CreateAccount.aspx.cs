﻿using HeliSound.DataAccessLayer;
using HeliSound.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Account
{
    public partial class CreateAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            var message = ValidateForm();

            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                return;
            };

            try
            {
                // create model for user
                var user = new User()
                {
                    FirstName = txtFirstName.Text.Trim(),
                    LastName = txtLastName.Text.Trim(),
                    EmailAddress = txtEmail.Text.Trim(),
                    PhoneNumber = txtPhone.Text.Trim(),
                    Password = FormsAuthentication.HashPasswordForStoringInConfigFile(txtPassword1.Text, "SHA1"),
                    Status = true,                          // default status: Active
                    RoleId = 3,                             // default role: Customers
                    Role = null
                };

                // save user model to database. Get users Id
                var userId = UserProvider.UserAdd(user);
                if (userId == 0)
                {
                    DisplayMessage("Can not add user to the databse", true);
                    return;
                }

                // create models for secret answers
                var answer = new SecretAnswer()
                {
                    UserId = userId,
                    QuestionId = Convert.ToInt32(ddlQuestion.SelectedValue),
                    Answer = FormsAuthentication.HashPasswordForStoringInConfigFile(txtAnswer.Text.Trim().ToUpper(), "SHA1")   // Answer is case insensitive
                };

                // save secret answer model into database
                var answer1Id = UserProvider.SecretAnswerAdd(answer);
                if (answer1Id == 0)
                {
                    DisplayMessage("Can not add secret answer to the databse", true);
                    return;
                }

                // clear the form and show the message
                btnCancel_Click(sender, e);
                DisplayMessage("Profile created successfully. Go to Login page.", false);
            }
            catch (Exception ex)
            {
                DisplayMessage("Can not create profile. Error:" + ex.Message, true);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            DisplayMessage(string.Empty, false);
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtPassword1.Text = string.Empty;
            txtPassword2.Text = string.Empty;
            ddlQuestion.SelectedValue = "0";
            txtAnswer.Text = string.Empty;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Account/Login.aspx");
        }

        //-----------------------------------------------------------------------------------------

        private string ValidateForm()
        {
            // First Name
            if (txtFirstName.Text.Trim().Length < 1)
            {
                return "First Name is too short (required minimum 1 character).";
            }
            if (txtFirstName.Text.Trim().Length > 60)
            {
                return "First Name is too long (maximum 60 character).";
            }
            //Last Name
            if (txtLastName.Text.Trim().Length < 1)
            {
                return "Last Name is too short (required minimum 1 character).";
            }
            if (txtLastName.Text.Trim().Length > 60)
            {
                return "Last Name is too long (maximum 60 character).";
            }
            // Email
            if (txtEmail.Text.Trim().Length > 124)
            {
                return "Email address is too long (maximum 124 character).";
            }
            try
            {
                MailAddress m = new MailAddress(txtEmail.Text.Trim());
            }
            catch
            {
                return "Email format is wrong.";
            }
            // Phone
            var rgx = new Regex(@"^\(\d{3}\) \d{3}-\d{4}$"); //exactly "(XXX) XXX-XXXX"
            if (!rgx.IsMatch(txtPhone.Text.Trim()))
            {
                return "Wrong phone number format. Should match exactly (XXX) XXX-XXXX";
            }
            // Password
            if (txtPassword1.Text.Length < 3)
            {
                return "Password is too short (required minimum 3 characters).";
            }
            if (txtPassword1.Text.Length > 10)
            {
                return "Password is too long (maximum 10 characters).";
            }
            // Matching passwords
            if (txtPassword1.Text != txtPassword2.Text)
            {
                return "Passowrd should match Repeat password .";
            }
            // Secret question
            if (ddlQuestion.SelectedValue.Equals('0'))
            {
                return "Select Secret question.";
            }
            // Secret answer
            if (txtAnswer.Text.Trim().Length < 3)
            {
                return "Secret answer is too short (required minimum 3 characters).";
            }
            // Check if user with this email already exists 
            if (UserProvider.UserIdByEmailAddressGet(txtEmail.Text.Trim()) != 0)
            {
                return "Specified email already used by other user.";
            }
            return string.Empty;
        }

        private void DisplayMessage(string message, bool isError)
        {
            lblErrorMessage.Text = string.Empty;
            lblInfoMessage.Text = string.Empty;
            if (isError)
            {
                lblErrorMessage.Text = message;
            }
            else
            {
                lblInfoMessage.Text = message;
            }
        }
    }
}