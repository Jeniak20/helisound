﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Account/Account.Master" AutoEventWireup="true" CodeBehind="CreateAccount.aspx.cs" Inherits="HeliSound.Account.CreateAccount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="border:solid green 2px;">
        <div>
            <div class="page-title">
                CREATE USER PROFILE
            </div>
            <table class="login-layout">
                <tr>
                    <td>
                        <span class="field-label">First Name</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" CssClass="text-field"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Last Name</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLastName" runat="server" placeholder="Last Name" CssClass="text-field"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Email</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" placeholder="name@company.domane" CssClass="text-field"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Phone</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPhone" runat="server" placeholder="(XXX) XXX-XXXX" CssClass="text-field"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Password</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword1" runat="server" TextMode="Password" placeholder="Password" CssClass="text-field"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Repeat password</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" placeholder="Repeat Password" CssClass="text-field"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Secret question</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlQuestion" runat="server" DataSourceID="dsSecretQuestions" DataTextField="Question" DataValueField="Id" AppendDataBoundItems="True" CssClass="text-field">
                        <asp:ListItem Value="0" Text="Select question" />
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="dsSecretQuestions" runat="server" ConnectionString="<%$ ConnectionStrings:HeliSound %>" SelectCommand="SELECT [Id], [Question] FROM [SecretQuestions] ORDER BY [Id]">
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="field-label">Answer</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAnswer" runat="server" placeholder="Answer for question" CssClass="text-field"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblErrorMessage" runat="server" Text="" CssClass="error-message"></asp:Label>
                        <asp:Label ID="lblInfoMessage" runat="server" Text="" CssClass="info-message"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnCreate" runat="server" Text="CREATE PROFILE" OnClick="btnCreate_Click" CssClass="action-button"/>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" Text="CANCEL" OnClick="btnCancel_Click" CssClass="action-button"/>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="btnLogin" runat="server" Text="TO LOGIN PAGE" OnClick="btnLogin_Click" CssClass="action-button"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
