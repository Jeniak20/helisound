﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Account/Account.Master" AutoEventWireup="true" CodeBehind="LogIn.aspx.cs" Inherits="HeliSound.Account.LogIn" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="page-title">Login Page</div>
        <table class="login-layout">
            <tr>
                <td>
                    <span class="field-label">Email</span>
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="text-field"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="field-label">Password</span>
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="text-field"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Login" CssClass="action-button"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Label ID="lblErrorMessage" runat="server" Text="Wrong Email or Password." Font-Bold="True" ForeColor="#CC3300" ></asp:Label>
                </td>
            </tr>        
            <tr style="margin: 50px 0 0 0;">
                <td>&nbsp;</td>
                <td>
                    <p class="my-comment">Following 3 buttons will help you to login using credentials of the presetted 3 users with differen roles: Administrators, Shipping, Customers.</p>
                </td>
            </tr>             <tr style="margin: 50px 0 0 0;">
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btnLogInAsAdministrator" runat="server" Text="Prefill credentials for 'Administrators' role" OnClick="btnLogInAsAdministrator_Click" Width="478px" />
                </td>
            </tr>   
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btnLogInAsShipping" runat="server" Text="Prefill credentials for 'Shipping' role" OnClick="btnLogInAsShipping_Click" Width="478px" />
                </td>
            </tr>   
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btnLogInAsCustomer" runat="server" Text="Prefill credentials for 'Customers' role" OnClick="btnLogInAsCustomer_Click" Width="478px" />
                </td>
            </tr>   
        </table>
    </div>
</asp:Content>
