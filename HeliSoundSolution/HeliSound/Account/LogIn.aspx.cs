﻿using HeliSound.DataAccessLayer;
using HeliSound.Models;
using HeliSound.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Account
{
    public partial class LogIn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblErrorMessage.Visible = false;
            }
        }

        protected void btnLogInAsAdministrator_Click(object sender, EventArgs e)
        {
            txtEmail.Text = "admin.smith@helisound.com";
            txtPassword.Attributes.Add("Value", "password"); // Assigning new value to the "Text" property of the TextBox with "passwprd" TextMode does not work

        }

        protected void btnLogInAsShipping_Click(object sender, EventArgs e)
        {
            txtEmail.Text = "shipping.brown@helisound.com";
            txtPassword.Attributes.Add("Value", "password"); // Assigning new value to the "Text" property of the TextBox with "passwprd" TextMode does not work
        }

        protected void btnLogInAsCustomer_Click(object sender, EventArgs e)
        {
            txtEmail.Text = "customer.jones@gmail.com";
            txtPassword.Attributes.Add("Value", "password"); // Assigning new value to the "Text" property of the TextBox with "passwprd" TextMode does not work
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            var hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(txtPassword.Text, "SHA1");
            var user = UserProvider.UserIdentityCheck(txtEmail.Text.Trim(), hashPassword);

            if (user != null)
            {
                SaveUserDataToSession(user);
                AuthorizeUser(user);
            };
            lblErrorMessage.Visible = true;
        }

        //--------------------------------------------------------------------------------------------------------

        private void SaveUserDataToSession(User user)
        {
            Session.Add(Constants.SessionKeys.LOGGED_USER, user);
        }

        private void AuthorizeUser(User user)
        {
            // Initialize FormsAuthentication, for what it's worth
            FormsAuthentication.Initialize();

            // Create a new ticket used for authentication
            var ticket = new FormsAuthenticationTicket(
                1, // Ticket version
                user.EmailAddress, // Username associated with ticket
                DateTime.Now, // Date/time issued
                DateTime.Now.AddMinutes(20), // Date/time to expire
                true, // "true" for a persistent user cookie
                user.Role.RoleName, // User-data, in this case the roles
                FormsAuthentication.FormsCookiePath);// Path cookie valid for

            // Encrypt the cookie using the machine key for secure transport
            var hash = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(
               FormsAuthentication.FormsCookieName, // Name of auth cookie
               hash); // Hashed ticket

            // Set the cookie's expiration time to the tickets expiration time
            if (ticket.IsPersistent) cookie.Expires = ticket.Expiration;

            // Add the cookie to the list for outgoing response
            Response.Cookies.Add(cookie);

            // Redirect to default page (depending on role) 
            string returnUrl = "/";
            switch (user.Role.RoleName)
            {
                case "Administrators":
                    returnUrl = "~/Administration/UserMaintenance.aspx";
                    break;
                case "Shipping":
                    returnUrl = "~/Shipping/Shipping.aspx";
                    break;
                case "Customers":
                    returnUrl = "~/Customers/OrderProducts.aspx";
                    break;
            };

            // Don't call FormsAuthentication.RedirectFromLoginPage since it
            // could
            // replace the authentication ticket (cookie) we just added
            Response.Redirect(returnUrl);
        }

    }
}