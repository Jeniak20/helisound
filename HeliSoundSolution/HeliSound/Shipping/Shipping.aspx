﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shipping/Shipping.Master" AutoEventWireup="true" CodeBehind="Shipping.aspx.cs" Inherits="HeliSound.Shipping.Shipping1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div  class="page-title">Shipping Page</div>
        <asp:RadioButtonList ID="rbShippingStatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rbShippingStatus_SelectedIndexChanged" CssClass="grid-layout radio-button-label">
            <asp:ListItem Selected="True" Value="0">Show Pending Orders</asp:ListItem>
            <asp:ListItem Value="1">Show Shipped Orders</asp:ListItem>
        </asp:RadioButtonList>
        <asp:GridView ID="grdInvoices" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Id" DataSourceID="dsInvoices" ForeColor="#333333" GridLines="None" OnRowEditing="grdInvoices_RowEditing" CssClass="grid-layout">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Invoice Number" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="DateCreation" HeaderText="Date Ordered" ReadOnly="True" SortExpression="DateCreation" />
                <asp:TemplateField HeaderText="Shipping Status" SortExpression="ShippingStatus">
                    <EditItemTemplate>
                        <asp:Label runat="server" Text="Shipped" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddInvoiceShippingStatus" runat="server" SelectedValue='<%# Bind("ShippingStatus") %>' Enabled="false">
                            <asp:ListItem Text="In progress" Value="0" />
                            <asp:ListItem Text="Shipped" Value="1" />
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Shipping Date" SortExpression="ShippingDate">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# DateTime.Now.ToString("yyyy-MM-dd h:mm tt") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# (Eval("ShippingDate").ToString().Equals(string.Empty)) ? "Not shipped" : Eval("ShippingDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField EditText="Ship this order" ShowEditButton="True" UpdateText="Confirm shipping" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />

        </asp:GridView>
        <asp:SqlDataSource ID="dsInvoices" runat="server" 
            ConflictDetection="CompareAllValues" 
            ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
            
            SelectCommand="SELECT [Id], [DateCreation], [ShippingStatus], [ShippingDate] FROM [Invoices] WHERE ([ShippingStatus] = @ShippingStatus) ORDER BY [Id]" 
            UpdateCommand=
                "UPDATE [Invoices] 
                SET [ShippingUserId] = @ShippingUserId, [ShippingStatus] = 1, [ShippingDate] = GETDATE() 
                WHERE [Id] = @original_Id

                SELECT @InvoiceId = @original_Id;"
            OldValuesParameterFormatString="original_{0}" OnUpdated="dsInvoices_Updated" >
            <SelectParameters>
                <asp:ControlParameter ControlID="rbShippingStatus" DefaultValue="0" Name="ShippingStatus" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="InvoiceId" Type="Int32" Direction="Output"/>
                <asp:Parameter Name="original_Id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <div class="grid-layout">
            <asp:Label ID="lblErrorMessage" runat="server" Text="" CssClass="error-message"></asp:Label>
            <asp:Label ID="lblInfoMessage" runat="server" Text="" CssClass="info-message"></asp:Label>
        </div>
    </div>
</asp:Content>
