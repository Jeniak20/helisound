﻿using HeliSound.Common;
using HeliSound.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Shipping
{
    public partial class Shipping1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (dsInvoices.UpdateParameters["ShippingUserId"] != null)
            {
                dsInvoices.UpdateParameters.Remove(dsInvoices.SelectParameters["ShippingUserId"]);
            }
            dsInvoices.UpdateParameters.Add("ShippingUserId", ((User)Session[Constants.SessionKeys.LOGGED_USER]).Id.ToString());
        }

        protected void rbShippingStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            // column 4 is CommandField. This index may change if GridView layout chaned.
            grdInvoices.Columns[4].Visible = rbShippingStatus.SelectedValue.Equals("0");
            DisplayMessage(string.Empty, false);
        }
        
        protected void dsInvoices_Updated(object sender, SqlDataSourceStatusEventArgs e)
        {
            DisplayMessage("Order with Invoice Number [" + e.Command.Parameters["@InvoiceId"].Value.ToString() + "] successfully shipped", false);            
            grdInvoices.DataBind();
        }

        protected void grdInvoices_RowEditing(object sender, GridViewEditEventArgs e)
        {
            DisplayMessage(string.Empty, false);
        }

        //-------------------------------------------------------------------------------

        private void DisplayMessage(string message, bool isError)
        {
            lblErrorMessage.Text = string.Empty;
            lblInfoMessage.Text = string.Empty;
            if (isError)
            {
                lblErrorMessage.Text = message;
            }
            else
            {
                lblInfoMessage.Text = message;
            }
        }
    }
}