﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HeliSound.Models
{
    public class Generic
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsEmptyOption { get; set; }
    }
}