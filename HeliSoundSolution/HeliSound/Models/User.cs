﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HeliSound.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public bool Status { get; set; }
        public Role Role { get; set; }
    }
}