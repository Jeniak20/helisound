﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HeliSound.Models
{
    public class SecretAnswer
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int QuestionId { get; set; }
        public string Answer { get; set; }

    }
}