﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/Customers.Master" AutoEventWireup="true" CodeBehind="OrderProducts.aspx.cs" Inherits="HeliSound.Customers.OrderProducts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>      
        <div class="page-title">
            Order Product
        </div>        
        <table class="order-product-controls">
            <tr>
                <td>
                    <span class="field-label">Select supplier</span>
                </td>
                <td></td>
                <td>
                    <span class="field-label">Select category</span>
                </td>
            </tr>
            <tr>
                <td>                    
                    <asp:DropDownList ID="ddSuppliers" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="dsSuppliers" DataTextField="CompanyName" DataValueField="Id" OnSelectedIndexChanged="ddSuppliers_SelectedIndexChanged" CssClass="text-field">
                        <asp:ListItem Selected="True" Text="Choose supplier" Value="0" />
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsSuppliers" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
                        SelectCommand="SELECT [Id], [CompanyName] FROM [Suppliers] WHERE ([Status] = 1) ORDER BY [CompanyAddress]">
                    </asp:SqlDataSource>
                </td>
                <td></td>
                <td>                    
                    <asp:DropDownList ID="ddCategories" runat="server" AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="dsCategories" DataTextField="CategoryDescription" DataValueField="Id" OnSelectedIndexChanged="ddCategories_SelectedIndexChanged" CssClass="text-field">
                        <asp:ListItem Selected="True" Text="Choose category" Value="0" />
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsCategories" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
                        SelectCommand="SELECT [Id], [CategoryDescription] FROM [Categories] WHERE ([Status] = 1) ORDER BY [CategoryDescription]">
                    </asp:SqlDataSource>
                </td>
            </tr>
        </table>
        <asp:panel ID="pnlProducts" runat="server" Visible="false">
            <h3 class="grid-layout">List of available products</h3>
            <p class="grid-layout my-comment">According to assignment requirement for the <b>Ordering Products</b> page: "For this assignment assume only 1 item can be placed on an order.". Also, there is not requirements about specifying quantity for the product. This will assume that quantity is always 1. However on the administrator's page <b>Orders</b> you can change quantity and price as well.</p>
            <asp:GridView ID="grdProducts" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="dsProducts" GridLines="None" OnSelectedIndexChanged="grdProducts_SelectedIndexChanged" CssClass="order-product-grid">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="False" />
                    <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
                    <asp:BoundField DataField="SupplierId" HeaderText="SupplierId" SortExpression="SupplierId" Visible="False" />
                    <asp:BoundField DataField="CategoryId" HeaderText="CategoryId" SortExpression="CategoryId" Visible="False" />
                    <asp:TemplateField HeaderText="Price" SortExpression="RetailPrice">
                        <ItemTemplate>
                            <asp:Label ID="lblBeforeTax" runat="server" Text='<%# Bind("RetailPrice", "{0:c}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CheckBoxField DataField="Status" HeaderText="Status" SortExpression="Status" Text="Active" Visible="False" />
                    <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="Tax">
                        <ItemTemplate>
                            <asp:Label ID="lblTax" runat="server" Text='<%# string.Format("{0:c}", (Convert.ToDecimal(Eval("RetailPrice").ToString())*0.13M)) %>' DataFormatString="{0:c}"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total">
                        <ItemTemplate>
                            <asp:Label ID="lblTotal" runat="server" Text='<%# string.Format("{0:c}", (Convert.ToDecimal(Eval("RetailPrice").ToString())*1.13M)) %>' DataFormatString="{0:c}"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField SelectText="Order Product" ShowSelectButton="True" />
                </Columns>
                <EditRowStyle BackColor="#7C6F57" />
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <asp:SqlDataSource ID="dsProducts" runat="server"                 
                SelectCommand=
                    "SELECT [Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status] 
                    FROM [Products] 
                    WHERE (([SupplierId] = @SupplierId) AND ([CategoryId] = @CategoryId) AND ([Status] = 1))
                    ORDER BY [ProductName]"
                ConnectionString="<%$ ConnectionStrings:HeliSound %>" >
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddSuppliers" DefaultValue="0" Name="SupplierId" PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="ddCategories" DefaultValue="0" Name="CategoryId" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>

            <asp:Panel ID="pnlInvoice" runat="server" Visible="false">
                <h3 class="grid-layout">Order product</h3>
                <asp:DetailsView ID="dtlInvoice" runat="server" AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="dsInvoice"  GridLines="None" DefaultMode="Insert" OnItemInserting="dtlInvoice_ItemInserting" OnModeChanging="dtlInvoice_ModeChanging" CssClass="order-product-details">
                    <AlternatingRowStyle BackColor="White" />
                    <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                    <EditRowStyle BackColor="#7C6F57" />
                    <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
                    <Fields>
                        <asp:TemplateField HeaderText="CHARGES">
                            <InsertItemTemplate>
                                <hr />
                            </InsertItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BeforeTax" SortExpression="BeforeTax">
                            <InsertItemTemplate>
                                <asp:Label ID="lblInvoiceBeforeTax" runat="server" Text='<%# string.Format("{0:c}", BeforeTax) %>'></asp:Label>
                            </InsertItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tax" SortExpression="Tax">
                            <InsertItemTemplate>
                                <asp:Label ID="lblInvoiceTax" runat="server" Text='<%# string.Format("{0:c}", Tax) %>'></asp:Label>
                            </InsertItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total" SortExpression="Total">
                            <InsertItemTemplate>
                                <asp:Label ID="lblInvoiceTotal" runat="server" Text='<%# string.Format("{0:c}", Total) %>'></asp:Label>
                            </InsertItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" InsertVisible="False" ReadOnly="True" Visible="False" />
                        <asp:BoundField DataField="DateCreation" HeaderText="DateCreation" SortExpression="DateCreation" Visible="False" />
                        <asp:BoundField DataField="UserId" HeaderText="UserId" SortExpression="UserId" Visible="False" />
                        <asp:TemplateField HeaderText="BILLING METHOD">
                            <InsertItemTemplate>
                                <hr />
                            </InsertItemTemplate>                        
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Card Type" SortExpression="CardTypeId">
                            <InsertItemTemplate>
                                <asp:DropDownList ID="ddInvoiceCardTypeId" runat="server" SelectedValue='<%# Bind("CardTypeId") %>'>
                                    <asp:ListItem Selected="True" Text="Choose card type" Value="0" />
                                    <asp:ListItem Text="Visa" Value="1" />
                                    <asp:ListItem Text="MasterCard" Value="2" />
                                    <asp:ListItem Text="American Express" Value="3" />
                                </asp:DropDownList>
                            </InsertItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CardHolderName" HeaderText="Card Holder Name" SortExpression="CardHolderName" />
                        <asp:BoundField DataField="CardNumber" HeaderText="Card Number" SortExpression="CardNumber" />
                        <asp:BoundField DataField="Card3Secutity" HeaderText="Card Secutity Code" SortExpression="Card3Secutity" />
                        <asp:TemplateField HeaderText="Card Expiration Month" SortExpression="CardExpirationMonth">
                            <InsertItemTemplate>
                                <asp:DropDownList ID="ddInvoiceCardExpirationMonth" runat="server" SelectedValue='<%# Bind("CardExpirationMonth") %>'>
                                    <asp:ListItem Selected="True" Text="Choose month" Value="0" />
                                    <asp:ListItem Text="January" Value="1" />
                                    <asp:ListItem Text="February" Value="2" />
                                    <asp:ListItem Text="March" Value="3" />
                                    <asp:ListItem Text="April" Value="4" />
                                    <asp:ListItem Text="May" Value="5" />
                                    <asp:ListItem Text="June" Value="6" />
                                    <asp:ListItem Text="July" Value="7" />
                                    <asp:ListItem Text="August" Value="8" />
                                    <asp:ListItem Text="September" Value="9" />
                                    <asp:ListItem Text="October" Value="10" />
                                    <asp:ListItem Text="November" Value="11" />
                                    <asp:ListItem Text="December" Value="12" />
                                </asp:DropDownList>
                            </InsertItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Card Expiration Year" SortExpression="CardExpirationYear">
                            <InsertItemTemplate>
                                <asp:DropDownList ID="ddInvoiceCardExpirationYear" runat="server" SelectedValue='<%# Bind("CardExpirationYear") %>'>
                                    <asp:ListItem Selected="True" Text="Choose year" Value="0" />
                                    <asp:ListItem Text="2017" Value="2017" />
                                    <asp:ListItem Text="2018" Value="2018" />
                                    <asp:ListItem Text="2019" Value="2019" />
                                    <asp:ListItem Text="2020" Value="2020" />
                                    <asp:ListItem Text="2021" Value="2021" />
                                    <asp:ListItem Text="2022" Value="2022" />
                                    <asp:ListItem Text="2023" Value="2023" />
                                    <asp:ListItem Text="2024" Value="2024" />
                                    <asp:ListItem Text="2025" Value="2025" />
                                    <asp:ListItem Text="2026" Value="2026" />
                                </asp:DropDownList>
                            </InsertItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DELIVERY ADDRESS">
                            <InsertItemTemplate>
                                <hr />
                            </InsertItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AddressHouseNumber" HeaderText="House Number" SortExpression="AddressHouseNumber" />
                        <asp:BoundField DataField="AddressStreetName" HeaderText="Street" SortExpression="AddressStreetName" />
                        <asp:TemplateField HeaderText="Street Designation" SortExpression="AddressStreetDesignationId">
                            <InsertItemTemplate>
                                <asp:DropDownList ID="ddInvoiceAddressStreetDesignationId" runat="server" SelectedValue='<%# Bind("AddressStreetDesignationId") %>'>
                                    <asp:ListItem Selected="True" Text="Choose street designation" Value="0" />
                                    <asp:ListItem Text="Street" Value="1" />
                                    <asp:ListItem Text="Avenue" Value="2" />
                                    <asp:ListItem Text="Court" Value="3" />
                                    <asp:ListItem Text="Circle" Value="4" />
                                    <asp:ListItem Text="Drive" Value="5" />
                                </asp:DropDownList>
                            </InsertItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AddressAppartmentNumber" HeaderText="Appartment (optional)" SortExpression="AddressAppartmentNumber" />
                        <asp:BoundField DataField="AddressCity" HeaderText="City" SortExpression="AddressCity" />
                        <asp:TemplateField HeaderText="Province" SortExpression="AddressProvince">
                            <InsertItemTemplate>
                                <asp:DropDownList ID="ddInvoiceAddressProvince" runat="server" SelectedValue='<%# Bind("AddressProvince") %>'>
                                    <asp:ListItem Selected="True" Text="Choose province" Value="0" />
                                    <asp:ListItem Text="Alberta" Value="Alberta" />
                                    <asp:ListItem Text="British Columbia" Value="British Columbia" />
                                    <asp:ListItem Text="Manitoba" Value="Manitoba" />
                                    <asp:ListItem Text="New Brunswick" Value="New Brunswick" />
                                    <asp:ListItem Text="Newfoundland and Labrador" Value="Newfoundland and Labrador" />
                                    <asp:ListItem Text="Nova Scotia" Value="Nova Scotia" />
                                    <asp:ListItem Text="Ontario" Value="Ontario" />
                                    <asp:ListItem Text="Prince Edward Island" Value="Prince Edward Island" />
                                    <asp:ListItem Text="Quebec" Value="Quebec" />
                                    <asp:ListItem Text="Saskatchewan" Value="Saskatchewan" />
                                </asp:DropDownList>
                            </InsertItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AddressPostalCode" HeaderText="Postal Code" SortExpression="AddressPostalCode" />
                        <asp:BoundField DataField="ShippingUserId" HeaderText="ShippingUserId" SortExpression="ShippingUserId" Visible="False" />
                        <asp:BoundField DataField="ShippingStatus" HeaderText="ShippingStatus" SortExpression="ShippingStatus" Visible="False" />
                        <asp:BoundField DataField="ShippingDate" HeaderText="ShippingDate" SortExpression="ShippingDate" Visible="False" />
                        <asp:CommandField ShowInsertButton="True" InsertText="Place order" />
                    </Fields>
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#E3EAEB" />
                </asp:DetailsView>
                <asp:SqlDataSource ID="dsInvoice" runat="server" 
                    ConflictDetection="CompareAllValues" 
                    ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
                    InsertCommand=
                        "INSERT INTO [Invoices] 
                        ([UserId], [CardTypeId], [CardHolderName], [CardNumber], [Card3Secutity], [CardExpirationMonth], [CardExpirationYear], [AddressHouseNumber], [AddressStreetName], [AddressStreetDesignationId], [AddressAppartmentNumber], [AddressCity], [AddressProvince], [AddressPostalCode], [BeforeTax], [Tax], [Total]) 
                        VALUES (@UserId, @CardTypeId, @CardHolderName, @CardNumber, @Card3Secutity, @CardExpirationMonth, @CardExpirationYear, @AddressHouseNumber, @AddressStreetName, @AddressStreetDesignationId, @AddressAppartmentNumber, @AddressCity, @AddressProvince, @AddressPostalCode, @BeforeTax, @Tax, @Total);
                        
                        SELECT @InvoiceId = SCOPE_IDENTITY();

                        INSERT INTO [InvoiceItems] 
                        ([InvoiceId], [ProductId], [Quantity], [RetailPrice], [Status]) 
                        VALUES (@InvoiceId, @ProductId, 1, @BeforeTax, 1); " 
                    OldValuesParameterFormatString="original_{0}" 
                    OnInserted="dsInvoice_Inserted" >
                    <InsertParameters>
                        <asp:Parameter Name="UserId" Type="Int32" />
                        <asp:Parameter Name="CardTypeId" Type="Int32" />
                        <asp:Parameter Name="CardHolderName" Type="String" />
                        <asp:Parameter Name="CardNumber" Type="String" />
                        <asp:Parameter Name="Card3Secutity" Type="String" />
                        <asp:Parameter Name="CardExpirationMonth" Type="Int32" />
                        <asp:Parameter Name="CardExpirationYear" Type="Int32" />
                        <asp:Parameter Name="AddressHouseNumber" Type="String" />
                        <asp:Parameter Name="AddressStreetName" Type="String" />
                        <asp:Parameter Name="AddressStreetDesignationId" Type="Int32" />
                        <asp:Parameter Name="AddressAppartmentNumber" Type="String" />
                        <asp:Parameter Name="AddressCity" Type="String" />
                        <asp:Parameter Name="AddressProvince" Type="String" />
                        <asp:Parameter Name="AddressPostalCode" Type="String" />
                        <asp:Parameter Name="BeforeTax" Type="Decimal" />
                        <asp:Parameter Name="Tax" Type="Decimal" />
                        <asp:Parameter Name="Total" Type="Decimal" />
                        <asp:Parameter Name="InvoiceId" Type="Int32" Direction="Output"/>
                        <asp:Parameter Name="ProductId" Type="Int32" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </asp:Panel>
        </asp:panel>

        <div class="grid-layout">
            <asp:Label ID="lblErrorMessage" runat="server" Text="" CssClass="error-message"></asp:Label>
            <asp:Label ID="lblInfoMessage" runat="server" Text="" CssClass="info-message"></asp:Label>
        </div>
    </div>
</asp:Content>
