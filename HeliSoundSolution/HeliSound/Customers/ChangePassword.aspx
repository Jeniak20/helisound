﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/Customers.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="HeliSound.Customers.ChangePassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div>

            <div class="page-title">
                CHANGE PASSWORD
            </div>

            <asp:Panel ID="pnlEmail" runat="server">
                <table class="grid-layout">
                    <tr>
                        <td>
                            <span class="field-label">Enter your email</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" placeholder="name@company.domain" CssClass="text-field"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnNext" runat="server" Text="NEXT" OnClick="btnNext_Click" CssClass="action-button"/>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel ID="pnlQuestion" runat="server" Visible="False">
                <table class="grid-layout">
                    <tr>
                        <td>
                            <span class="field-label">Question</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtQuestion" runat="server" Text="" CssClass="text-field" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="field-label">Your answer</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAnswer" runat="server" placeholder="Answer" CssClass="text-field"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>                            
                        </td>
                        <td>
                            <asp:Button ID="btnAnswer" runat="server" Text="ANSWER" OnClick="btnAnswer_Click" CssClass="action-button"/>
                        </td>
                    </tr>
                </table>   
            </asp:Panel>

            <asp:Panel ID="pnlPassword" runat="server" Visible="False">
                <table class="grid-layout">
                    <tr>
                        <td>
                        </td>
                        <td>
                            <span class="field-label">You successfully answered secret question</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="field-label">New password</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassword1" runat="server" TextMode="Password" CssClass="text-field"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="field-label">Repeat new password</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" CssClass="text-field"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnResetPassword" runat="server" Text="RESET PASSWORD" OnClick="btnResetPassword_Click" CssClass="action-button"/>
                        </td>
                    </tr>
                </table>                
            </asp:Panel>

            <table class="grid-layout">
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Label ID="lblErrorMessage" runat="server" Text="" CssClass="error-message"></asp:Label>
                        <asp:Label ID="lblInfoMessage" runat="server" Text="" CssClass="info-message"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnLogin" runat="server" Text="TO LOGIN PAGE" OnClick="btnLogin_Click" CssClass="action-button"/>
                    </td>
                </tr>
            </table>

        </div>
    </div>
</asp:Content>
