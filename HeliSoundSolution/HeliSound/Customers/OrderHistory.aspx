﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/Customers.Master" AutoEventWireup="true" CodeBehind="OrderHistory.aspx.cs" Inherits="HeliSound.Customers.OrderHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="page-title">
            Order History
        </div>
        <div class="grid-layout">
            <asp:Button ID="btnUpdate" runat="server" Text="Refresh list" OnClick="btnUpdate_Click" CssClass="action-button" />
        </div>
        <asp:GridView ID="grdInvoices" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Id" DataSourceID="dsInvoices" ForeColor="#333333" GridLines="None" EmptyDataText="There is not shipped orders." CssClass="grid-layout grid-order-history">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Invoice Number" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="DateCreation" HeaderText="Date Ordered" SortExpression="DateCreation" />
                <asp:BoundField DataField="ShippingDate" HeaderText="Shipping Date" SortExpression="ShippingDate" />
                <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" DataFormatString="{0:c}" />                
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsInvoices" runat="server"             
            SelectCommand=
                "SELECT [Id], [DateCreation], [Total], [ShippingDate] FROM [Invoices] 
                WHERE (([ShippingStatus] = @ShippingStatus) AND ([UserId] = @UserId)) ORDER BY [Id]"
            ConnectionString="<%$ ConnectionStrings:HeliSound %>" >
            <SelectParameters>
                <asp:Parameter DefaultValue="1" Name="ShippingStatus" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>
