﻿using HeliSound.Common;
using HeliSound.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Customers
{
    public partial class TrackOrder : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearchInvoice_Click(object sender, EventArgs e)
        {
            try
            {
                var invoiceId = Convert.ToInt32(txtInvoiceId.Text);
            }
            catch 
            {
                dsInvoice.SelectParameters["Id"].DefaultValue = string.Empty;
                DisplayMessage("Invalid Invoice Number format. Must be a positive number.", true);
                return;
            }
            if (dsInvoice.SelectParameters["UserId"] != null)
            {
                dsInvoice.SelectParameters.Remove(dsInvoice.SelectParameters["UserId"]);
            }
            dsInvoice.SelectParameters.Add("UserId", ((User)Session[Constants.SessionKeys.LOGGED_USER]).Id.ToString());
            dtlInvoice.DataBind();
        }

        protected void dtlInvoice_DataBound(object sender, EventArgs e)
        {
            pnlInvoice.Visible = dtlInvoice.SelectedValue != null;
            if (pnlInvoice.Visible)
            {
                DisplayMessage(string.Empty, false);
            }
            else
            {
                DisplayMessage("Can not find Invoice by your request.", true);
            }
        }

        //-------------------------------------------------------------------------------

        private void DisplayMessage(string message, bool isError)
        {
            lblErrorMessage.Text = string.Empty;
            lblInfoMessage.Text = string.Empty;
            if (isError)
            {
                lblErrorMessage.Text = message;
            }
            else
            {
                lblInfoMessage.Text = message;
            }
        }
    }
}