﻿using HeliSound.Common;
using HeliSound.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Customers
{
    public partial class OrderHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (dsInvoices.SelectParameters["UserId"] != null)
            {
                dsInvoices.SelectParameters.Remove(dsInvoices.SelectParameters["UserId"]);
            }
            dsInvoices.SelectParameters.Add("UserId", ((User)Session[Constants.SessionKeys.LOGGED_USER]).Id.ToString());
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            grdInvoices.DataBind();
        }
    }
}