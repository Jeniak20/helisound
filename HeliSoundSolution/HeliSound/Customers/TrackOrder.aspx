﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customers/Customers.Master" AutoEventWireup="true" CodeBehind="TrackOrder.aspx.cs" Inherits="HeliSound.Customers.TrackOrder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="page-title">
            Track Order
        </div>
        <table class="track-order-controls">
            <tr>
                <td>
                    <span class="field-label">Enter Invoice Number</span>
                </td>
                <td>
                    <asp:TextBox ID="txtInvoiceId" runat="server" CssClass="text-field"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnSearchInvoice" runat="server" Text="Search" OnClick="btnSearchInvoice_Click" CssClass="action-button" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlInvoice" runat="server" Visible="false">
            <asp:DetailsView ID="dtlInvoice" runat="server" AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="dsInvoice" ForeColor="#333333" GridLines="None" OnDataBound="dtlInvoice_DataBound" CssClass="track-order-details">
                <AlternatingRowStyle BackColor="White" />
                <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                <EditRowStyle BackColor="#7C6F57" />
                <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
                <Fields>
                    <asp:BoundField DataField="Id" HeaderText="Invoice Number" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                    <asp:BoundField DataField="DateCreation" HeaderText="Date Ordered" SortExpression="DateCreation" />
                    <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" DataFormatString="{0:c}" />
                    <asp:TemplateField HeaderText="Shipping Status" SortExpression="ShippingStatus">
                        <ItemTemplate>
                                <asp:DropDownList ID="ddInvoiceShippingStatus" runat="server" SelectedValue='<%# Bind("ShippingStatus") %>' Enabled="false">
                                    <asp:ListItem Text="In progress" Value="0" />
                                    <asp:ListItem Text="Shipped" Value="1" />
                                </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Shipping Date" SortExpression="ShippingDate">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# (Eval("ShippingDate").ToString().Equals(string.Empty)) ? "Not shipped" : Eval("ShippingDate") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Fields>
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
            </asp:DetailsView>
            <asp:SqlDataSource ID="dsInvoice" runat="server" 
                ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
                SelectCommand=
                    "SELECT [Id], [DateCreation], [Total], CONVERT(INT, [ShippingStatus]) 'ShippingStatus', [ShippingDate] 
                    FROM [Invoices] 
                    WHERE (([UserId] = @UserId) AND (CONVERT(NVARCHAR(MAX) ,[Id]) = @Id))">
                <SelectParameters>
                    <asp:Parameter DefaultValue="0" DbType="Int32" Direction="Input" Name="UserId" Type="Int32" />
                    <asp:ControlParameter ControlID="txtInvoiceId" DefaultValue="0" Name="Id" PropertyName="Text" Type="string" />
                </SelectParameters>
            </asp:SqlDataSource>
        </asp:Panel>

        <div class="grid-layout">
            <asp:Label ID="lblErrorMessage" runat="server" Text="" CssClass="error-message"></asp:Label>
            <asp:Label ID="lblInfoMessage" runat="server" Text="" CssClass="info-message"></asp:Label>
        </div>
    </div>
</asp:Content>
