﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HeliSound.DataAccessLayer;
using System.Web.Security;
using HeliSound.Models;
using HeliSound.Common;

namespace HeliSound.Customers
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HideAllPanels();
            if (!IsPostBack)
            {
                // show email address panel at the begining
                pnlEmail.Visible = true;
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            if (!((User)Session[Constants.SessionKeys.LOGGED_USER]).EmailAddress.ToUpper().Equals(txtEmail.Text.Trim().ToUpper()))
            {
                DisplayMessage("Incorrect Email.", true);
                pnlEmail.Visible = true;
                return;
            }
            var question = UserProvider.SecretQuestionByUserEmailAddressGet(txtEmail.Text.Trim());
            if (string.IsNullOrWhiteSpace(question))
            {
                DisplayMessage("Can not find secret question.", true);
                pnlEmail.Visible = true;
            }
            else
            {
                DisplayMessage(string.Empty, false);
                txtQuestion.Text = question;
                pnlQuestion.Visible = true;
            };
        }

        protected void btnAnswer_Click(object sender, EventArgs e)
        {
            var answer = FormsAuthentication.HashPasswordForStoringInConfigFile(txtAnswer.Text.Trim().ToUpper(), "SHA1");
            var answerId = UserProvider.UserSecretAnswerCheck(txtEmail.Text.Trim(), answer);
            if (answerId == 0)
            {
                DisplayMessage("Wrong answer.", true);
                pnlQuestion.Visible = true;
            }
            else
            {
                DisplayMessage(string.Empty, false);
                pnlPassword.Visible = true;
            }
        }

        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            var message = ValidateForm();

            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                pnlPassword.Visible = true;
                return;
            };

            var password = FormsAuthentication.HashPasswordForStoringInConfigFile(txtPassword1.Text, "SHA1");
            var userId = UserProvider.UserPasswordUpdate(txtEmail.Text.Trim(), password);
            if (userId == 0)
            {
                DisplayMessage("Can not update your password.", true);
                pnlEmail.Visible = true;
                return;
            }
            FormsAuthentication.SignOut();
            DisplayMessage("Password updated successfully. Go to Login page.", false);
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {            
            Response.Redirect("~/Account/Login.aspx");
        }

        //------------------------------------------------------------------------------

        private void HideAllPanels()
        {
            pnlEmail.Visible = false;
            pnlQuestion.Visible = false;
            pnlPassword.Visible = false;
        }

        private void DisplayMessage(string nessage, bool isError)
        {
            lblErrorMessage.Text = string.Empty;
            lblInfoMessage.Text = string.Empty;
            if (isError)
            {
                lblErrorMessage.Text = nessage;
            }
            else
            {
                lblInfoMessage.Text = nessage;
            }
        }

        private string ValidateForm()
        {
            // Password
            if (txtPassword1.Text.Length < 3)
            {
                return "Password is too short (required minimum 3 characters).";
            }
            // Repeat password
            if (txtPassword2.Text.Length < 3)
            {
                return "Repeat password is too short (required minimum 3 characters).";
            }
            // Matching passwords
            if (txtPassword1.Text != txtPassword2.Text)
            {
                return "Passowrd should match Repeat password .";
            }
            return string.Empty;
        }
    }
}