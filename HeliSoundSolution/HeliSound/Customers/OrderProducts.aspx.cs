﻿using HeliSound.Common;
using HeliSound.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Customers
{
    public partial class OrderProducts : System.Web.UI.Page
    {
        public decimal BeforeTax {
            get {
                if (grdProducts.SelectedIndex != -1)
                {
                    // Use binded SqlDataSource of the Products GridView to get selected products data (RetailPrice in this case)
                    var dv = (DataView)dsProducts.Select(DataSourceSelectArguments.Empty);
                    return Convert.ToDecimal((dv).Table.Rows[grdProducts.SelectedIndex]["RetailPrice"].ToString());
                }
                else return 0M;
            }
        }
        public decimal Tax
        {
            get
            {
                return BeforeTax * 0.13M; 
            }
        }
        public decimal Total
        {
            get
            {
                return BeforeTax * 1.13M;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void grdProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdProducts.SelectedIndex != -1)
            {
                dtlInvoice.DataBind();
            }            
            UpdateUI();
        }

        protected void ddSuppliers_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdProducts.SelectedIndex = -1;
            DisplayMessage(string.Empty, false);
            UpdateUI();
        }

        protected void ddCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdProducts.SelectedIndex = -1;
            DisplayMessage(string.Empty, false);
            UpdateUI();
        }
        protected void dtlInvoice_ModeChanging(object sender, DetailsViewModeEventArgs e)
        {
            grdProducts.SelectedIndex = -1;
            UpdateUI();
        }

        protected void dtlInvoice_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            var message = ValidateForm(e.Values, 0);
            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                e.Cancel = true;
                return;
            };

        }
        protected void dsInvoice_Inserted(object sender, SqlDataSourceStatusEventArgs e)
        {
            var invoiceId = e.Command.Parameters["@InvoiceId"].Value;
            DisplayMessage("You order successfully placed. <br />" +
                "Your invoice number is " + invoiceId.ToString() + ". <br />" + 
                "Please keep it for track shipping status."
                , false);
            ddSuppliers.SelectedIndex = -1;
            ddCategories.SelectedIndex = -1;
            UpdateUI();
        }

        //-------------------------------------------------------------------------------

        private void DisplayMessage(string message, bool isError)
        {
            lblErrorMessage.Text = string.Empty;
            lblInfoMessage.Text = string.Empty;
            if (isError)
            {
                lblErrorMessage.Text = message;
            }
            else
            {
                lblInfoMessage.Text = message;
            }
        }

        private string ValidateForm(IOrderedDictionary values, int updatingId)
        {
            Regex rgx;

            // [UserId]                     INT NOT NULL,
            values["UserId"] = ((User)Session[Constants.SessionKeys.LOGGED_USER]).Id;
            // [CardTypeId] INT NOT NULL,
            if ((values["CardTypeId"] == null) || (values["CardTypeId"].ToString().Trim().Equals("0")))
            {
                return "Card Type is required.";
            }
            // [CardHolderName]             NVARCHAR(128)  NOT NULL,
            if ((values["CardHolderName"] == null) || (values["CardHolderName"].ToString().Length < 3))
            {
                return "Card Holder Name is too short (required minimum 3 characters).";
            }
            if (values["CardHolderName"].ToString().Length > 120)
            {
                return "Card Holder Name is too long (maximum 120 characters).";
            }
            // [CardNumber]                 NVARCHAR(32)   NOT NULL,
            //      the regular expression for REAL card numbers can be taken from here:
            //      https://stackoverflow.com/questions/9315647/regex-credit-card-number-tests
            //      I'll use trivial check for mask: "#### #### #### ####"
            rgx = new Regex("^[0-9][0-9][0-9][0-9] ?[0-9][0-9][0-9][0-9] ?[0-9][0-9][0-9][0-9] ?[0-9][0-9][0-9][0-9]$"); //Trivial card number (with optional spaces)
            if ((values["CardNumber"] == null) || !rgx.IsMatch(values["CardNumber"].ToString().Trim()))
            {
                return "Wrong Card Number format. Should match exactly 0000 0000 0000 0000 (spaces are optional).";
            }
            // [Card3Secutity]              NVARCHAR(4)    NOT NULL,
            if ((values["Card3Secutity"] == null) || (values["Card3Secutity"].ToString().Trim().Length != 3))
            {
                return "Card Secutity Code is required. Must be a 3 digit number.";
            }
            else
            {
                try
                {
                    var houseNumber = Convert.ToInt32(values["Card3Secutity"].ToString().Trim());
                    if (houseNumber < 1) return "Card Secutity Code must be greater that 0.";
                    if (houseNumber >= 1000) return "Card Secutity Code must be less that 1000.";
                }
                catch
                {
                    return "Card Secutity Code must be a 3 digit number.";
                }
            }
            // [CardExpirationMonth]        INT NOT NULL,
            if ((values["CardExpirationMonth"] == null) || (values["CardExpirationMonth"].ToString().Trim().Equals("0")))
            {
                return "Card Expiration Month is required.";
            }
            // [CardExpirationYear] INT NOT NULL,
            if ((values["CardExpirationYear"] == null) || (values["CardExpirationYear"].ToString().Trim().Equals("0")))
            {
                return "Card Expiration Year is required.";
            }
            // [AddressHouseNumber]         NVARCHAR(16)   NOT NULL,
            if ((values["AddressHouseNumber"] == null) || (string.IsNullOrWhiteSpace(values["AddressHouseNumber"].ToString())))
            {
                return "House Number is required.";
            }
            else
            {
                try
                {
                    var houseNumber = Convert.ToInt32(values["AddressHouseNumber"].ToString().Trim());
                    if (houseNumber < 1) return "House Number must be greater that 0.";
                    if (houseNumber >= 10000) return "House Number must be less that 10000.";
                }
                catch
                {
                    return "House Number must be a number.";
                }
            }
            // [AddressStreetName]          NVARCHAR(128)  NOT NULL,
            if ((values["AddressStreetName"] == null) || (values["AddressStreetName"].ToString().Length < 2))
            {
                return "Street Name is too short (required minimum 2 characters).";
            }
            if (values["AddressStreetName"].ToString().Length > 60)
            {
                return "Street Name is too long (maximum 60 characters).";
            }
            // [AddressStreetDesignationId] INT NOT NULL,
            if ((values["AddressStreetDesignationId"] == null) || (values["AddressStreetDesignationId"].ToString().Trim().Equals("0")))
            {
                return "Street Designation is required.";
            }
            // [AddressAppartmentNumber]    NVARCHAR(16)   NULL,
            if ((values["AddressAppartmentNumber"] == null) || (string.IsNullOrWhiteSpace(values["AddressAppartmentNumber"].ToString())))
            {
                // this field allows NULL
                values["AddressAppartmentNumber"] = null;
            }
            else
            {
                try
                {
                    var appNumber = Convert.ToInt32(values["AddressAppartmentNumber"].ToString().Trim());
                    if (appNumber < 1) return "Appartment Number must be greater that 0.";
                    if (appNumber >= 10000) return "Appartment Number must be less that 10000.";
                }
                catch
                {
                    return "Appartment Number must be a number.";
                }
            }
            // [AddressCity]                NVARCHAR(64)   NOT NULL,
            if ((values["AddressCity"] == null) || (values["AddressCity"].ToString().Length < 2))
            {
                return "City is too short (required minimum 2 characters).";
            }
            if (values["AddressCity"].ToString().Length > 60)
            {
                return "City is too long (maximum 60 characters).";
            }
            // [AddressProvince]            NVARCHAR(64)   NOT NULL,
            if ((values["AddressProvince"] == null) || (values["AddressProvince"].ToString().Trim().Equals("0")))
            {
                return "Province is required.";
            }
            // [AddressPostalCode]          NVARCHAR(8)    NOT NULL,
            //      I'm using suggestion from:
            //      https://stackoverflow.com/questions/1146202/canadian-postal-code-validation
            rgx = new Regex("^[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]$"); //Canada postal code (with optional space)
            if ((values["AddressPostalCode"] == null) || !rgx.IsMatch(values["AddressPostalCode"].ToString().Trim().ToUpper()))
            {
                return "Wrong Canada Postal Code format. Should match exactly A0A 0A0 (space is optional)." + Environment.NewLine +
                    "Canadian postal codes can't contain the letters D, F, I, O, Q, or U, and cannot start with W or Z";
            }
            // [BeforeTax]                  DECIMAL(18, 4) NOT NULL,
            values["BeforeTax"] = BeforeTax;
            // [Tax]                        DECIMAL(18, 4) NOT NULL,
            values["Tax"] = Tax;
            // [Total]                      DECIMAL(18, 4) NOT NULL,
            values["Total"] = Total;

            //---------------------------------------------------------------
            // [ProductId]   INT             NOT NULL,
            try
            {
                values["ProductId"] = Convert.ToInt32(grdProducts.SelectedValue.ToString());
            }
            catch
            {
                return "Wrong product ID. Critical error.";
            }
            

            return string.Empty;
        }

        private void UpdateUI()
        {
            pnlProducts.Visible = ((ddSuppliers.SelectedIndex != 0) && (ddCategories.SelectedIndex != 0));
            pnlInvoice.Visible = (pnlProducts.Visible && (grdProducts.SelectedIndex != -1));
        }
    }
}