﻿using HeliSound.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Administration
{
    public partial class Roles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void dtlRole_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {            
            Response.Redirect(Request.Url.ToString());
        }

        protected void dtlRole_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            var message = ValidateForm(e);

            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                e.Cancel = true;
                return;
            };
        }

        private void DisplayMessage(string message, bool isError)
        {
            lblErrorMessage.Text = string.Empty;
            lblInfoMessage.Text = string.Empty;
            if (isError)
            {
                lblErrorMessage.Text = message;
            }
            else
            {
                lblInfoMessage.Text = message;
            }
        }

        private string ValidateForm(DetailsViewInsertEventArgs e)
        {
            // Role
            if ((e.Values["Role"] == null) || (e.Values["Role"].ToString().Length < 3))
            {
                return "Role name is too short (required minimum 3 characters).";
            }
            if (e.Values["Role"].ToString().Length > 60)
            {
                return "Role name is too long (maximum 60 characters).";
            }
            // Description
            if ((e.Values["Description"] == null) || (e.Values["Description"].ToString().Length < 3))
            {
                return "Description is too short (required minimum 3 characters).";
            }
            if (e.Values["Description"].ToString().Length > 1020)
            {
                return "Description is too long (maximum 1020 characters).";
            }
            // Existing role
            if (UserProvider.RoleIdByRoleNameGet(e.Values["Role"].ToString().Trim()) != 0)
            {
                return "Specified role name already exists.";
            }
            return string.Empty;
        }
    }
}