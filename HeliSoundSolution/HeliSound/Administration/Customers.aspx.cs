﻿using HeliSound.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Administration
{
    public partial class Customers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void txtSearchByNames_TextChanged(object sender, EventArgs e)
        {
            btnSearchByNames_Click(sender, e);
        }
        protected void btnSearchByNames_Click(object sender, EventArgs e)
        {
            txtSearchById.Text = string.Empty;
            grdCustomers.DataSourceID = "dsCustomersByNames";
            grdCustomers.DataBind();
            grdCustomers.SelectedIndex = -1;
        }

        protected void txtSearchById_TextChanged(object sender, EventArgs e)
        {
            btnSearchById_Click(sender, e);
        }
        protected void btnSearchById_Click(object sender, EventArgs e)
        {
            txtSearchByNames.Text = string.Empty;
            try
            {
                var Id = Convert.ToInt32(txtSearchById.Text);
            }
            catch
            {
                DisplayMessage("Wrong customer ID", true);
                return;
            }
            grdCustomers.DataSourceID = "dsCustomersById";
            grdCustomers.DataBind();
            if (grdCustomers.Rows.Count > 0)
            {
                grdCustomers.SelectedIndex = 0;
            }
        }
        protected void dtlCustomer_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            grdCustomers.DataBind();
            grdCustomers.SelectedIndex = -1;
            UpdateUI();
        }

        protected void dtlCustomer_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
        {
            grdCustomers.DataBind();
            grdCustomers.SelectedIndex = -1;
            UpdateUI();
        }

        protected void dtlCustomer_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            var id = (int)grdCustomers.SelectedValue;
            var message = ValidateForm(e.NewValues, id);

            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                e.Cancel = true;
                return;
            };
        }

        protected void grdCustomers_DataBound(object sender, EventArgs e)
        {
            UpdateUI();
        }
        protected void grdCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtlCustomer.ChangeMode(DetailsViewMode.ReadOnly);
            UpdateUI();
        }
        protected void dtlCustomer_ModeChanged(object sender, EventArgs e)
        {
            DisplayMessage(string.Empty, false);
        }
        //-------------------------------------------------------------------------------

        private void DisplayMessage(string message, bool isError)
        {
            lblErrorMessage.Text = string.Empty;
            lblInfoMessage.Text = string.Empty;
            if (isError)
            {
                lblErrorMessage.Text = message;
            }
            else
            {
                lblInfoMessage.Text = message;
            }
        }

        private string ValidateForm(IOrderedDictionary values, int updatingId)
        {
            DisplayMessage(string.Empty, false);
            // [FirstName]    NVARCHAR (64)  NULL,
            if ((values["FirstName"] == null) || (values["FirstName"].ToString().Length < 1))
            {
                return "First name is too short (required minimum 1 characters).";
            }
            if (values["FirstName"].ToString().Length > 60)
            {
                return "First name is too long (maximum 60 characters).";
            }
            // [LastName]     NVARCHAR (64)  NULL,
            if ((values["LastName"] == null) || (values["LastName"].ToString().Length < 1))
            {
                return "Last name is too short (required minimum 1 characters).";
            }
            if (values["LastName"].ToString().Length > 60)
            {
                return "Last name is too long (maximum 60 characters).";
            }
            // [EmailAddress] NVARCHAR (128) NOT NULL,
            if ((values["EmailAddress"] == null) || (string.IsNullOrWhiteSpace(values["LastName"].ToString())))
            {
                return "Email address is required.";
            }
            if (values["EmailAddress"].ToString().Length > 124)
            {
                return "Email address is too long (maximum 124 characters).";
            }
            try
            {
                MailAddress m = new MailAddress(values["EmailAddress"].ToString().Trim());
            }
            catch
            {
                return "Email format is wrong.";
            }
            // [PhoneNumber]  NVARCHAR (64)  NULL,
            var rgx = new Regex(@"^\(\d{3}\) \d{3}-\d{4}$"); //exactly "(XXX) XXX-XXXX"
            if (!rgx.IsMatch(values["PhoneNumber"].ToString().Trim()))
            {
                return "Wrong phone number format. Should match exactly (XXX) XXX-XXXX";
            }

            // [Password]     NVARCHAR (128) NOT NULL,
            var password1 = ((TextBox)dtlCustomer.FindControl("txtUserPassword1")).Text;
            if ((updatingId == 0) || (!string.IsNullOrEmpty(password1))) // updatingId == 0 means inserting mode
            {
                if (password1.Length < 3)
                {
                    return "Password is too short (required minimum 3 characters).";
                }
                if (password1.Length > 10)
                {
                    return "Password is too long (maximum 10 characters).";
                }
                values["Password"] = FormsAuthentication.HashPasswordForStoringInConfigFile(password1, "SHA1");
            }
            // Matching password
            var password2 = ((TextBox)dtlCustomer.FindControl("txtUserPassword2")).Text;
            if (!password1.Equals(password2))
            {
                return "Passowrd should match Repeat password .";
            }
            // [RoleId]       INT            NOT NULL,
            //      not used here.

            // Existing user
            var existingId = UserProvider.UserIdByEmailAddressGet(values["EmailAddress"].ToString().Trim());
            if ((existingId > 0) && (updatingId != existingId))
            {
                return "Specified email already used by other user.";
            }
            return string.Empty;
        }

        private void UpdateUI()
        {
            pnlSearchResult.Visible = false;
            pnlCustomerEditDelete.Visible = false;
            DisplayMessage(string.Empty, false);
            if (grdCustomers.Rows.Count > 0)
            {
                pnlSearchResult.Visible = true;
                if (grdCustomers.SelectedIndex > -1)
                {
                    pnlCustomerEditDelete.Visible = true;
                }
            }
        }
    }
}