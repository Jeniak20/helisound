﻿using HeliSound.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Administration
{
    public partial class Suppliers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAddSupplier_Click(object sender, EventArgs e)
        {
            dtlSupplier.ChangeMode(DetailsViewMode.Insert);
        }


        protected void dtlSupplier_DataBound(object sender, EventArgs e)
        {
            btnAddSupplier.Visible = (dtlSupplier.DataItemCount == 0) && (dtlSupplier.CurrentMode != DetailsViewMode.Insert);
        }

        protected void dtlSupplier_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            Response.Redirect(Request.Url.ToString());
        }

        protected void dtlSupplier_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            Response.Redirect(Request.Url.ToString());
        }

        protected void dtlSupplier_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
        {
            Response.Redirect(Request.Url.ToString());
        }

        protected void dtlSupplier_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            var message = ValidateForm(e.Values, 0);
            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                e.Cancel = true;
                return;
            };
        }

        protected void dtlSupplier_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            var id = (int)grdSuppliers.SelectedValue;
            var message = ValidateForm(e.NewValues, id);
            
            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                e.Cancel = true;
                return;
            };
        }

        protected void dtlSupplier_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
        {
            var id = (int)grdSuppliers.SelectedValue;
            var count = StoreProvider.ProductIdBySupplierIdCount(id);
            if (count != 0)
            {
                // Invalid data
                DisplayMessage("Can not delete. There is " + count.ToString() + " products assosiated with this supplier.", true);
                e.Cancel = true;
                return;
            };
        }
        protected void dtlSupplier_ModeChanged(object sender, EventArgs e)
        {
            DisplayMessage(string.Empty, false);
        }
        //-------------------------------------------------------------------------------

        private void DisplayMessage(string message, bool isError)
        {
            lblErrorMessage.Text = string.Empty;
            lblInfoMessage.Text = string.Empty;
            if (isError)
            {
                lblErrorMessage.Text = message;
            }
            else
            {
                lblInfoMessage.Text = message;
            }
        }

        private string ValidateForm(IOrderedDictionary values, int updatingId)
        {
            // CompanyName
            if ((values["CompanyName"] == null) || (values["CompanyName"].ToString().Length < 3))
            {
                return "Company name is too short (required minimum 3 characters).";
            }
            if (values["CompanyName"].ToString().Length > 60)
            {
                return "Company name is too long (maximum 60 characters).";
            }
            // CompanyAddress
            if ((values["CompanyAddress"] == null) || (values["CompanyAddress"].ToString().Length < 3))
            {
                return "Company address is too short (required minimum 3 characters).";
            }
            if (values["CompanyAddress"].ToString().Length > 60)
            {
                return "Company addresse is too long (maximum 252 characters).";
            }
            // ContactPersonName
            if ((values["ContactPersonName"] == null) || (values["ContactPersonName"].ToString().Length < 3))
            {
                return "Contact person name is too short (required minimum 3 characters).";
            }
            if (values["ContactPersonName"].ToString().Length > 60)
            {
                return "Contact person name is too long (maximum 60 characters).";
            }
            // ContactPersonTelephoneNumber
            var rgx = new Regex(@"^\(\d{3}\) \d{3}-\d{4}$"); //exactly "(XXX) XXX-XXXX"
            if ((values["ContactPersonTelephoneNumber"] == null) || (!rgx.IsMatch(values["ContactPersonTelephoneNumber"].ToString().Trim())))
            {
                return "Wrong phone number format. Should match exactly (XXX) XXX-XXXX";
            }
            // Existing supplier
            var existingId = StoreProvider.SupplierIdByCompanyNameGet(values["CompanyName"].ToString().Trim());
            if ((existingId > 0) && (updatingId != existingId))
            {
                return "Specified company name already exists.";
            }
            return string.Empty;
        }


    }
}