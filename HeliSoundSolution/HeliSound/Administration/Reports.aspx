﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administration/Admin.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="HeliSound.Administration.Reports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>        
        <div class="page-title">
            Sales Reports
        </div>  
        <h3 class="grid-layout-800">Select report's date range</h3>
        <table class="grid-layout-800">
            <tr>
                <td>
                    <asp:RadioButtonList ID="rbReportDateRange" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rbReportDateRange_SelectedIndexChanged" CssClass="radio-group">
                        <asp:ListItem Selected="True" Value="Today">Today</asp:ListItem>
                        <asp:ListItem Value="Yesterday">Yesterday</asp:ListItem>
                        <asp:ListItem Value="ThisWeek">This week</asp:ListItem>
                        <asp:ListItem Value="LastWeek">Last week</asp:ListItem>
                        <asp:ListItem Value="SpecificDateRange">Specific date range</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <asp:Panel ID="pnlDateRange" runat="server" Visible="False">
                    <td>
                        <span class="field-label">From: </span>
                        <asp:Calendar ID="calFrom" runat="server" FirstDayOfWeek="Monday" OnSelectionChanged="calFrom_SelectionChanged"></asp:Calendar>
                    </td>
                    <td>
                        <span class="field-label">To: </span>
                        <asp:Calendar ID="calTo" runat="server" FirstDayOfWeek="Monday" OnSelectionChanged="calTo_SelectionChanged" ></asp:Calendar>
                    </td>
                </asp:Panel>        
            </tr>
        </table>

        <h3 class="grid-layout-800">Sales report</h3>
        <div class="grid-layout-800">
            <span class="field-label">Time range. From : </span>
            <asp:label ID="lblFrom" runat="server" text="" CssClass="field-label"></asp:label>
            <span class="field-label">   To :</span>
            <asp:label ID="lblTo" runat="server" text="" CssClass="field-label"></asp:label>
        </div>


        <asp:GridView ID="grdReport" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="dsReport"  GridLines="None" EmptyDataText="There is not invoices created in specified date range." OnDataBound="grdReport_DataBound" CssClass="report-grid">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Invoice Number" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                <asp:BoundField DataField="Total" DataFormatString="{0:c}" HeaderText="Total" SortExpression="Total" />
                <asp:TemplateField HeaderText="Shipping Status" SortExpression="ShippingStatus">
                    <ItemTemplate>
                        <asp:DropDownList ID="ddReportShippingStatus" runat="server" SelectedValue='<%# Bind("ShippingStatus") %>' Enabled="false">
                            <asp:ListItem Text="In progress" Value="0" />
                            <asp:ListItem Text="Shipped" Value="1" />
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsReport" runat="server"              
            SelectCommand=
                "SELECT i.[Id], i.[Total], i.[ShippingStatus], u.[FirstName], u.[LastName]
                FROM [Invoices] i
                INNER JOIN [Users] u
                ON i.[UserId] = u.[Id]
                WHERE i.[DateCreation] &gt;= @DateCreationFrom
                AND i.[DateCreation] &lt;= @DateCreationTo
                ORDER BY i.[Id]"
            ConnectionString="<%$ ConnectionStrings:HeliSound %>">
            <SelectParameters>
                <asp:Parameter Name="DateCreationFrom" />
                <asp:Parameter Name="DateCreationTo" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:DetailsView ID="dtlReport" runat="server" GridLines="None" AutoGenerateRows="False" DataSourceID="dsReportSum" CssClass="report-details">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
            <EditRowStyle BackColor="#7C6F57" />
            <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
            <Fields>
                <asp:BoundField DataField="OrderCount" HeaderText="Orders Count" ReadOnly="True" SortExpression="OrderCount" />
                <asp:BoundField DataField="OrderBeforeTaxSum" HeaderText="Before Tax Sum" SortExpression="OrderBeforeTaxSum" DataFormatString="{0:c}" ReadOnly="True" />
                <asp:BoundField DataField="OrderTaxSum" HeaderText="Tax Sum" SortExpression="OrderTaxSum" DataFormatString="{0:c}" ReadOnly="True" />
                <asp:BoundField DataField="OrderTotalSum" HeaderText="Total Sum" SortExpression="OrderTotalSum" DataFormatString="{0:c}" ReadOnly="True" />
            </Fields>
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="dsReportSum" runat="server" 
            SelectCommand=
                "SELECT COUNT([Id]) 'OrderCount', SUM([BeforeTax]) 'OrderBeforeTaxSum', SUM([Tax]) 'OrderTaxSum', SUM([Total]) 'OrderTotalSum'
                FROM [Invoices]
                WHERE [DateCreation] &gt;= @DateCreationFrom
                AND [DateCreation] &lt;= @DateCreationTo"
            ConnectionString="<%$ ConnectionStrings:HeliSound %>" >
            <SelectParameters>
                <asp:Parameter Name="DateCreationFrom" />
                <asp:Parameter Name="DateCreationTo" />
            </SelectParameters>
        </asp:SqlDataSource>

    </div>
</asp:Content>
