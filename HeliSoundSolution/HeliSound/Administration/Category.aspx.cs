﻿using HeliSound.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Administration
{
    public partial class Category : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAddCategory_Click(object sender, EventArgs e)
        {
            dtlCategory.ChangeMode(DetailsViewMode.Insert);
        }

        protected void dtlCategory_DataBound(object sender, EventArgs e)
        {
            btnAddCategory.Visible = (dtlCategory.DataItemCount == 0) && (dtlCategory.CurrentMode != DetailsViewMode.Insert);
        }

        protected void dtlCategory_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            Response.Redirect(Request.Url.ToString());
        }

        protected void dtlCategory_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            Response.Redirect(Request.Url.ToString());
        }

        protected void dtlCategory_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
        {
            Response.Redirect(Request.Url.ToString());
        }

        protected void dtlCategory_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            var message = ValidateForm(e.Values, 0);
            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                e.Cancel = true;
                return;
            };
        }

        protected void dtlCategory_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            var id = (int)grdCategories.SelectedValue;
            var message = ValidateForm(e.NewValues, id);

            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                e.Cancel = true;
                return;
            };
        }

        protected void dtlCategory_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
        {
            var id = (int)grdCategories.SelectedValue;
            var count = StoreProvider.ProductIdByCategoryIdCount(id);
            if (count != 0)
            {
                // Invalid data
                DisplayMessage("Can not delete. There is " + count.ToString() + " products assosiated with this category.", true);
                e.Cancel = true;
                return;
            };
        }
        //-------------------------------------------------------------------------------

        private void DisplayMessage(string message, bool isError)
        {
            lblErrorMessage.Text = string.Empty;
            lblInfoMessage.Text = string.Empty;
            if (isError)
            {
                lblErrorMessage.Text = message;
            }
            else
            {
                lblInfoMessage.Text = message;
            }
        }

        private string ValidateForm(IOrderedDictionary values, int updatingId)
        {
            // CategoryDescription
            if ((values["CategoryDescription"] == null) || (values["CategoryDescription"].ToString().Length < 3))
            {
                return "Category description is too short (required minimum 3 characters).";
            }
            if (values["CategoryDescription"].ToString().Length > 60)
            {
                return "Category description is too long (maximum 60 characters).";
            }
            // Existing category
            var existingId = StoreProvider.CategoryIdByCategoryDescriptionGet(values["CategoryDescription"].ToString().Trim());
            if ((existingId > 0) && (updatingId != existingId))
            {
                return "Specified category description already exists.";
            }
            return string.Empty;
        }
    }
}