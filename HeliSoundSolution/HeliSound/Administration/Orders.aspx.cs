﻿using HeliSound.Common;
using HeliSound.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Administration
{
    public partial class Orders : System.Web.UI.Page
    {
        public int _ShippingStatus {
            get {
                if (dtlInvoice.Rows.Count == 0) return -1;
                try
                {
                    var dv = (DataView)dsInvoice.Select(DataSourceSelectArguments.Empty);
                    return Convert.ToInt32((dv).Table.Rows[0]["ShippingStatus"].ToString());
                }
                catch
                {
                    return -1;
                }                
            }
        }

        public string _InvoiceId
        {
            get
            {
                try
                {
                    var invoiceId = Convert.ToInt32(txtInvoiceId.Text);
                    return invoiceId.ToString();
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        //-----------------------------------------------------------------------------------------

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DisplayMessageInvoice(string.Empty, false);
                pnlInvoice.Visible = false;
                pnlInvoiceItems.Visible = false;
                pnlInvoiceItem.Visible = false;
                // check if InvoiceId parameter exists in URL then use it.
                var paramInvoiceId = Request.QueryString["InvoiceId"];
                if (paramInvoiceId != null)
                {
                    txtInvoiceId.Text = paramInvoiceId;
                    dsInvoice.SelectParameters["Id"].DefaultValue = _InvoiceId;
                    dtlInvoice.DataBind();
                }                
            } else
            {
                dsInvoice.SelectParameters["Id"].DefaultValue = _InvoiceId;
            }
        }

        protected void btnSearchInvoice_Click(object sender, EventArgs e)
        {
            dsInvoice.SelectParameters["Id"].DefaultValue = _InvoiceId;
            dtlInvoice.DataBind();
        }

        protected void dtlInvoice_DataBound(object sender, EventArgs e)
        {
            //UpdateUI();
            pnlInvoice.Visible = true;
            pnlInvoiceItems.Visible = (dtlInvoice.PageCount > 0);
            pnlInvoiceItem.Visible = grdInvoiceItems.SelectedIndex != -1;
        }
        protected void dtlInvoice_ModeChanged(object sender, EventArgs e)
        {
            UpdateUI();
            if (dtlInvoice.CurrentMode != DetailsViewMode.ReadOnly)
            {
                grdInvoiceItems.SelectedIndex = -1;
                dtlInvoiceItem.ChangeMode(DetailsViewMode.ReadOnly);
            }
        }
        protected void grdInvoiceItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateUI();
            if (grdInvoiceItems.SelectedIndex != -1)
            {
                dtlInvoice.ChangeMode(DetailsViewMode.ReadOnly);
                dtlInvoiceItem.ChangeMode(DetailsViewMode.Edit);
            }
        }
        protected void dtlInvoiceItem_ModeChanged(object sender, EventArgs e)
        {
            dtlInvoiceItem_ItemUpdated(sender, null);
        }

        protected void ddInvoiceItemProductId_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                dsProduct.SelectParameters["Id"].DefaultValue = ((DropDownList)sender).SelectedValue;
                var dv = (DataView)dsProduct.Select(DataSourceSelectArguments.Empty);
                ((TextBox)dtlInvoiceItem.FindControl("txtInvoiceItemRetailPrice")).Text = dv.Table.Rows[0]["RetailPrice"].ToString();
            }
            catch 
            {
                DisplayMessageInvoiceItem("Can not fing product's price. Critical error.", true);
            }
        }

        protected void dtlInvoiceItem_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
        {
            // This is workaround code.
            //      when updating data in dtlInvoiceItem more that one time
            //      then exception thrown with error:  ViewState tree is broken...
            // So, the workaround is just refresh page after each update.
            RefreshPage();
        }


        protected void dtlInvoice_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            var message = ValidateInvoiceForm(e.NewValues);

            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessageInvoice(message, true);
                e.Cancel = true;
                return;
            };
        }

        protected void dtlInvoiceItem_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            var message = ValidateInvoiceItemForm(e.NewValues);

            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessageInvoiceItem(message, true);
                e.Cancel = true;
                return;
            };
        }


        protected void grdInvoiceItems_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            dtlInvoice_ItemDeleted(sender, null);
        }

        protected void dtlInvoice_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            DisplayMessageInvoiceItem("Invoice " + _InvoiceId + " cuccessfully deleted.", false);

            txtInvoiceId.Text = string.Empty;
            pnlInvoice.Visible = false;
            pnlInvoiceItems.Visible = false;
            pnlInvoiceItem.Visible = false;

        }

        //-------------------------------------------------------------------------------

        private void DisplayMessageInvoice(string message, bool isError)
        {
            lblErrorMessageInvoiceItem.Text = string.Empty;
            lblInfoMessageInvoiceItem.Text = string.Empty;
            lblErrorMessageInvoice.Text = string.Empty;
            lblInfoMessageInvoice.Text = string.Empty;
            if (isError)
            {
                lblErrorMessageInvoice.Text = message;
            }
            else
            {
                lblInfoMessageInvoice.Text = message;
            }
        }

        private void DisplayMessageInvoiceItem(string message, bool isError)
        {
            lblErrorMessageInvoiceItem.Text = string.Empty;
            lblInfoMessageInvoiceItem.Text = string.Empty;
            lblErrorMessageInvoice.Text = string.Empty;
            lblInfoMessageInvoice.Text = string.Empty;
            if (isError)
            {
                lblErrorMessageInvoiceItem.Text = message;
            }
            else
            {
                lblInfoMessageInvoiceItem.Text = message;
            }
        }

        private void UpdateUI()
        {
            DisplayMessageInvoice(string.Empty, false);
            pnlInvoice.Visible = true;
            pnlInvoiceItems.Visible = (dtlInvoice.PageCount > 0);
            pnlInvoiceItem.Visible = grdInvoiceItems.SelectedIndex != -1;
        }

        private void RefreshPage()
        {
            var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
            nameValues.Set("InvoiceId", _InvoiceId);
            Response.Redirect(Request.Url.AbsolutePath + "?" + nameValues);
        }
        private string ValidateInvoiceForm(IOrderedDictionary values)
        {
            DisplayMessageInvoice(string.Empty, false);
            Regex rgx;

            // [CardTypeId] INT NOT NULL,
            if ((values["CardTypeId"] == null) || (values["CardTypeId"].ToString().Trim().Equals(string.Empty)))
            {
                return "Card Type is required.";
            }
            // [CardHolderName]             NVARCHAR(128)  NOT NULL,
            if ((values["CardHolderName"] == null) || (values["CardHolderName"].ToString().Length < 3))
            {
                return "Card Holder Name is too short (required minimum 3 characters).";
            }
            if (values["CardHolderName"].ToString().Length > 120)
            {
                return "Card Holder Name is too long (maximum 120 characters).";
            }
            // [CardNumber]                 NVARCHAR(32)   NOT NULL,
            //      the regular expression for REAL card numbers can be taken from here:
            //      https://stackoverflow.com/questions/9315647/regex-credit-card-number-tests
            //      I'll use trivial check for mask: "#### #### #### ####"
            rgx = new Regex("^[0-9][0-9][0-9][0-9] ?[0-9][0-9][0-9][0-9] ?[0-9][0-9][0-9][0-9] ?[0-9][0-9][0-9][0-9]$"); //Trivial card number (with optional spaces)
            if ((values["CardNumber"] == null) || !rgx.IsMatch(values["CardNumber"].ToString().Trim()))
            {
                return "Wrong Card Number format. Should match exactly 0000 0000 0000 0000 (spaces are optional).";
            }
            // [Card3Secutity]              NVARCHAR(4)    NOT NULL,
            if ((values["Card3Secutity"] == null) || (values["Card3Secutity"].ToString().Trim().Length != 3))
            {
                return "Card Secutity Code is required. Must be a 3 digit number.";
            }
            else
            {
                try
                {
                    var houseNumber = Convert.ToInt32(values["Card3Secutity"].ToString().Trim());
                    if (houseNumber < 1) return "Card Secutity Code must be greater that 0.";
                    if (houseNumber >= 1000) return "Card Secutity Code must be less that 1000.";
                }
                catch
                {
                    return "Card Secutity Code must be a 3 digit number.";
                }
            }
            // [CardExpirationMonth]        INT NOT NULL,
            if ((values["CardExpirationMonth"] == null) || (values["CardExpirationMonth"].ToString().Trim().Equals(string.Empty)))
            {
                return "Card Expiration Month is required.";
            }
            // [CardExpirationYear] INT NOT NULL,
            if ((values["CardExpirationYear"] == null) || (values["CardExpirationYear"].ToString().Trim().Equals(string.Empty)))
            {
                return "Card Expiration Year is required.";
            }

            // [AddressHouseNumber]         NVARCHAR(16)   NOT NULL,
            if ((values["AddressHouseNumber"] == null) || (string.IsNullOrWhiteSpace(values["AddressHouseNumber"].ToString())))
            {
                return "House Number is required.";
            }
            else
            {
                try
                {
                    var houseNumber = Convert.ToInt32(values["AddressHouseNumber"].ToString().Trim());
                    if (houseNumber < 1) return "House Number must be greater that 0.";
                    if (houseNumber >= 10000) return "House Number must be less that 10000.";
                }
                catch
                {
                    return "House Number must be a number.";
                }
            }
            // [AddressStreetName]          NVARCHAR(128)  NOT NULL,
            if ((values["AddressStreetName"] == null) || (values["AddressStreetName"].ToString().Length < 2))
            {
                return "Street Name is too short (required minimum 2 characters).";
            }
            if (values["AddressStreetName"].ToString().Length > 60)
            {
                return "Street Name is too long (maximum 60 characters).";
            }
            // [AddressStreetDesignationId] INT NOT NULL,
            if ((values["AddressStreetDesignationId"] == null) || (values["AddressStreetDesignationId"].ToString().Trim().Equals(string.Empty)))
            {
                return "Street Designation is required.";
            }
            // [AddressAppartmentNumber]    NVARCHAR(16)   NULL,
            if ((values["AddressAppartmentNumber"] == null) || (string.IsNullOrWhiteSpace(values["AddressAppartmentNumber"].ToString())))
            {
                // this field allows NULL
                values["AddressAppartmentNumber"] = null;
            }
            else
            {
                try
                {
                    var appNumber = Convert.ToInt32(values["AddressAppartmentNumber"].ToString().Trim());
                    if (appNumber < 1) return "Appartment Number must be greater that 0.";
                    if (appNumber >= 10000) return "Appartment Number must be less that 10000.";
                }
                catch
                {
                    return "Appartment Number must be a number.";
                }
            }
            // [AddressCity]                NVARCHAR(64)   NOT NULL,
            if ((values["AddressCity"] == null) || (values["AddressCity"].ToString().Length < 2))
            {
                return "City is too short (required minimum 2 characters).";
            }
            if (values["AddressCity"].ToString().Length > 60)
            {
                return "City is too long (maximum 60 characters).";
            }
            // [AddressProvince]            NVARCHAR(64)   NOT NULL,
            if ((values["AddressProvince"] == null) || (values["AddressProvince"].ToString().Trim().Equals(string.Empty)))
            {
                return "Province is required.";
            }
            // [AddressPostalCode]          NVARCHAR(8)    NOT NULL,
            //      I'm using suggestion from:
            //      https://stackoverflow.com/questions/1146202/canadian-postal-code-validation
            rgx = new Regex("^[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]$"); //Canada postal code (with optional space)
            if ((values["AddressPostalCode"] == null) || !rgx.IsMatch(values["AddressPostalCode"].ToString().Trim().ToUpper()))
            {
                return "Wrong Canada Postal Code format. Should match exactly A0A 0A0 (space is optional)." + Environment.NewLine +
                    "Canadian postal codes can't contain the letters D, F, I, O, Q, or U, and cannot start with W or Z";
            }

            return string.Empty;
        }
        private string ValidateInvoiceItemForm(IOrderedDictionary values)
        {
            //  ProductId            
            if ((values["ProductId"] == null) || (values["ProductId"].ToString().Trim().Equals(string.Empty)))
            {
                return "Product is required.";
            }
            //  Quantity
            try
            {
                var houseNumber = Convert.ToInt32(values["Quantity"].ToString().Trim());
                if (houseNumber < 1) return "Quantity must be greater that 0.";
                if (houseNumber > 100) return "Quantity must be less that 100.";
            }
            catch
            {
                return "Quantity must be a whole number.";
            }
            //  RetailPrice
            try
            {
                var houseNumber = Convert.ToDecimal(values["RetailPrice"].ToString().Trim());
                if (houseNumber < 0.01M) return "RetailPrice must be greater that 0.01";
                if (houseNumber > 10000M) return "RetailPrice must be less that 10,000.00";
            }
            catch
            {
                return "RetailPrice must be a decimal number.";
            }

            return string.Empty;
        }


    }
}