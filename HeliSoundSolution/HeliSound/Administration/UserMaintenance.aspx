﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administration/Admin.Master" AutoEventWireup="true" CodeBehind="UserMaintenance.aspx.cs" Inherits="HeliSound.Administration.UserMaintenance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="page-title">
            User Maintenance
        </div>  

        <h3 class="grid-layout-800">List of available roles</h3>
        
        <asp:GridView ID="grdUsers" runat="server" GridLines="None" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="dsUsers" CssClass="user-grid">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="False" />
                <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                <asp:BoundField DataField="EmailAddress" HeaderText="Email" SortExpression="EmailAddress" />
                <asp:BoundField DataField="PhoneNumber" HeaderText="Phone" SortExpression="PhoneNumber" Visible="False" />
                <asp:BoundField DataField="Password" HeaderText="Password" SortExpression="Password" Visible="False" />
                <asp:BoundField DataField="RoleId" HeaderText="RoleId" SortExpression="RoleId" Visible="False" />
                <asp:BoundField DataField="Role" HeaderText="Role" SortExpression="Role" />
                <asp:CheckBoxField DataField="Status" HeaderText="Status" SortExpression="Status" Text="Active" />
                <asp:CommandField ShowSelectButton="True" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsUsers" runat="server" 
            SelectCommand=
                "SELECT u.[Id], u.[FirstName], u.[LastName], u.[EmailAddress], u.[PhoneNumber], u.[Password], u.[RoleId], u.[Status], r.[Role] 
                FROM [Users] u 
                INNER JOIN [Roles] r
                ON u.[RoleId] = r.[Id]
                WHERE u.[RoleId] IN (1, 2)
                ORDER BY u.[LastName], u.[FirstName]"
            ConnectionString="<%$ ConnectionStrings:HeliSound %>"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsRoles" runat="server" 
            SelectCommand=
                "SELECT [Id], [Role]
                FROM [Roles] 
                WHERE [Id] IN (1, 2)
                ORDER BY [Role]"
            ConnectionString="<%$ ConnectionStrings:HeliSound %>"></asp:SqlDataSource>

        <div class="grid-layout-800">
            <asp:Button ID="btnAddUser" runat="server" Text="Add new user" OnClick="btnAddUser_Click" CssClass="action-button" />
        </div>
        
        <asp:DetailsView ID="dtlUser" runat="server" AutoGenerateRows="False"  DataKeyNames="Id" DataSourceID="dsUser" GridLines="None" OnItemDeleted="dtlUser_ItemDeleted" OnItemDeleting="dtlUser_ItemDeleting" OnItemInserted="dtlUser_ItemInserted" OnItemInserting="dtlUser_ItemInserting" OnItemUpdated="dtlUser_ItemUpdated" OnItemUpdating="dtlUser_ItemUpdating" OnDataBound="dtlUser_DataBound" CssClass="user-details" OnModeChanged="dtlUser_ModeChanged">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
            <EditRowStyle BackColor="#7C6F57" />
            <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
            <Fields>
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="False" />
                <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                <asp:BoundField DataField="EmailAddress" HeaderText="Email" SortExpression="EmailAddress" />
                <asp:BoundField DataField="PhoneNumber" HeaderText="Phone" SortExpression="PhoneNumber" Visible="False" />
                <asp:TemplateField HeaderText="Password" SortExpression="Password">
                    <EditItemTemplate>
                        <br />
                        <span>Enter password (optional)</span>
                        <asp:TextBox ID="txtUserPassword1" runat="server" Text="" TextMode="Password"></asp:TextBox>
                        <br />
                        <span>Confirm password (optional)</span>
                        <asp:TextBox ID="txtUserPassword2" runat="server" Text="" TextMode="Password"></asp:TextBox>
                        <br />
                        <br />
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <br />
                        <span>Enter password</span>
                        <asp:TextBox ID="txtUserPassword1" runat="server" Text="" TextMode="Password"></asp:TextBox>
                        <br />
                        <span>Repeat password</span>
                        <asp:TextBox ID="txtUserPassword2" runat="server" Text="" TextMode="Password"></asp:TextBox>
                        <br />
                        <br />
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" Text="********"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Role" SortExpression="RoleId">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddUserRoleIdEdit" runat="server"
                            DataSourceID="dsRoles" DataTextField="Role" DataValueField="Id" SelectedValue='<%# Bind("RoleId") %>'>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:DropDownList ID="ddUserRoleIdEdit" runat="server" AppendDataBoundItems="true"
                            DataSourceID="dsRoles" DataTextField="Role" DataValueField="Id" SelectedValue='<%# Bind("RoleId") %>'>
                            <asp:ListItem Selected="True" Text="Choose Role" Value="0" />
                        </asp:DropDownList>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddUserRoleIdEdit" runat="server" Enabled="false"
                            DataSourceID="dsRoles" DataTextField="Role" DataValueField="Id" SelectedValue='<%# Bind("RoleId") %>'>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" />
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" />
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" Enabled="false" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
            </Fields>
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="dsUser" runat="server" 
            ConflictDetection="CompareAllValues" 
            ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
            OldValuesParameterFormatString="original_{0}" 
            DeleteCommand="DELETE FROM [Users] WHERE [Id] = @original_Id" 
            InsertCommand="INSERT INTO [Users] ([FirstName], [LastName], [EmailAddress], [PhoneNumber], [Password], [RoleId], [Status]) VALUES (@FirstName, @LastName, @EmailAddress, @PhoneNumber, @Password, @RoleId, @Status)" 
            SelectCommand="SELECT [Id], [FirstName], [LastName], [EmailAddress], [PhoneNumber], [Password], [RoleId], [Status] FROM [Users] WHERE ([Id] = @Id)" 
            UpdateCommand="UPDATE [Users] SET [FirstName] = @FirstName, [LastName] = @LastName, [EmailAddress] = @EmailAddress, [PhoneNumber] = @PhoneNumber, [Password] = ISNULL(@Password, [Password]), [RoleId] = @RoleId, [Status] = @Status WHERE [Id] = @original_Id">
            <DeleteParameters>
                <asp:Parameter Name="original_Id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="FirstName" Type="String" />
                <asp:Parameter Name="LastName" Type="String" />
                <asp:Parameter Name="EmailAddress" Type="String" />
                <asp:Parameter Name="PhoneNumber" Type="String" />
                <asp:Parameter Name="Password" Type="String" />
                <asp:Parameter Name="RoleId" Type="Int32" />
                <asp:Parameter Name="Status" Type="Boolean" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="grdUsers" DefaultValue="0" Name="Id" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="FirstName" Type="String" />
                <asp:Parameter Name="LastName" Type="String" />
                <asp:Parameter Name="EmailAddress" Type="String" />
                <asp:Parameter Name="PhoneNumber" Type="String" />
                <asp:Parameter Name="Password" Type="String" />
                <asp:Parameter Name="RoleId" Type="Int32" />
                <asp:Parameter Name="Status" Type="Boolean" />
                <asp:Parameter Name="original_Id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <br />

        <div class="grid-layout-800">
            <asp:Label ID="lblErrorMessage" runat="server" Text="" CssClass="error-message"></asp:Label>
            <asp:Label ID="lblInfoMessage" runat="server" Text="" CssClass="info-message"></asp:Label>
        </div>
    </div>
</asp:Content>
