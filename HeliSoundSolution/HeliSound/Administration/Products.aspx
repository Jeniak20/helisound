﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administration/Admin.Master" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="HeliSound.Administration.Products" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="page-title">
            Products
        </div>  
        <table class="grid-layout-400">
            <tr>
                <td>
                    <span class="field-label">Choose Supplier</span>
                </td>
                <td>
                    <asp:DropDownList ID="ddChooseSupplier" runat="server" AutoPostBack="True" DataSourceID="dsSuppliers" DataTextField="CompanyName" DataValueField="Id" AppendDataBoundItems="true" OnSelectedIndexChanged="ddChooseSupplier_SelectedIndexChanged" CssClass="text-field">
                        <asp:ListItem Selected="True" Text="Choose Supplier" Value="0" />
                    </asp:DropDownList>
                </td>
            </tr>
        </table>


        <asp:SqlDataSource ID="dsSuppliers" runat="server" ConnectionString="<%$ ConnectionStrings:HeliSound %>" SelectCommand="SELECT [Id], [CompanyName] FROM [Suppliers] ORDER BY [CompanyAddress]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsCategories" runat="server" ConnectionString="<%$ ConnectionStrings:HeliSound %>" SelectCommand="SELECT [Id], [CategoryDescription] FROM [Categories] ORDER BY [CategoryDescription]"></asp:SqlDataSource>
        <asp:Panel ID="pnlProducts" runat="server" Visible="false">
            <h3 class="grid-layout-800">List of available products</h3>
            <asp:GridView ID="grdProducts" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="dsProducts" GridLines="None" CssClass="product-grid" OnSelectedIndexChanged="grdProducts_SelectedIndexChanged">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="False" />
                    <asp:BoundField DataField="CategoryDescription" HeaderText="Category" SortExpression="CategoryDescription" />
                    <asp:BoundField DataField="ProductName" HeaderText="Product" SortExpression="ProductName" />
                    <asp:BoundField DataField="SupplierId" HeaderText="SupplierId" SortExpression="SupplierId" Visible="False" />
                    <asp:BoundField DataField="CategoryId" HeaderText="CategoryId" SortExpression="CategoryId" Visible="False" />
                    <asp:BoundField DataField="RetailPrice" HeaderText="Price" SortExpression="RetailPrice" DataFormatString="{0:c}" />
                    <asp:CheckBoxField DataField="Status" HeaderText="Status" SortExpression="Status" Text="Active" />
                    <asp:CommandField ShowSelectButton="True" SelectText="Edit" />
                </Columns>
                <EditRowStyle BackColor="#7C6F57" />
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <asp:SqlDataSource ID="dsProducts" runat="server" 
                ConflictDetection="CompareAllValues" 
                ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
                SelectCommand=
                    "SELECT p.[Id], p.[ProductName], p.[SupplierId], p.[CategoryId], p.[RetailPrice], p.[Status] , c.[CategoryDescription]
                    FROM [Products] p
                    INNER JOIN [Categories] c
                    ON p.[CategoryId] = c.[Id]
                    WHERE (p.[SupplierId] = @SupplierId) 
                    ORDER BY p.[ProductName]"
                OldValuesParameterFormatString="original_{0}">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddChooseSupplier" DefaultValue="0" Name="SupplierId" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>

            <div class="grid-layout-800">
                <asp:Button ID="btnAddProduct" runat="server" Text="Add new product" OnClick="btnAddProduct_Click" CssClass="action-button" />
            </div>

            <asp:DetailsView ID="dtlProduct" runat="server" AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="dsProduct" GridLines="None" OnDataBound="dtlProduct_DataBound" OnItemDeleted="dtlProduct_ItemDeleted" OnItemInserted="dtlProduct_ItemInserted" OnItemInserting="dtlProduct_ItemInserting" OnItemUpdated="dtlProduct_ItemUpdated" OnItemUpdating="dtlProduct_ItemUpdating" CssClass="product-details" OnModeChanged="dtlProduct_ModeChanged">
                <AlternatingRowStyle BackColor="White" />
                <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                <EditRowStyle BackColor="#7C6F57" />
                <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
                <Fields>
                    <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="False" />
                    <asp:TemplateField HeaderText="Supplier" SortExpression="CompanyName">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddProductSupplierIdEdit" runat="server"
                                DataSourceID="dsSuppliers" DataTextField="CompanyName" DataValueField="Id" SelectedValue='<%# Bind("SupplierId") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:DropDownList ID="ddProductSupplierIdInsert" runat="server" AppendDataBoundItems="true"
                                DataSourceID="dsSuppliers" DataTextField="CompanyName" DataValueField="Id" SelectedValue='<%# Bind("SupplierId") %>'>
                                <asp:ListItem Selected="True" Text="Choose Supplier" Value="0" />
                            </asp:DropDownList>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("CompanyName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category" SortExpression="CategoryDescription">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddProductCategoryIdEdit" runat="server"
                                DataSourceID="dsCategories" DataTextField="CategoryDescription" DataValueField="Id" SelectedValue='<%# Bind("CategoryId") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:DropDownList ID="ddProductCategoryIdInsert" runat="server" AppendDataBoundItems="true"
                                DataSourceID="dsCategories" DataTextField="CategoryDescription" DataValueField="Id" SelectedValue='<%# Bind("CategoryId") %>'>
                                <asp:ListItem Selected="True" Text="Choose Category" Value="0" />
                            </asp:DropDownList>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("CategoryDescription") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ProductName" HeaderText="Product" SortExpression="ProductName" />
                    <asp:BoundField DataField="SupplierId" HeaderText="SupplierId" SortExpression="SupplierId" Visible="False" />
                    <asp:BoundField DataField="CategoryId" HeaderText="CategoryId" SortExpression="CategoryId" Visible="False" />
                    <asp:BoundField DataField="RetailPrice" DataFormatString="{0:c}" HeaderText="Price" SortExpression="RetailPrice" />
                    <asp:TemplateField HeaderText="Status" SortExpression="Status">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" />
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" />
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" Enabled="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                </Fields>
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
            </asp:DetailsView>
            <asp:SqlDataSource ID="dsProduct" runat="server" 
                ConflictDetection="CompareAllValues" 
                ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
                DeleteCommand="DELETE FROM [Products] WHERE [Id] = @original_Id" 
                InsertCommand="INSERT INTO [Products] ([ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (@ProductName, @SupplierId, @CategoryId, @RetailPrice, @Status)" 
                UpdateCommand="UPDATE [Products] SET [ProductName] = @ProductName, [SupplierId] = @SupplierId, [CategoryId] = @CategoryId, [RetailPrice] = @RetailPrice, [Status] = @Status WHERE [Id] = @original_Id"
                SelectCommand=
                    "SELECT p.[Id], p.[ProductName], p.[SupplierId], p.[CategoryId], p.[RetailPrice], p.[Status], s.[CompanyName], c.[CategoryDescription]
                    FROM [Products] p
                    INNER JOIN [Suppliers] s
                    ON p.[SupplierId] = s.[Id]
                    INNER JOIN [Categories] c
                    ON p.[CategoryId] = c.[Id]
                    WHERE (p.[Id] = @Id)" 
                OldValuesParameterFormatString="original_{0}">
                <DeleteParameters>
                    <asp:Parameter Name="original_Id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="ProductName" Type="String" />
                    <asp:Parameter Name="SupplierId" Type="Int32" />
                    <asp:Parameter Name="CategoryId" Type="Int32" />
                    <asp:Parameter Name="RetailPrice" Type="Decimal" />
                    <asp:Parameter Name="Status" Type="Boolean" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="grdProducts" DefaultValue="0" Name="Id" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="ProductName" Type="String" />
                    <asp:Parameter Name="SupplierId" Type="Int32" />
                    <asp:Parameter Name="CategoryId" Type="Int32" />
                    <asp:Parameter Name="RetailPrice" Type="Decimal" />
                    <asp:Parameter Name="Status" Type="Boolean" />
                    <asp:Parameter Name="original_Id" Type="Int32" />
                    <asp:Parameter Name="original_ProductName" Type="String" />
                    <asp:Parameter Name="original_SupplierId" Type="Int32" />
                    <asp:Parameter Name="original_CategoryId" Type="Int32" />
                    <asp:Parameter Name="original_RetailPrice" Type="Decimal" />
                    <asp:Parameter Name="original_Status" Type="Boolean" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </asp:Panel>
        <div class="grid-layout-800">
            <asp:Label ID="lblErrorMessage" runat="server" Text="" CssClass="error-message"></asp:Label>
            <asp:Label ID="lblInfoMessage" runat="server" Text="" CssClass="info-message"></asp:Label>
        </div>
    </div>
</asp:Content>
