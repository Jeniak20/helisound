﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administration/Admin.Master" AutoEventWireup="true" CodeBehind="Category.aspx.cs" Inherits="HeliSound.Administration.Category" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="page-title">
            Category
        </div>  
        <h3 class="grid-layout-600">List of available categories</h3>
        <asp:GridView ID="grdCategories" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="dsCategories" GridLines="None" CssClass="category-grid">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="False" />
                <asp:BoundField DataField="CategoryDescription" HeaderText="Category Description" SortExpression="CategoryDescription" />
                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Enabled="false" Text="Active" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowSelectButton="True" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsCategories" runat="server" ConnectionString="<%$ ConnectionStrings:HeliSound %>" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT [Id], [CategoryDescription], [Status] FROM [Categories] ORDER BY [CategoryDescription]"></asp:SqlDataSource>

        <div class="grid-layout-600">
            <asp:Button ID="btnAddCategory" runat="server" Text="Add new category" OnClick="btnAddCategory_Click" CssClass="action-button" />
        </div>

        <asp:DetailsView ID="dtlCategory" runat="server" AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="dbCategory" GridLines="None" OnDataBound="dtlCategory_DataBound" OnItemDeleted="dtlCategory_ItemDeleted" OnItemInserted="dtlCategory_ItemInserted" OnItemInserting="dtlCategory_ItemInserting" OnItemUpdated="dtlCategory_ItemUpdated" OnItemUpdating="dtlCategory_ItemUpdating" OnItemDeleting="dtlCategory_ItemDeleting" CssClass="category-details">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
            <EditRowStyle BackColor="#7C6F57" />
            <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
            <Fields>
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="False" />
                <asp:BoundField DataField="CategoryDescription" HeaderText="Category Description" SortExpression="CategoryDescription" />
                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" />
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" />
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" Enabled="false" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
            </Fields>
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="dbCategory" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:HeliSound %>" DeleteCommand="DELETE FROM [Categories] WHERE [Id] = @original_Id AND [CategoryDescription] = @original_CategoryDescription AND [Status] = @original_Status" InsertCommand="INSERT INTO [Categories] ([CategoryDescription], [Status]) VALUES (@CategoryDescription, @Status)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT [Id], [CategoryDescription], [Status] FROM [Categories] WHERE ([Id] = @Id)" UpdateCommand="UPDATE [Categories] SET [CategoryDescription] = @CategoryDescription, [Status] = @Status WHERE [Id] = @original_Id AND [CategoryDescription] = @original_CategoryDescription AND [Status] = @original_Status">
            <DeleteParameters>
                <asp:Parameter Name="original_Id" Type="Int32" />
                <asp:Parameter Name="original_CategoryDescription" Type="String" />
                <asp:Parameter Name="original_Status" Type="Boolean" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="CategoryDescription" Type="String" />
                <asp:Parameter Name="Status" Type="Boolean" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="grdCategories" DefaultValue="0" Name="Id" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="CategoryDescription" Type="String" />
                <asp:Parameter Name="Status" Type="Boolean" />
                <asp:Parameter Name="original_Id" Type="Int32" />
                <asp:Parameter Name="original_CategoryDescription" Type="String" />
                <asp:Parameter Name="original_Status" Type="Boolean" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <div class="grid-layout-600">
            <asp:Label ID="lblErrorMessage" runat="server" Text="" CssClass="error-message"></asp:Label>
            <asp:Label ID="lblInfoMessage" runat="server" Text="" CssClass="info-message"></asp:Label>
        </div>
    </div>
</asp:Content>
