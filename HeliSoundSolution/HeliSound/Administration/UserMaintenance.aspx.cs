﻿using HeliSound.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Administration
{
    public partial class UserMaintenance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            dtlUser.ChangeMode(DetailsViewMode.Insert);
        }

        protected void dtlUser_DataBound(object sender, EventArgs e)
        {
            btnAddUser.Visible = (dtlUser.DataItemCount == 0) && (dtlUser.CurrentMode != DetailsViewMode.Insert);
        }

        protected void dtlUser_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            grdUsers.DataBind();
        }

        protected void dtlUser_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            grdUsers.DataBind();
        }

        protected void dtlUser_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
        {
            grdUsers.DataBind();
        }

        protected void dtlUser_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
        {
            // Last active administrator
            int deletingId = (int)grdUsers.SelectedValue;
            var activeAdministratorIds = UserProvider.ActiveAdministratorIdsGet();

            if ((activeAdministratorIds.Length == 1)                       // only one active admin left in database
                && (deletingId == activeAdministratorIds[0]))              // we are deleting last active admin
            {
                // Invalid data
                DisplayMessage("System need at least one active administrator.", true);
                e.Cancel = true;
                return;
            }
        }

        protected void dtlUser_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            var message = ValidateForm(e.Values, 0);
            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                e.Cancel = true;
                return;
            };
        }

        protected void dtlUser_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            var id = (int)grdUsers.SelectedValue;
            var message = ValidateForm(e.NewValues, id);

            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                e.Cancel = true;
                return;
            };
        }
        protected void dtlUser_ModeChanged(object sender, EventArgs e)
        {
            DisplayMessage(string.Empty, false);
        }
        //-------------------------------------------------------------------------------

        private void DisplayMessage(string message, bool isError)
        {
            lblErrorMessage.Text = string.Empty;
            lblInfoMessage.Text = string.Empty;
            if (isError)
            {
                lblErrorMessage.Text = message;
            }
            else
            {
                lblInfoMessage.Text = message;
            }
        }

        private string ValidateForm(IOrderedDictionary values, int updatingId)
        {
            // [FirstName]    NVARCHAR (64)  NULL,
            if ((values["FirstName"] == null) || (values["FirstName"].ToString().Length < 1))
            {
                return "First name is too short (required minimum 1 characters).";
            }
            if (values["FirstName"].ToString().Length > 60)
            {
                return "First name is too long (maximum 60 characters).";
            }
            // [LastName]     NVARCHAR (64)  NULL,
            if ((values["LastName"] == null) || (values["LastName"].ToString().Length < 1))
            {
                return "Last name is too short (required minimum 1 characters).";
            }
            if (values["LastName"].ToString().Length > 60)
            {
                return "Last name is too long (maximum 60 characters).";
            }
            // [EmailAddress] NVARCHAR (128) NOT NULL,
            if ((values["EmailAddress"] == null) || (string.IsNullOrWhiteSpace(values["LastName"].ToString())))
            {
                return "Email address is required.";
            }
            if (values["EmailAddress"].ToString().Length > 124)
            {
                return "Email address is too long (maximum 124 characters).";
            }
            try
            {
                MailAddress m = new MailAddress(values["EmailAddress"].ToString().Trim());
            }
            catch
            {
                return "Email format is wrong.";
            }
            // [PhoneNumber]  NVARCHAR (64)  NULL,
            //    Not user in this form

            // [Password]     NVARCHAR (128) NOT NULL,
            var password1 = ((TextBox)dtlUser.FindControl("txtUserPassword1")).Text;
            if ((updatingId == 0) || (!string.IsNullOrEmpty(password1))) // updatingId == 0 means inserting mode
            {
                if (password1.Length < 3)
                {
                    return "Password is too short (required minimum 3 characters).";
                }
                if (password1.Length > 10)
                {
                    return "Password is too long (maximum 10 characters).";
                }
                values["Password"] = FormsAuthentication.HashPasswordForStoringInConfigFile(password1, "SHA1");
            }
            // Matching password
            var password2 = ((TextBox)dtlUser.FindControl("txtUserPassword2")).Text;
            if (!password1.Equals(password2))
            {
                return "Passowrd should match Repeat password .";
            }
            // [RoleId]       INT            NOT NULL,
            if ((values["RoleId"] == null) || (values["RoleId"].ToString().Trim().Equals("0")))
            {
                return "Role is required.";
            }
            // Existing user
            var existingId = UserProvider.UserIdByEmailAddressGet(values["EmailAddress"].ToString().Trim());
            if ((existingId > 0) && (updatingId != existingId))
            {
                return "Specified email already used by other user.";
            }
            // Last active administrator
            var activeAdministratorIds = UserProvider.ActiveAdministratorIdsGet();
            if ((updatingId != 0)                                           // updatingId != 0 means editing mode
                && (activeAdministratorIds.Length==1)                       // only one active admin left in database
                && (updatingId == activeAdministratorIds[0])                // we are editing last active admin
                && ((!values["RoleId"].ToString().Trim().Equals("1") ||     //      (we are changing role to NOT ADMINISTRATORS
                    (!Convert.ToBoolean(values["Status"])))))               //      OR we are changing status to NOT ACTIVE)
            {
                return "System need at least one active administrator.";
            }
            return string.Empty;
        }


    }
}