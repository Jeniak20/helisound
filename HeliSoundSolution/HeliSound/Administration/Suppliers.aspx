﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administration/Admin.Master" AutoEventWireup="true" CodeBehind="Suppliers.aspx.cs" Inherits="HeliSound.Administration.Suppliers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="page-title">
            Suppliers
        </div>  

        <h3 class="grid-layout-800">List of available roles</h3>
        <asp:GridView ID="grdSuppliers" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="dsSuppliers" GridLines="None" AllowSorting="True" CssClass="supplier-grid">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="CompanyName" HeaderText="Company" SortExpression="CompanyName" />
                <asp:BoundField DataField="ContactPersonName" HeaderText="Contact Person" SortExpression="ContactPersonName" />
                <asp:BoundField DataField="ContactPersonTelephoneNumber" HeaderText="Telephone Number" SortExpression="ContactPersonTelephoneNumber" />
                <asp:CheckBoxField DataField="Status" HeaderText="Status" SortExpression="Status" Text="Active"/>
                <asp:CommandField ShowSelectButton="True" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsSuppliers" runat="server" 
            ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
            SelectCommand="SELECT [Id], [CompanyName], [ContactPersonName], [ContactPersonTelephoneNumber], [Status] FROM [Suppliers] ORDER BY [CompanyName]"></asp:SqlDataSource>

        <div class="grid-layout-800">
            <asp:Button ID="btnAddSupplier" runat="server" Text="Add new supplier" OnClick="btnAddSupplier_Click" CssClass="action-button" />
        </div>

        <asp:DetailsView ID="dtlSupplier" runat="server" AutoGenerateRows="False"  DataKeyNames="Id" DataSourceID="dsSupplier"  GridLines="None" OnDataBound="dtlSupplier_DataBound" OnItemDeleted="dtlSupplier_ItemDeleted" OnItemInserted="dtlSupplier_ItemInserted" OnItemUpdated="dtlSupplier_ItemUpdated" OnItemInserting="dtlSupplier_ItemInserting" OnItemUpdating="dtlSupplier_ItemUpdating" OnItemDeleting="dtlSupplier_ItemDeleting" CssClass="supplier-details" OnModeChanged="dtlSupplier_ModeChanged">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
            <EditRowStyle BackColor="#7C6F57" />
            <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
            <Fields>
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="False" />
                <asp:BoundField DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName" />
                <asp:BoundField DataField="CompanyAddress" HeaderText="Company Address" SortExpression="CompanyAddress" />
                <asp:BoundField DataField="ContactPersonName" HeaderText="Contact Person Name" SortExpression="ContactPersonName" />
                <asp:BoundField DataField="ContactPersonTelephoneNumber" HeaderText="Contact Telephone Number" SortExpression="ContactPersonTelephoneNumber" />
                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" />
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" />
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" Enabled="false" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
            </Fields>
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="dsSupplier" runat="server" 
            ConflictDetection="CompareAllValues" 
            ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
            DeleteCommand="DELETE FROM [Suppliers] WHERE [Id] = @original_Id AND [CompanyName] = @original_CompanyName AND [CompanyAddress] = @original_CompanyAddress AND [ContactPersonName] = @original_ContactPersonName AND [ContactPersonTelephoneNumber] = @original_ContactPersonTelephoneNumber AND [Status] = @original_Status" 
            InsertCommand="INSERT INTO [Suppliers] ([CompanyName], [CompanyAddress], [ContactPersonName], [ContactPersonTelephoneNumber], [Status]) VALUES (@CompanyName, @CompanyAddress, @ContactPersonName, @ContactPersonTelephoneNumber, @Status)" 
            OldValuesParameterFormatString="original_{0}" 
            SelectCommand="SELECT [Id], [CompanyName], [CompanyAddress], [ContactPersonName], [ContactPersonTelephoneNumber], [Status] FROM [Suppliers] WHERE ([Id] = @Id)" 
            UpdateCommand="UPDATE [Suppliers] SET [CompanyName] = @CompanyName, [CompanyAddress] = @CompanyAddress, [ContactPersonName] = @ContactPersonName, [ContactPersonTelephoneNumber] = @ContactPersonTelephoneNumber, [Status] = @Status WHERE [Id] = @original_Id AND [CompanyName] = @original_CompanyName AND [CompanyAddress] = @original_CompanyAddress AND [ContactPersonName] = @original_ContactPersonName AND [ContactPersonTelephoneNumber] = @original_ContactPersonTelephoneNumber AND [Status] = @original_Status">
            <DeleteParameters>
                <asp:Parameter Name="original_Id" Type="Int32" />
                <asp:Parameter Name="original_CompanyName" Type="String" />
                <asp:Parameter Name="original_CompanyAddress" Type="String" />
                <asp:Parameter Name="original_ContactPersonName" Type="String" />
                <asp:Parameter Name="original_ContactPersonTelephoneNumber" Type="String" />
                <asp:Parameter Name="original_Status" Type="Boolean" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="CompanyName" Type="String" />
                <asp:Parameter Name="CompanyAddress" Type="String" />
                <asp:Parameter Name="ContactPersonName" Type="String" />
                <asp:Parameter Name="ContactPersonTelephoneNumber" Type="String" />
                <asp:Parameter Name="Status" Type="Boolean" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="grdSuppliers" DefaultValue="0" Name="Id" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="CompanyName" Type="String" />
                <asp:Parameter Name="CompanyAddress" Type="String" />
                <asp:Parameter Name="ContactPersonName" Type="String" />
                <asp:Parameter Name="ContactPersonTelephoneNumber" Type="String" />
                <asp:Parameter Name="Status" Type="Boolean" />
                <asp:Parameter Name="original_Id" Type="Int32" />
                <asp:Parameter Name="original_CompanyName" Type="String" />
                <asp:Parameter Name="original_CompanyAddress" Type="String" />
                <asp:Parameter Name="original_ContactPersonName" Type="String" />
                <asp:Parameter Name="original_ContactPersonTelephoneNumber" Type="String" />
                <asp:Parameter Name="original_Status" Type="Boolean" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <div class="grid-layout-800">
            <asp:Label ID="lblErrorMessage" runat="server" Text="" CssClass="error-message"></asp:Label>
            <asp:Label ID="lblInfoMessage" runat="server" Text="" CssClass="info-message"></asp:Label>
        </div>
    </div>
</asp:Content>
