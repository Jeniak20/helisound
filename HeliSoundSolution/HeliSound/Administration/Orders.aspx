﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administration/Admin.Master" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="HeliSound.Administration.Orders" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:SqlDataSource ID="dsProducts" runat="server" 
        ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
        SelectCommand="SELECT * FROM [Products] ORDER BY [ProductName]">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsProduct" runat="server" 
        ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
        SelectCommand="SELECT * FROM [Products] WHERE ([Id] = @Id)">
        <SelectParameters>
            <asp:Parameter DefaultValue="0" Name="Id" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:ObjectDataSource ID="dsShippingStatuses" runat="server" 
        SelectMethod="ShippingStatusesGet" TypeName="HeliSound.DataAccessLayer.NonDatabaseProvider">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsCardExpirationMonths" runat="server" 
        SelectMethod="CardExpirationMonthsGet" TypeName="HeliSound.DataAccessLayer.NonDatabaseProvider">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsCardExpirationYears" runat="server" 
        SelectMethod="CardExpirationYearsGet" TypeName="HeliSound.DataAccessLayer.NonDatabaseProvider">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsCanadaProvinces" runat="server" 
        SelectMethod="CanadaProvincesGet" TypeName="HeliSound.DataAccessLayer.NonDatabaseProvider">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsStreetDesignations" runat="server" 
        SelectMethod="StreetDesignationsGet" TypeName="HeliSound.DataAccessLayer.NonDatabaseProvider">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsCardTypes" runat="server" 
        SelectMethod="CardTypesGet" TypeName="HeliSound.DataAccessLayer.NonDatabaseProvider">
    </asp:ObjectDataSource>


    <div>
        <div class="page-title">
            Order Reviewing
        </div>
        <table class="order-search">
            <tr>
                <td>
                    <span class="field-label">Enter&nbsp;Invoice&nbsp;Number</span>
                </td>
                <td>
                    <asp:TextBox ID="txtInvoiceId" runat="server" CssClass="text-field"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnSearchInvoice" runat="server" Text="Search" OnClick="btnSearchInvoice_Click" CssClass="action-button" />
                </td>
            </tr>
        </table>        
        
        <asp:Panel ID="pnlInvoice" runat="server">
            <h3 class="grid-layout-600">Invoice information</h3>
            <p class="grid-layout-600 my-comment">"DELETE" buttons on <b>Invoice information</b> and on <b>Items information</b> will delete both: Order (Invoice) and Product (InvoiceItem) from the database.</p>
            <asp:DetailsView ID="dtlInvoice" runat="server" AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="dsInvoice" GridLines="None" OnDataBound="dtlInvoice_DataBound" OnModeChanged="dtlInvoice_ModeChanged" EmptyDataText="Can not find invoice with specified number." OnItemUpdating="dtlInvoice_ItemUpdating" OnItemDeleted="dtlInvoice_ItemDeleted" CssClass="order-details" >
                <AlternatingRowStyle BackColor="White" />
                <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                <EditRowStyle BackColor="#7C6F57" />
                <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
                <Fields>
                    <asp:TemplateField HeaderText="GENERAL INFO">
                        <ItemTemplate>
                            <hr />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Id" HeaderText="Invoice Number" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                    <asp:BoundField DataField="DateCreation" HeaderText="Date Ordered" SortExpression="DateCreation" ReadOnly="True" />
                    <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" ReadOnly="True" />
                    <asp:BoundField DataField="FirstName" HeaderText="First Name" ReadOnly="True" SortExpression="FirstName" />
                    <asp:BoundField DataField="BeforeTax" HeaderText="BeforeTax" ReadOnly="True" SortExpression="BeforeTax" DataFormatString="{0:c}" />
                    <asp:BoundField DataField="Tax" HeaderText="Tax 13%" ReadOnly="True" SortExpression="Tax" DataFormatString="{0:c}" />
                    <asp:BoundField DataField="Total" HeaderText="Total" ReadOnly="True" SortExpression="Total" DataFormatString="{0:c}" />
                    <asp:TemplateField HeaderText="Shipping Status" SortExpression="ShippingStatus">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddInvoiceShippingStatus" runat="server" DataSourceID="dsShippingStatuses" DataTextField="Name" DataValueField="Id"  SelectedValue='<%# Bind("ShippingStatus") %>' Enabled="false">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ShippingDate" HeaderText="Shipping Date" ReadOnly="True" SortExpression="ShippingDate" />
                    <asp:TemplateField HeaderText="PAYMENT METHOD">
                        <ItemTemplate>
                            <hr />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Card Type" SortExpression="CardTypeId">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddInvoiceCardTypeId" runat="server" DataSourceID="dsCardTypes" DataTextField="Name" DataValueField="Id"  SelectedValue='<%# Bind("CardTypeId") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:DropDownList ID="ddInvoiceCardTypeId" runat="server" DataSourceID="dsCardTypes" DataTextField="Name" DataValueField="Id"  SelectedValue='<%# Bind("CardTypeId") %>' Enabled="false">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="CardHolderName" HeaderText="Card Holder Name" SortExpression="CardHolderName" />
                    <asp:BoundField DataField="CardNumber" HeaderText="Card Number" SortExpression="CardNumber" />
                    <asp:BoundField DataField="Card3Secutity" HeaderText="Card Secutity Code" SortExpression="Card3Secutity" />
                    <asp:TemplateField HeaderText="Card Expiration Month" SortExpression="CardExpirationMonth">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddInvoiceCardExpirationMonth" runat="server" DataSourceID="dsCardExpirationMonths" DataTextField="Name" DataValueField="Id"  SelectedValue='<%# Bind("CardExpirationMonth") %>'>
                            </asp:DropDownList>

                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:DropDownList ID="ddInvoiceCardExpirationMonth" runat="server" DataSourceID="dsCardExpirationMonths" DataTextField="Name" DataValueField="Id"  SelectedValue='<%# Bind("CardExpirationMonth") %>' Enabled="false">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Card Expiration Year" SortExpression="CardExpirationYear">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddInvoiceCardExpirationYear" runat="server" DataSourceID="dsCardExpirationYears" DataTextField="Name" DataValueField="Id"  SelectedValue='<%# Bind("CardExpirationYear") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:DropDownList ID="ddInvoiceCardExpirationYear" runat="server" DataSourceID="dsCardExpirationYears" DataTextField="Name" DataValueField="Id"  SelectedValue='<%# Bind("CardExpirationYear") %>' Enabled="false">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DELIVERY ADDRESS">
                        <ItemTemplate>
                            <hr />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="AddressHouseNumber" HeaderText="House Number" SortExpression="AddressHouseNumber" />
                    <asp:BoundField DataField="AddressStreetName" HeaderText="Street Name" SortExpression="AddressStreetName" />
                    <asp:TemplateField HeaderText="Street DesignationId" SortExpression="AddressStreetDesignationId">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddAddressStreetDesignationId" runat="server" DataSourceID="dsStreetDesignations" DataTextField="Name" DataValueField="Id"  SelectedValue='<%# Bind("AddressStreetDesignationId") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:DropDownList ID="ddAddressStreetDesignationId" runat="server" DataSourceID="dsStreetDesignations" DataTextField="Name" DataValueField="Id"  SelectedValue='<%# Bind("AddressStreetDesignationId") %>' Enabled="false">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="AddressAppartmentNumber" HeaderText="Appartment Number (optional)" SortExpression="AddressAppartmentNumber" />
                    <asp:BoundField DataField="AddressCity" HeaderText="City" SortExpression="AddressCity" />
                    <asp:TemplateField HeaderText="Province" SortExpression="AddressProvince">
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddAddressProvince" runat="server" DataSourceID="dsCanadaProvinces" DataTextField="Name" DataValueField="Id"  SelectedValue='<%# Bind("AddressProvince") %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:DropDownList ID="ddAddressProvince" runat="server" DataSourceID="dsCanadaProvinces" DataTextField="Name" DataValueField="Id"  SelectedValue='<%# Bind("AddressProvince") %>' Enabled="false">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="AddressPostalCode" HeaderText="Postal Code" SortExpression="AddressPostalCode" />
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:LinkButton ID="LinkButton1" Visible='<%# (_ShippingStatus == 0) %>' runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton2" Visible='<%# (_ShippingStatus == 0) %>' runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" Visible='<%# (_ShippingStatus == 0) %>' runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton2" Visible='<%# (_ShippingStatus == 0) %>' runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Fields>
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
            </asp:DetailsView>
            <asp:SqlDataSource ID="dsInvoice" runat="server" 
                ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
                SelectCommand="SELECT i.*, u.[LastName], u.[FirstName] FROM [Invoices] i INNER JOIN [USERS] u ON i.[UserId] = u.[Id] WHERE (CAST(i.[Id] AS NVARCHAR(MAX)) = @Id)" 
                DeleteCommand=
                    "DELETE FROM [InvoiceItems] WHERE ([InvoiceId] = @original_Id);
                
                    DELETE FROM [Invoices] WHERE [Id] = @original_Id;"                  
                UpdateCommand=
                    "UPDATE [Invoices] 
                    SET [CardTypeId] = @CardTypeId, [CardHolderName] = @CardHolderName, [CardNumber] = @CardNumber, [Card3Secutity] = @Card3Secutity, [CardExpirationMonth] = @CardExpirationMonth, [CardExpirationYear] = @CardExpirationYear, 
                    [AddressHouseNumber] = @AddressHouseNumber, [AddressStreetName] = @AddressStreetName, [AddressStreetDesignationId] = @AddressStreetDesignationId, [AddressAppartmentNumber] = @AddressAppartmentNumber, [AddressCity] = @AddressCity, [AddressProvince] = @AddressProvince, [AddressPostalCode] = @AddressPostalCode  
                    WHERE [Id] = @original_Id"
                OldValuesParameterFormatString="original_{0}" >
                <DeleteParameters>
                    <asp:Parameter Name="original_Id" Type="Int32" />
                </DeleteParameters>
                <SelectParameters>
                    <asp:Parameter DefaultValue="0" Name="Id" Type="String" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="CardTypeId" Type="Int32" />
                    <asp:Parameter Name="CardHolderName" Type="String" />
                    <asp:Parameter Name="CardNumber" Type="String" />
                    <asp:Parameter Name="Card3Secutity" Type="String" />
                    <asp:Parameter Name="CardExpirationMonth" Type="Int32" />
                    <asp:Parameter Name="CardExpirationYear" Type="Int32" />
                    <asp:Parameter Name="AddressHouseNumber" Type="String" />
                    <asp:Parameter Name="AddressStreetName" Type="String" />
                    <asp:Parameter Name="AddressStreetDesignationId" Type="Int32" />
                    <asp:Parameter Name="AddressAppartmentNumber" Type="String" />
                    <asp:Parameter Name="AddressCity" Type="String" />
                    <asp:Parameter Name="AddressProvince" Type="String" />
                    <asp:Parameter Name="AddressPostalCode" Type="String" />
                    <asp:Parameter Name="original_Id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>

            <div class="grid-layout-800">
                <asp:Label ID="lblErrorMessageInvoice" runat="server" Text="" CssClass="error-message"></asp:Label>
                <asp:Label ID="lblInfoMessageInvoice" runat="server" Text="" CssClass="info-message"></asp:Label>
            </div>
            <br />

            <asp:Panel ID="pnlInvoiceItems" runat="server">
                <h3 class="grid-layout-800">Items information</h3>
                <p class="grid-layout-800 my-comment">According to assignment requirement for the <b>Ordering Products</b> page: "For this assignment assume only 1 item can be placed on an order.". This means that each order (invoice) can contains only one product (invoice item).</p>
                <asp:GridView ID="grdInvoiceItems" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="dsInvoiceItems" GridLines="None" OnSelectedIndexChanged="grdInvoiceItems_SelectedIndexChanged" OnRowDeleted="grdInvoiceItems_RowDeleted" CssClass="order-items-grid">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="False" />
                        <asp:BoundField DataField="InvoiceId" HeaderText="Invoice Number" SortExpression="InvoiceId" ReadOnly="True" />
                        <asp:TemplateField HeaderText="Product" SortExpression="ProductId">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("ProductName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                        <asp:BoundField DataField="RetailPrice" DataFormatString="{0:c}" HeaderText="Price" SortExpression="RetailPrice" />
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" Visible='<%# (_ShippingStatus == 0) %>' runat="server" CausesValidation="False" CommandName="Select" Text="Edit"></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" Visible='<%# (_ShippingStatus == 0) %>' runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#7C6F57" />
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#E3EAEB" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                </asp:GridView>
                <asp:SqlDataSource ID="dsInvoiceItems" runat="server" 
                    ConflictDetection="CompareAllValues" 
                    ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
                    DeleteCommand=
                        "DELETE FROM [Invoices] WHERE [Id] IN (SELECT [InvoiceId] FROM [InvoiceItems] WHERE ([ID] = @original_Id));

                        DELETE FROM [InvoiceItems] WHERE [Id] = @original_Id;" 
                    SelectCommand="SELECT ii.*, p.[ProductName] FROM [InvoiceItems] ii INNER JOIN [Products] p ON ii.[ProductId] = p.[Id] WHERE (CAST(ii.[InvoiceId] AS NVARCHAR(MAX)) = @InvoiceId)" 
                    OldValuesParameterFormatString="original_{0}" >
                    <DeleteParameters>
                        <asp:Parameter Name="original_Id" Type="Int32" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtInvoiceId" DefaultValue="0" Name="InvoiceId" PropertyName="Text" Type="string" />
                    </SelectParameters>
                </asp:SqlDataSource>


                <asp:Panel ID="pnlInvoiceItem" runat="server">
                    <h3 class="grid-layout-400">Edit Item</h3>
                    <asp:DetailsView ID="dtlInvoiceItem" runat="server" AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="dsInvoiceItem" GridLines="None" EmptyDataText="Select an Item for editing." OnItemUpdated="dtlInvoiceItem_ItemUpdated" OnModeChanged="dtlInvoiceItem_ModeChanged" OnItemUpdating="dtlInvoiceItem_ItemUpdating" CssClass="order-item-details">
                        <AlternatingRowStyle BackColor="White" />
                        <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                        <EditRowStyle BackColor="#7C6F57" />
                        <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
                        <Fields>
                            <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="False" />
                            <asp:BoundField DataField="InvoiceId" HeaderText="Invoice Number" SortExpression="InvoiceId" ReadOnly="True" />
                            <asp:TemplateField HeaderText="Product" SortExpression="ProductId">
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddInvoiceItemProductId" runat="server" DataSourceID="dsProducts" DataTextField="ProductName" DataValueField="Id"  SelectedValue='<%# Bind("ProductId") %>' OnSelectedIndexChanged="ddInvoiceItemProductId_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ProductName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                            <asp:TemplateField HeaderText="Price" SortExpression="RetailPrice">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtInvoiceItemRetailPrice" runat="server" Text='<%# Bind("RetailPrice") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="txtInvoiceItemRetailPrice" runat="server" Text='<%# Bind("RetailPrice", "{0:c}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ShowEditButton="True" />
                        </Fields>
                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#E3EAEB" />
                    </asp:DetailsView>
                    <asp:SqlDataSource ID="dsInvoiceItem" runat="server" 
                        ConflictDetection="OverwriteChanges" 
                        ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
                        SelectCommand="SELECT ii.*, p.[ProductName] FROM [InvoiceItems] ii INNER JOIN [Products] p ON ii.[ProductId] = p.[Id] WHERE (ii.[Id] = @Id)" 
                        UpdateCommand=
                            "UPDATE [InvoiceItems] 
                            SET [ProductId] = @ProductId, [Quantity] = @Quantity, [RetailPrice] = @RetailPrice 
                            WHERE [Id] = @original_Id;

                            UPDATE [Invoices]
                            SET [BeforeTax] = (@RetailPrice  * @Quantity), [Tax] = (@RetailPrice  * @Quantity * 0.13), [Total] = (@RetailPrice  * @Quantity * 1.13)
                            WHERE [Id] IN (SELECT [InvoiceId] FROM [InvoiceItems] WHERE [Id] = @original_Id); "
                        OldValuesParameterFormatString="original_{0}">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="grdInvoiceItems" DefaultValue="0" Name="Id" PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="ProductId" Type="Int32" />
                            <asp:Parameter Name="Quantity" Type="Int32" />
                            <asp:Parameter Name="RetailPrice" Type="Decimal" />
                            <asp:Parameter Name="original_Id" Type="Int32" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <br />
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>

        <div class="grid-layout-800">
            <asp:Label ID="lblErrorMessageInvoiceItem" runat="server" Text="" CssClass="error-message"></asp:Label>
            <asp:Label ID="lblInfoMessageInvoiceItem" runat="server" Text="" CssClass="info-message"></asp:Label>
        </div>
    </div>
</asp:Content>
