﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administration/Admin.Master" AutoEventWireup="true" CodeBehind="Roles.aspx.cs" Inherits="HeliSound.Administration.Roles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="page-title">
            Roles
        </div>  
        <h3 class="grid-layout-800">List of available roles</h3>
        <p class="grid-layout-800 my-comment">Having <b>Roles</b> page with role management functional is decorative. Adding new role should means adding new functional to the web application (new pages). It is not just a record in the database.</p>
        <asp:GridView ID="grdRoles" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="dsRoles" GridLines="None" CssClass="role-grid">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="Role" HeaderText="Role" ReadOnly="True" SortExpression="Role" />
                <asp:BoundField DataField="Description" HeaderText="Description" ReadOnly="True" SortExpression="Description" />
                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                    <ItemTemplate>
                        <asp:CheckBox runat="server" checked='<%# Eval("Status") %>' Text="Active" Enabled="false"/>
                    </ItemTemplate>                    
                    <EditItemTemplate>
                        <asp:CheckBox ID="chbEditStatus" runat="server" Checked='<%# Bind("Status") %>' Text="Active" Enabled="true" ReadOnly="false" />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowEditButton="True" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsRoles" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
            OldValuesParameterFormatString="original_{0}" 
            SelectCommand="SELECT [Id], [Role], [Status], [Description] FROM [Roles] ORDER BY [Role]" 
            UpdateCommand="UPDATE [Roles] SET [Status] = @Status, [Description] = @Description  WHERE [Id] = @original_Id ">
            <DeleteParameters>
                <asp:Parameter Name="original_Id" Type="Int32" />
                <asp:Parameter Name="original_Role" Type="String" />
                <asp:Parameter Name="original_Status" Type="Int32" />
                <asp:Parameter Name="original_Description" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Role" Type="String" />
                <asp:Parameter Name="Status" Type="Int32" />
                <asp:Parameter Name="Description" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Role" Type="String" />
                <asp:Parameter Name="Status" Type="Int32" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="original_Id" Type="Int32" />
                <asp:Parameter Name="original_Role" Type="String" />
                <asp:Parameter Name="original_Status" Type="Int32" />
                <asp:Parameter Name="original_Description" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <h3 class="grid-layout-800">Add new role</h3>
        <asp:DetailsView ID="dtlRole" runat="server" AutoGenerateInsertButton="True" AutoGenerateRows="False"  DataKeyNames="Id" DataSourceID="dsRole" GridLines="None" OnItemInserted="dtlRole_ItemInserted" DefaultMode="Insert" OnItemInserting="dtlRole_ItemInserting" CssClass="role-details">
            <AlternatingRowStyle BackColor="White" />
            <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
            <EditRowStyle BackColor="#7C6F57" />
            <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
            <Fields>
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="Role" HeaderText="Role" SortExpression="Role" />
                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                    <InsertItemTemplate>
                        <asp:CheckBox ID="chbStatusInsert" runat="server" Checked='<%# Bind("Status") %>' Text="Active"/>
                    </InsertItemTemplate>
                </asp:TemplateField>
            </Fields>
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="dsRole" runat="server" 
            ConflictDetection="CompareAllValues" 
            ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
            DeleteCommand="DELETE FROM [Roles] WHERE [Id] = @original_Id AND [Role] = @original_Role AND (([Description] = @original_Description) OR ([Description] IS NULL AND @original_Description IS NULL)) AND [Status] = @original_Status" 
            InsertCommand="INSERT INTO [Roles] ([Role], [Description], [Status]) VALUES (@Role, @Description, @Status)"              
            SelectCommand="SELECT [Id], [Role], [Description], [Status] FROM [Roles] WHERE ([Id] = @Id)" 
            UpdateCommand="UPDATE [Roles] SET [Role] = @Role, [Description] = @Description, [Status] = @Status WHERE [Id] = @original_Id AND [Role] = @original_Role AND (([Description] = @original_Description) OR ([Description] IS NULL AND @original_Description IS NULL)) AND [Status] = @original_Status"
            OldValuesParameterFormatString="original_{0}">
            <DeleteParameters>
                <asp:Parameter Name="original_Id" Type="Int32" />
                <asp:Parameter Name="original_Role" Type="String" />
                <asp:Parameter Name="original_Description" Type="String" />
                <asp:Parameter Name="original_Status" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Role" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="Status" Type="Int32" />
            </InsertParameters>
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="Id" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="Role" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="Status" Type="Int32" />
                <asp:Parameter Name="original_Id" Type="Int32" />
                <asp:Parameter Name="original_Role" Type="String" />
                <asp:Parameter Name="original_Description" Type="String" />
                <asp:Parameter Name="original_Status" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <div class="grid-layout-800">
            <asp:Label ID="lblErrorMessage" runat="server" Text="" CssClass="error-message"></asp:Label>
            <asp:Label ID="lblInfoMessage" runat="server" Text="" CssClass="info-message"></asp:Label>
        </div>
    </div>
</asp:Content>
