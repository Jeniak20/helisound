﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administration/Admin.Master" AutoEventWireup="true" CodeBehind="Customers.aspx.cs" Inherits="HeliSound.Administration.Customers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>        
        <div class="page-title">
            Customers
        </div>

        <h3 class="grid-layout-800">Search customer</h3>
        <table class="customer-search">
            <tr>
                <td>
                    <span class="field-label">Search&nbsp;by&nbsp;Firs&nbsp;name,<br />Last&nbsp;name&nbsp;or&nbsp;Email</span>
                </td>
                <td>
                    <asp:TextBox ID="txtSearchByNames" runat="server" OnTextChanged="txtSearchByNames_TextChanged" CssClass="text-field"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnSearchByNames" runat="server" Text="Search" OnClick="btnSearchByNames_Click" CssClass="action-button" />
                </td>
            </tr>
            <tr>
                <td>
                    <span class="field-label">Search&nbsp;by&nbsp;customer&nbsp;Id</span>
                </td>
                <td>
                    <asp:TextBox ID="txtSearchById" runat="server" OnTextChanged="txtSearchById_TextChanged" CssClass="text-field"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnSearchById" runat="server" Text="Search" OnClick="btnSearchById_Click" CssClass="action-button" />
                </td>
            </tr>
        </table>

        <asp:Panel ID="pnlSearchResult" runat="server" Visible="false">

            <h3 class="grid-layout-800">Search result</h3>
            <asp:GridView ID="grdCustomers" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="dsCustomersByNames" GridLines="None" AllowSorting="True" EmptyDataText="There is not customers found by your search request." OnDataBound="grdCustomers_DataBound" OnSelectedIndexChanged="grdCustomers_SelectedIndexChanged" CssClass="customer-grid">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                    <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                    <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                    <asp:BoundField DataField="EmailAddress" HeaderText="Email" SortExpression="EmailAddress" />
                    <asp:BoundField DataField="PhoneNumber" HeaderText="Phone" SortExpression="PhoneNumber" />
                    <asp:BoundField DataField="Password" HeaderText="Password" SortExpression="Password" Visible="False" />
                    <asp:BoundField DataField="RoleId" HeaderText="RoleId" SortExpression="RoleId" Visible="False" />
                    <asp:CheckBoxField DataField="Status" HeaderText="Status" SortExpression="Status" Text="Active" />
                    <asp:CommandField ShowSelectButton="True" />
                </Columns>
                <EditRowStyle BackColor="#7C6F57" />
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <asp:SqlDataSource ID="dsCustomersByNames" runat="server"  
                SelectCommand=
                    "SELECT [Id], [FirstName], [LastName], [EmailAddress], [PhoneNumber], [Password], [RoleId], [Status] 
                    FROM [Users] 
                    WHERE [RoleId] = 3 AND
                    (UPPER([FirstName]) LIKE '%'+UPPER(@SearchString) +'%' 
                    OR UPPER([LastName]) LIKE '%'+UPPER(@SearchString) +'%' 
                    OR UPPER([EmailAddress]) LIKE '%'+UPPER(@SearchString) +'%')
                    ORDER BY [LastName], [FirstName]"
                ConnectionString="<%$ ConnectionStrings:HeliSound %>">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtSearchByNames" DefaultValue="" Name="SearchString" PropertyName="Text" />
                </SelectParameters>
            </asp:SqlDataSource>

            <asp:SqlDataSource ID="dsCustomersById" runat="server"  
                SelectCommand=
                    "SELECT [Id], [FirstName], [LastName], [EmailAddress], [PhoneNumber], [Password], [RoleId], [Status] 
                    FROM [Users] 
                    WHERE [RoleId] = 3 AND
                    CAST([Id] AS NVARCHAR(MAX)) = @SearchString
                    ORDER BY [LastName], [FirstName]"
                ConnectionString="<%$ ConnectionStrings:HeliSound %>">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtSearchById" DefaultValue="0" Name="SearchString" PropertyName="Text"/>
                </SelectParameters>
            </asp:SqlDataSource>

            <asp:Panel ID="pnlCustomerEditDelete" runat="server" Visible="false">
                <h3 class="grid-layout-800">Edit or delete customer</h3>
                <asp:DetailsView ID="dtlCustomer" runat="server" GridLines="None" AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="dsCustomer" OnItemDeleted="dtlCustomer_ItemDeleted" OnItemUpdated="dtlCustomer_ItemUpdated" OnItemUpdating="dtlCustomer_ItemUpdating" CssClass="customer-details" OnModeChanged="dtlCustomer_ModeChanged">
                    <AlternatingRowStyle BackColor="White" />
                    <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
                    <EditRowStyle BackColor="#7C6F57" />
                    <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
                    <Fields>
                        <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                        <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                        <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                        <asp:BoundField DataField="EmailAddress" HeaderText="Email" SortExpression="EmailAddress" />
                        <asp:BoundField DataField="PhoneNumber" HeaderText="Phone" SortExpression="PhoneNumber" />
                        <asp:TemplateField HeaderText="Password" SortExpression="Password">
                            <EditItemTemplate>
                                <br />
                                <span>Enter password (optional)</span>
                                <asp:TextBox ID="txtUserPassword1" runat="server" Text="" TextMode="Password"></asp:TextBox>
                                <br />
                                <span>Confirm password (optional)</span>
                                <asp:TextBox ID="txtUserPassword2" runat="server" Text="" TextMode="Password"></asp:TextBox>
                                <br />
                                <br />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" Text="********"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="RoleId" HeaderText="RoleId" SortExpression="RoleId" Visible="False" />
                        <asp:TemplateField HeaderText="Status" SortExpression="Status">
                            <EditItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" />
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" />
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Text="Active" Enabled="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    </Fields>
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#E3EAEB" />
                </asp:DetailsView>

                <asp:SqlDataSource ID="dsCustomer" runat="server" 
                    ConflictDetection="CompareAllValues" 
                    ConnectionString="<%$ ConnectionStrings:HeliSound %>" 
                    OldValuesParameterFormatString="original_{0}" 
                    DeleteCommand="UPDATE [Users] SET [Status] = 0 WHERE [Id] = @original_Id" 
                    SelectCommand="SELECT [Id], [FirstName], [LastName], [EmailAddress], [PhoneNumber], [Password], [RoleId], [Status] FROM [Users] WHERE ([Id] = @Id)" 
                    UpdateCommand="UPDATE [Users] SET [FirstName] = @FirstName, [LastName] = @LastName, [EmailAddress] = @EmailAddress, [PhoneNumber] = @PhoneNumber, [Password] = ISNULL(@Password, [Password]), [RoleId] = ISNULL(@RoleId, [RoleId]), [Status] = @Status WHERE [Id] = @original_Id">
                    <DeleteParameters>
                        <asp:Parameter Name="original_Id" Type="Int32" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="grdCustomers" DefaultValue="0" Name="Id" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="FirstName" Type="String" />
                        <asp:Parameter Name="LastName" Type="String" />
                        <asp:Parameter Name="EmailAddress" Type="String" />
                        <asp:Parameter Name="PhoneNumber" Type="String" />
                        <asp:Parameter Name="Password" Type="String" />
                        <asp:Parameter Name="RoleId" Type="Int32" />
                        <asp:Parameter Name="Status" Type="Boolean" />
                        <asp:Parameter Name="original_Id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </asp:Panel>
        </asp:Panel>

        <div class="grid-layout-800">
            <asp:Label ID="lblErrorMessage" runat="server" Text="" CssClass="error-message"></asp:Label>
            <asp:Label ID="lblInfoMessage" runat="server" Text="" CssClass="error-message"></asp:Label>
        </div>
    </div>
</asp:Content>
