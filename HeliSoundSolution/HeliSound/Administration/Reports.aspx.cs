﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Administration
{
    public partial class Reports : System.Web.UI.Page
    {
        public DateTime ReportDateRangeFrom {
            get {
                switch (rbReportDateRange.SelectedValue)
                {                    
                    case "Yesterday":
                        return DateTime.Today.AddDays(-1);
                    case "ThisWeek": // monday on this week                        
                        return DateTime.Today.AddDays(1-(int)DateTime.Today.DayOfWeek);
                    case "LastWeek": // monday on last week
                        return DateTime.Today.AddDays(-6 - (int)DateTime.Today.DayOfWeek);
                    case "SpecificDateRange":
                        return calFrom.SelectedDate;
                    default: // this is going to be "Taday"
                        return DateTime.Today;
                }
            }
        }
        public DateTime ReportDateRangeTo
        {
            get
            {
                switch (rbReportDateRange.SelectedValue)
                {
                    case "Yesterday":
                        return DateTime.Today.AddDays(-1);
                    case "ThisWeek": // comming sunday                       
                        return DateTime.Today.AddDays(7 - (int)DateTime.Today.DayOfWeek);
                    case "LastWeek": // sunday on last week
                        return DateTime.Today.AddDays(0 - (int)DateTime.Today.DayOfWeek);
                    case "SpecificDateRange":
                        return calTo.SelectedDate;
                    default: // this is going to be "Taday"
                        return DateTime.Today;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                calFrom.SelectedDate = DateTime.Today.AddDays(-6);
                calTo.SelectedDate = DateTime.Today;
                UpdateUI();
            }
        }

        protected void rbReportDateRange_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlDateRange.Visible = rbReportDateRange.SelectedValue.Equals("SpecificDateRange");
            UpdateUI();
        }

        protected void calFrom_SelectionChanged(object sender, EventArgs e)
        {
            UpdateUI();
        }

        protected void calTo_SelectionChanged(object sender, EventArgs e)
        {
            UpdateUI();
        }

        protected void grdReport_DataBound(object sender, EventArgs e)
        {
            dtlReport.Visible = grdReport.Rows.Count > 0;
        }

        //------------------------------------------------------------------------------------

        private void UpdateUI()
        {
            lblFrom.Text = ReportDateRangeFrom.ToString("MMMM dd, yyyy");
            lblTo.Text = ReportDateRangeTo.ToString("MMMM dd, yyyy");
            dsReport.SelectParameters["DateCreationFrom"].DefaultValue = ReportDateRangeFrom.ToString("MMMM dd, yyyy");
            dsReport.SelectParameters["DateCreationTo"].DefaultValue = ReportDateRangeTo.ToString("MMMM dd, yyyy");
            dsReportSum.SelectParameters["DateCreationFrom"].DefaultValue = ReportDateRangeFrom.ToString("MMMM dd, yyyy");
            dsReportSum.SelectParameters["DateCreationTo"].DefaultValue = ReportDateRangeTo.ToString("MMMM dd, yyyy");
            grdReport.DataBind();
            dtlReport.DataBind();
        }
    }
}