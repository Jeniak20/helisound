﻿using HeliSound.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound.Administration
{
    public partial class Products : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void btnAddProduct_Click(object sender, EventArgs e)
        {
            dtlProduct.ChangeMode(DetailsViewMode.Insert);
        }

        protected void dtlProduct_DataBound(object sender, EventArgs e)
        {
            btnAddProduct.Visible = (dtlProduct.DataItemCount == 0) && (dtlProduct.CurrentMode != DetailsViewMode.Insert);
        }

        protected void dtlProduct_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            grdProducts.DataBind();
        }

        protected void dtlProduct_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            grdProducts.DataBind();
        }

        protected void dtlProduct_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
        {
            grdProducts.DataBind();
        }

        protected void dtlProduct_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            var message = ValidateForm(e.Values, 0);
            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                e.Cancel = true;
                return;
            };
        }

        protected void dtlProduct_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            var id = (int)grdProducts.SelectedValue;
            var message = ValidateForm(e.NewValues, id);

            if (!string.IsNullOrWhiteSpace(message))
            {
                // Invalid data
                DisplayMessage(message, true);
                e.Cancel = true;
                return;
            };
        }

        protected void ddChooseSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlProducts.Visible = !ddChooseSupplier.SelectedValue.Equals("0");
            grdProducts.SelectedIndex = -1;
            dtlProduct.ChangeMode(DetailsViewMode.ReadOnly);
            dtlProduct.DataBind();
        }
        protected void grdProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtlProduct.ChangeMode(DetailsViewMode.ReadOnly);
        }
        protected void dtlProduct_ModeChanged(object sender, EventArgs e)
        {
            DisplayMessage(string.Empty, false);
        }
        //-------------------------------------------------------------------------------

        private void DisplayMessage(string message, bool isError)
        {
            lblErrorMessage.Text = string.Empty;
            lblInfoMessage.Text = string.Empty;
            if (isError)
            {
                lblErrorMessage.Text = message;
            }
            else
            {
                lblInfoMessage.Text = message;
            }
        }

        private string ValidateForm(IOrderedDictionary values, int updatingId)
        {
            // [ProductName] NVARCHAR (128)
            if ((values["ProductName"] == null) || (values["ProductName"].ToString().Length < 3))
            {
                return "Product name is too short (required minimum 3 characters).";
            }
            if (values["ProductName"].ToString().Length > 124)
            {
                return "Product name is too long (maximum 124 characters).";
            }
            // [SupplierId]  INT
            if ((values["SupplierId"] == null) || (values["SupplierId"].ToString().Trim().Equals("0")))
            {
                return "Suppliyer is required.";
            }
            // [CategoryId]  INT
            if ((values["CategoryId"] == null) || (values["CategoryId"].ToString().Trim().Equals("0")))
            {
                return "Category is required.";
            }
            // [RetailPrice] DECIMAL (18, 4)
            if ((values["RetailPrice"] == null) || string.IsNullOrWhiteSpace(values["RetailPrice"].ToString()))
            {
                return "Product price is required.";
            }
            try
            {
                var price = Convert.ToDecimal(values["RetailPrice"].ToString());
                if ((price <= 0.01M) || (price > 10000M))
                {
                    return "Product price must be between $0.01 and $10,000.00 .";
                }
            }
            catch
            {
                return "Wrong product price.";
            }
            // Existing product
            var existingId = StoreProvider.ProductIdByProductNameSupplierIdCategoryIdGet(
                values["ProductName"].ToString().Trim(),
                Convert.ToInt32(values["SupplierId"].ToString()),
                Convert.ToInt32(values["CategoryId"].ToString()));
            if ((existingId > 0) && (updatingId != existingId))
            {
                return "Specified product name with the same suppliyer and category already exists.";
            }
            return string.Empty;
        }
    }
}