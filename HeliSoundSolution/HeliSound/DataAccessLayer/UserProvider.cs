﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HeliSound.Models;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace HeliSound.DataAccessLayer
{
    public static class UserProvider
    {
        static string DBConnectionString = ConfigurationManager.ConnectionStrings["HeliSound"].ConnectionString;
        static SqlConnection Connection = new SqlConnection(DBConnectionString);

        //--------------------------------- Public methods ---------------------------------
        public static User UserIdentityCheck(string emailAddress, string password)
        {
            // the password parameter is hashed already
            var dAdapter = new SqlDataAdapter("UserIdentityCheck", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@EmailAddress", emailAddress);
            dAdapter.SelectCommand.Parameters.AddWithValue("@Password", password);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return ParceUserData(dSet.Tables[0].Rows[0]);
            }
            catch
            {
                return null;
            }
            finally
            {
                Connection.Close();
            }
        }

        public static int UserAdd(User user)
        {
            var dAdapter = new SqlDataAdapter("UserAdd", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@FirstName", user.FirstName);
            dAdapter.SelectCommand.Parameters.AddWithValue("@LastName", user.LastName);
            dAdapter.SelectCommand.Parameters.AddWithValue("@EmailAddress", user.EmailAddress);
            dAdapter.SelectCommand.Parameters.AddWithValue("@PhoneNumber", user.PhoneNumber);
            dAdapter.SelectCommand.Parameters.AddWithValue("@Password", user.Password);
            dAdapter.SelectCommand.Parameters.AddWithValue("@RoleId", user.RoleId);
            dAdapter.SelectCommand.Parameters.AddWithValue("@Status", user.Status);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return Convert.ToInt32(dSet.Tables[0].Rows[0]["Id"].ToString());
            }
            catch
            {
                return 0;
            }
            finally
            {
                Connection.Close();
            }
        }

        public static int SecretAnswerAdd(SecretAnswer answer)
        {
            var dAdapter = new SqlDataAdapter("SecretAnswerAdd", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@UserId", answer.UserId);
            dAdapter.SelectCommand.Parameters.AddWithValue("@QuestionId", answer.QuestionId);
            dAdapter.SelectCommand.Parameters.AddWithValue("@Answer", answer.Answer);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return Convert.ToInt32(dSet.Tables[0].Rows[0]["Id"].ToString());
            }
            catch
            {
                return 0;
            }
            finally
            {
                Connection.Close();
            }
        }

        public static int UserIdByEmailAddressGet(string emailAddress)
        {
            var dAdapter = new SqlDataAdapter("UserIdByEmailAddressGet", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@EmailAddress", emailAddress);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return Convert.ToInt32(dSet.Tables[0].Rows[0]["Id"].ToString());
            }
            catch
            {
                return 0;
            }
            finally
            {
                Connection.Close();
            }
        }

        public static User UserByEmailAddressGet(string emailAddress)
        {
            var dAdapter = new SqlDataAdapter("UserByEmailAddressGet", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@EmailAddress", emailAddress);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return ParceUserData(dSet.Tables[0].Rows[0]);
            }
            catch
            {
                return null;
            }
            finally
            {
                Connection.Close();
            }
        }


        public static string SecretQuestionByUserEmailAddressGet(string emailAddress)
        {
            var dAdapter = new SqlDataAdapter("SecretQuestionByUserEmailAddressGet", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@EmailAddress", emailAddress);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return dSet.Tables[0].Rows[0]["Question"].ToString();
            }
            catch
            {
                return string.Empty;
            }
            finally
            {
                Connection.Close();
            }
        }

        
        public static int UserSecretAnswerCheck(string emailAddress, string answer)
        {
            var dAdapter = new SqlDataAdapter("UserSecretAnswerCheck", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@EmailAddress", emailAddress);
            dAdapter.SelectCommand.Parameters.AddWithValue("@Answer", answer);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return Convert.ToInt32(dSet.Tables[0].Rows[0]["Id"].ToString());
            }
            catch
            {
                return 0;
            }
            finally
            {
                Connection.Close();
            }
        }

        
        public static int UserPasswordUpdate(string emailAddress, string Password)
        {
            var dAdapter = new SqlDataAdapter("UserPasswordUpdate", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@EmailAddress", emailAddress);
            dAdapter.SelectCommand.Parameters.AddWithValue("@Password", Password);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return Convert.ToInt32(dSet.Tables[0].Rows[0]["Id"].ToString());
            }
            catch
            {
                return 0;
            }
            finally
            {
                Connection.Close();
            }
        }

        public static int RoleIdByRoleNameGet(string role)
        {
            var dAdapter = new SqlDataAdapter("RoleIdByRoleNameGet", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@Role", role);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return Convert.ToInt32(dSet.Tables[0].Rows[0]["Id"].ToString());
            }
            catch
            {
                return 0;
            }
            finally
            {
                Connection.Close();
            }
        }

        public static int[] ActiveAdministratorIdsGet()
        {
            var dAdapter = new SqlDataAdapter("SELECT [Id] FROM [Users] WHERE [RoleId] = 1 AND [Status] = 1", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.Text;
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return ParceActiveAdministratorIds(dSet.Tables[0].Rows); //Convert.ToInt32(dSet.Tables[0].Rows[0]["Id"].ToString());
            }
            catch
            {
                return new int[0];
            }
            finally
            {
                Connection.Close();
            }
        }
        
        //--------------------------------- Private methods ---------------------------------

        private static User ParceUserData(DataRow dataRow)
        {
            try
            {
                var user = new User
                {
                    Id = Convert.ToInt32(dataRow["Id"].ToString()),
                    FirstName = dataRow["FirstName"].ToString(),
                    LastName = dataRow["LastName"].ToString(),
                    EmailAddress = dataRow["EmailAddress"].ToString(),
                    PhoneNumber = dataRow["PhoneNumber"].ToString(),
                    RoleId = Convert.ToInt32(dataRow["RoleId"].ToString()),
                    Status = Convert.ToBoolean(dataRow["Status"].ToString()),
                    Role = new Role
                    {
                        Id = Convert.ToInt32(dataRow["RoleId"].ToString()),
                        RoleName = dataRow["Role"].ToString()
                    }
                };
                return user;
            }
            catch
            {
                return null;
            }
            
        }

        private static int[] ParceActiveAdministratorIds(DataRowCollection rows)
        {
            try
            {
                var result = new int[rows.Count];
                for (int i = 0; i < rows.Count; i++)
                {
                    result[i] = Convert.ToInt32(rows[i]["Id"].ToString());
                }

                return result;
            }
            catch
            {
                return new int[0];
            }
        }
    }
}