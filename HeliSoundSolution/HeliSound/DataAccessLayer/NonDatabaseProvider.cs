﻿using HeliSound.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HeliSound.DataAccessLayer
{
    public static class NonDatabaseProvider
    {

        public static Generic[] ShippingStatusesGet()
        {
            return shippingStatuses.Where(i => !i.IsEmptyOption).ToArray();
        }
        public static Generic[] ShippingStatusesWithEmptyOptionGet()
        {
            return shippingStatuses;
        }

        public static Generic[] CardExpirationMonthsGet()
        {
            return CardExpirationMonths.Where(i => !i.IsEmptyOption).ToArray();
        }
        public static Generic[] CardExpirationMonthsWithEmptyOptionGet()
        {
            return CardExpirationMonths;
        }

        public static Generic[] CardExpirationYearsGet()
        {
            return CardExpirationYears.Where(i => !i.IsEmptyOption).ToArray();
        }
        public static Generic[] CardExpirationYearsWithEmptyOptionGet()
        {
            return CardExpirationYears;
        }

        public static Generic[] CanadaProvincesGet()
        {
            return CanadaProvinces.Where(i => !i.IsEmptyOption).ToArray();
        }
        public static Generic[] CanadaProvincesWithEmptyOptionGet()
        {
            return CanadaProvinces;
        }

        public static Generic[] StreetDesignationsGet()
        {
            return StreetDesignations.Where(i => !i.IsEmptyOption).ToArray();
        }
        public static Generic[] StreetDesignationsWithEmptyOptionGet()
        {
            return StreetDesignations;
        }

        public static Generic[] CardTypesGet()
        {
            return CardTypes.Where(i => !i.IsEmptyOption).ToArray();
        }
        public static Generic[] CardTypesWithEmptyOptionGet()
        {
            return CardTypes;
        }

        //-----------------------------------------------------------------------------------

        private static Generic[] shippingStatuses = new Generic[] {
            new Generic { Id = string.Empty, Name = "Select shipping status", IsEmptyOption = true },
            new Generic { Id =  "0", Name = "Pending", IsEmptyOption = false },
            new Generic { Id =  "1", Name = "Shipped", IsEmptyOption = false }
        };

        private static Generic[] CardExpirationMonths = new Generic[] {
            new Generic { Id = string.Empty, Name = "Select expiration month", IsEmptyOption = true },
            new Generic { Id =  "1", Name = "January", IsEmptyOption = false },
            new Generic { Id =  "2", Name = "February", IsEmptyOption = false },
            new Generic { Id =  "3", Name = "March", IsEmptyOption = false },
            new Generic { Id =  "4", Name = "April", IsEmptyOption = false },
            new Generic { Id =  "5", Name = "May", IsEmptyOption = false },
            new Generic { Id =  "6", Name = "June", IsEmptyOption = false },
            new Generic { Id =  "7", Name = "July", IsEmptyOption = false },
            new Generic { Id =  "8", Name = "August", IsEmptyOption = false },
            new Generic { Id =  "9", Name = "September", IsEmptyOption = false },
            new Generic { Id = "10", Name = "October", IsEmptyOption = false },
            new Generic { Id = "11", Name = "November", IsEmptyOption = false },
            new Generic { Id = "12", Name = "December", IsEmptyOption = false }
        };

        private static Generic[] CardExpirationYears = new Generic[] {
            new Generic { Id =   string.Empty, Name = "Select expiration year", IsEmptyOption = true },
            new Generic { Id = "2017", Name = "2017", IsEmptyOption = false },
            new Generic { Id = "2018", Name = "2018", IsEmptyOption = false },
            new Generic { Id = "2019", Name = "2019", IsEmptyOption = false },
            new Generic { Id = "2020", Name = "2020", IsEmptyOption = false },
            new Generic { Id = "2021", Name = "2021", IsEmptyOption = false },
            new Generic { Id = "2022", Name = "2022", IsEmptyOption = false },
            new Generic { Id = "2023", Name = "2023", IsEmptyOption = false },
            new Generic { Id = "2024", Name = "2024", IsEmptyOption = false },
            new Generic { Id = "2025", Name = "2025", IsEmptyOption = false },
            new Generic { Id = "2026", Name = "2026", IsEmptyOption = false }
        };

        private static Generic[] CanadaProvinces = new Generic[] {
            new Generic { Id = string.Empty,                        Name = "Select province", IsEmptyOption = true },
            new Generic { Id = "Alberta",                   Name = "Alberta", IsEmptyOption = false },
            new Generic { Id = "British Columbia",          Name = "British Columbia", IsEmptyOption = false },
            new Generic { Id = "Manitoba",                  Name = "Manitoba", IsEmptyOption = false },
            new Generic { Id = "New Brunswick",             Name = "New Brunswick", IsEmptyOption = false },
            new Generic { Id = "Newfoundland and Labrador", Name = "Newfoundland and Labrador", IsEmptyOption = false },
            new Generic { Id = "Nova Scotia",               Name = "Nova Scotia", IsEmptyOption = false },
            new Generic { Id = "Ontario",                   Name = "Ontario", IsEmptyOption = false },
            new Generic { Id = "Prince Edward Island",      Name = "Prince Edward Island", IsEmptyOption = false },
            new Generic { Id = "Quebec",                    Name = "Quebec", IsEmptyOption = false },
            new Generic { Id = "Saskatchewan",              Name = "Saskatchewan", IsEmptyOption = false }
        };

        private static Generic[] StreetDesignations = new Generic[] {
            new Generic { Id = string.Empty, Name = "Select street designation", IsEmptyOption = true },
            new Generic { Id = "1", Name = "Street", IsEmptyOption = false },
            new Generic { Id = "2", Name = "Avenue", IsEmptyOption = false },
            new Generic { Id = "3", Name = "Court", IsEmptyOption = false },
            new Generic { Id = "4", Name = "Circle", IsEmptyOption = false },
            new Generic { Id = "5", Name = "Drive", IsEmptyOption = false }
        };

        private static Generic[] CardTypes = new Generic[] {
            new Generic { Id = string.Empty, Name = "Select card type", IsEmptyOption = true },
            new Generic { Id = "1", Name = "Visa", IsEmptyOption = false },
            new Generic { Id = "2", Name = "MasterCard", IsEmptyOption = false },
            new Generic { Id = "3", Name = "American Express", IsEmptyOption = false }
        };
    }
}