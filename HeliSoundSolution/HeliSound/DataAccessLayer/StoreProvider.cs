﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HeliSound.Models;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace HeliSound.DataAccessLayer
{
    public static class StoreProvider
    {
        static string DBConnectionString = ConfigurationManager.ConnectionStrings["HeliSound"].ConnectionString;
        static SqlConnection Connection = new SqlConnection(DBConnectionString);

        //--------------------------------- Public methods ---------------------------------
        public static int SupplierIdByCompanyNameGet(string companyName)
        {
            var dAdapter = new SqlDataAdapter("SupplierIdByCompanyNameGet", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@CompanyName", companyName);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return Convert.ToInt32(dSet.Tables[0].Rows[0]["Id"].ToString());
            }
            catch
            {
                return 0;
            }
            finally
            {
                Connection.Close();
            }
        }

        public static int CategoryIdByCategoryDescriptionGet(string categoryDescription)
        {
            var dAdapter = new SqlDataAdapter("CategoryIdByCategoryDescriptionGet", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@CategoryDescription", categoryDescription);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return Convert.ToInt32(dSet.Tables[0].Rows[0]["Id"].ToString());
            }
            catch
            {
                return 0;
            }
            finally
            {
                Connection.Close();
            }
        }

        
        public static int ProductIdByProductNameSupplierIdCategoryIdGet(string productName, int supplierId, int categoryId)
        {
            var dAdapter = new SqlDataAdapter("ProductIdByProductNameSupplierIdCategoryIdGet", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@ProductName", productName);
            dAdapter.SelectCommand.Parameters.AddWithValue("@SupplierId", supplierId);
            dAdapter.SelectCommand.Parameters.AddWithValue("@CategoryId", categoryId);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return Convert.ToInt32(dSet.Tables[0].Rows[0]["Id"].ToString());
            }
            catch
            {
                return 0;
            }
            finally
            {
                Connection.Close();
            }
        }

        
        public static int ProductIdBySupplierIdCount(int supplierId)
        {
            var dAdapter = new SqlDataAdapter("ProductIdBySupplierIdCount", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@SupplierId", supplierId);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return Convert.ToInt32(dSet.Tables[0].Rows[0][0].ToString());
            }
            catch
            {
                return 0;
            }
            finally
            {
                Connection.Close();
            }
        }

        public static int ProductIdByCategoryIdCount(int categoryId)
        {
            var dAdapter = new SqlDataAdapter("ProductIdByCategoryIdCount", Connection);
            dAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            dAdapter.SelectCommand.Parameters.AddWithValue("@CategoryId", categoryId);
            var dSet = new DataSet();
            try
            {
                dAdapter.SelectCommand.Prepare();
                dAdapter.Fill(dSet);
                return Convert.ToInt32(dSet.Tables[0].Rows[0][0].ToString());
            }
            catch
            {
                return 0;
            }
            finally
            {
                Connection.Close();
            }
        }
        //--------------------------------- Private methods ---------------------------------


    }
}