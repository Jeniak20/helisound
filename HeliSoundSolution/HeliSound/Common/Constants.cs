﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HeliSound.Common
{
    public static class Constants
    {
        public static class SessionKeys
        {
            public static string LOGGED_USER = Guid.NewGuid().ToString();
        }

        public static Dictionary<string, string> ShippingStatuses = new Dictionary<string, string>
        {
            {"0", "Pending"},
            {"1", "Shipped"}
        };

        public static Dictionary<string, string> GetShippingStatuses()
        {
            return ShippingStatuses;
        }

        public static string[] GetShippingStatuses2()
        {
             return new string[] { "Pending", "Shipped" };
        }
        //public static class ShippingStatuses
        //{
        //    Dictionary<string, string>
        //}

    }
}