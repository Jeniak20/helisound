﻿using HeliSound.Common;
using HeliSound.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HeliSound
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && (HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // get info from auth cookie about currently logged user
                HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

                // try to load user's ingo from database
                var user = UserProvider.UserByEmailAddressGet(ticket.Name);
                if (user == null)
                {
                    // if user's email is not correct, then logout currently logged user
                    FormsAuthentication.SignOut();
                    Response.Redirect(Request.Url.ToString());
                    return;
                }
                
                // save users ingo in session
                Session.Add(Constants.SessionKeys.LOGGED_USER, user);

                // Redirect to default page (depending on role) 
                string returnUrl = "/";
                switch (ticket.UserData)
                {
                    case "Administrators":
                        returnUrl = "~/Administration/UserMaintenance.aspx";
                        break;
                    case "Shipping":
                        returnUrl = "~/Shipping/Shipping.aspx";
                        break;
                    case "Customers":
                        returnUrl = "~/Customers/OrderProducts.aspx";
                        break;
                };

                Response.Redirect(returnUrl);
            }
        }
    }
}