﻿CREATE TABLE [dbo].[SecretQuestions]
(
	[Id] INT NOT NULL IDENTITY, 
    [Question] NVARCHAR(MAX) NOT NULL, 
    CONSTRAINT [PK_SecretQuestions] PRIMARY KEY ([Id]) 
)
