﻿CREATE TABLE [dbo].[Suppliers] (
    [Id]                           INT            IDENTITY (1, 1) NOT NULL,
    [CompanyName]                  NVARCHAR (64)  NOT NULL,
    [CompanyAddress]                NVARCHAR (256) NOT NULL,
    [ContactPersonName]            NVARCHAR (64)  NOT NULL,
    [ContactPersonTelephoneNumber] NVARCHAR (64)  NOT NULL,
    [Status]                       BIT            DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([CompanyName] ASC)
);

