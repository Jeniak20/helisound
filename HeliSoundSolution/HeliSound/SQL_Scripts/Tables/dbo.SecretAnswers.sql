﻿CREATE TABLE [dbo].[SecretAnswers]
(
	[Id] INT NOT NULL IDENTITY, 
	[UserId] INT NOT NULL, 
	[QuestionId] INT NOT NULL, 
    [Answer] NVARCHAR(MAX) NOT NULL, 
    CONSTRAINT [PK_SecretAnswers] PRIMARY KEY ([Id]) 
)