﻿CREATE TABLE [dbo].[Users] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]    NVARCHAR (64)  NULL,
    [LastName]     NVARCHAR (64)  NULL,
    [EmailAddress] NVARCHAR (128) NOT NULL UNIQUE,
    [PhoneNumber]  NVARCHAR (64)  NULL,
    [Password]     NVARCHAR (128) NOT NULL,
    [RoleId]       INT            NOT NULL,
    [Status]       BIT            NOT NULL DEFAULT 0,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([EmailAddress] ASC)
);

