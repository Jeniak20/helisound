﻿CREATE TABLE [dbo].[InvoiceItems] (
    [Id]							INT				IDENTITY (1, 1) NOT NULL,
    [InvoiceId]						INT				NOT NULL,
    [ProductId]						INT				NOT NULL,
    [Quantity]						INT				DEFAULT ((1)) NOT NULL,
    [RetailPrice]					DECIMAL(18,4)	NOT NULL,
    [Status]						BIT				DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
