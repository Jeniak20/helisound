﻿CREATE TABLE [dbo].[Categories] (
    [Id]                           INT            IDENTITY (1, 1) NOT NULL,
    [CategoryDescription]          NVARCHAR (64)  NOT NULL,
    [Status]                       BIT            DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([CategoryDescription] ASC)
);