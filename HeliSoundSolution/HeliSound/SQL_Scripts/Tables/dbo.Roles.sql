﻿CREATE TABLE [dbo].[Roles] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [Role]        NVARCHAR (64)   NOT NULL,
    [Description] NVARCHAR (1024) NULL,
    [Status]      BIT             DEFAULT (0) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

