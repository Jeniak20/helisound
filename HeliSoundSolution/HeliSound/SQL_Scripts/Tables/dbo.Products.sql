﻿CREATE TABLE [dbo].[Products] (
    [Id]							INT				IDENTITY (1, 1) NOT NULL,
    [ProductName]					NVARCHAR (128)	NOT NULL,
    [SupplierId]					INT				NOT NULL,
    [CategoryId]					INT				NOT NULL,
    [RetailPrice]					DECIMAL(18,4)	NOT NULL,
    [Status]						BIT				DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
