﻿CREATE PROCEDURE [dbo].[SecretQuestionByUserEmailAddressGet]

	@EmailAddress NVARCHAR(128) = ''

AS
BEGIN

	SELECT sq.Question
	FROM [Users] u 
	INNER JOIN [SecretAnswers] sa ON u.Id = sa.UserId
	INNER JOIN [SecretQuestions] sq ON sa.QuestionId = sq.Id
	WHERE UPPER(u.EmailAddress) = UPPER(@EmailAddress)

END