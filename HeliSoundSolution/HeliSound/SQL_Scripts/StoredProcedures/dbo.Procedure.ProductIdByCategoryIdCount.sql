﻿CREATE PROCEDURE [dbo].[ProductIdByCategoryIdCount]

	@CategoryId INT = 0

AS
BEGIN

	SELECT COUNT([Id])
	FROM [Products]
	WHERE [CategoryId] = @CategoryId

END