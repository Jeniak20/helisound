﻿CREATE PROCEDURE [dbo].[RoleIdByRoleNameGet]

	@Role NVARCHAR(64) = ''

AS
BEGIN

	SELECT [Id]
	FROM [Roles]
	WHERE UPPER([Role]) = UPPER(@Role)

END