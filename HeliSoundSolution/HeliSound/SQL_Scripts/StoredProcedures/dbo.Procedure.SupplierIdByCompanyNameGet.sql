﻿CREATE PROCEDURE [dbo].[SupplierIdByCompanyNameGet]

	@CompanyName NVARCHAR(64) = ''

AS
BEGIN

	SELECT [Id]
	FROM [Suppliers]
	WHERE UPPER([CompanyName]) = UPPER(@CompanyName)

END