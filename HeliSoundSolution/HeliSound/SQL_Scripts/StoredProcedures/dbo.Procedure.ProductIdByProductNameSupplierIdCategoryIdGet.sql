﻿CREATE PROCEDURE [dbo].[ProductIdByProductNameSupplierIdCategoryIdGet]

	@ProductName NVARCHAR(128) = '',
	@SupplierId INT = 0,
	@CategoryId INT = 0

AS
BEGIN

	SELECT [Id]
	FROM [Products]
	WHERE UPPER([ProductName]) = UPPER(@ProductName)
	AND [SupplierId] = @SupplierId
	AND [CategoryId] = @CategoryId

END