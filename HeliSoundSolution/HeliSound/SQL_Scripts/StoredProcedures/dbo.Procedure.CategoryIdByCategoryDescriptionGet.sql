﻿CREATE PROCEDURE [dbo].[CategoryIdByCategoryDescriptionGet]

	@CategoryDescription NVARCHAR(64) = ''

AS
BEGIN

	SELECT [Id]
	FROM [Categories]
	WHERE UPPER([CategoryDescription]) = UPPER(@CategoryDescription)

END