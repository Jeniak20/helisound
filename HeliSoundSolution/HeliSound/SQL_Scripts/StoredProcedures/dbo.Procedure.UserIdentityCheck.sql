﻿CREATE PROCEDURE [dbo].[UserIdentityCheck]

	@EmailAddress NVARCHAR(128) = '',
	@Password NVARCHAR(128) = ''

AS
BEGIN

	SELECT u.Id, u.FirstName, u.LastName, u.EmailAddress, u.PhoneNumber, u.RoleId, u.[Status], r.[Role]
	FROM Users u INNER JOIN Roles r ON u.RoleId = r.Id
	WHERE UPPER(u.EmailAddress) = UPPER(@EmailAddress) AND u.[Password] = @Password AND u.[Status] = 1

END