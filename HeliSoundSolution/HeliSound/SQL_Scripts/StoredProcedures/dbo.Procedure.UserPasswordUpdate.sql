﻿CREATE PROCEDURE [dbo].[UserPasswordUpdate]

	@EmailAddress NVARCHAR(128) = '',
	@Password NVARCHAR(128) = ''

AS
BEGIN

	UPDATE [dbo].[Users]
	SET [Password] = ISNULL(@Password, [Password])
	WHERE UPPER(EmailAddress) = UPPER(@EmailAddress);

	SELECT [Id]
	FROM [Users]
	WHERE UPPER(EmailAddress) = UPPER(@EmailAddress);

END