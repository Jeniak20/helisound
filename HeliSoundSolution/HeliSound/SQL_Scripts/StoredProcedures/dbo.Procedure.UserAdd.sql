﻿CREATE PROCEDURE [dbo].[UserAdd]

	@FirstName NVARCHAR (64) = '',
	@LastName NVARCHAR (64) = '',
	@EmailAddress NVARCHAR (128) = '',
	@PhoneNumber NVARCHAR (64) = '',
	@Password NVARCHAR (128) = '',
	@RoleId int = 0,
	@Status bit = 0

AS
BEGIN

	INSERT INTO [dbo].[Users]
			   ([FirstName]
			   ,[LastName]
			   ,[EmailAddress]
			   ,[PhoneNumber]
			   ,[Password]
			   ,[RoleId]
			   ,[Status])
		 VALUES
			   (@FirstName
			   ,@LastName
			   ,@EmailAddress
			   ,@PhoneNumber
			   ,@Password
			   ,@RoleId
			   ,@Status);

	SELECT SCOPE_IDENTITY() 'Id';

END
