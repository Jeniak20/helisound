﻿CREATE PROCEDURE [dbo].[SecretAnswerAdd]

	@UserId INT = 0,
	@QuestionId INT = 0,
	@Answer NVARCHAR (MAX) = ''

AS
BEGIN

	INSERT INTO [dbo].[SecretAnswers]
			   ([UserId]
			   ,[QuestionId]
			   ,[Answer])
		 VALUES
			   (@UserId
			   ,@QuestionId
			   ,@Answer);

	SELECT SCOPE_IDENTITY() 'Id';

END
