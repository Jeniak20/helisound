﻿CREATE PROCEDURE [dbo].[UserIdByEmailAddressGet]

	@EmailAddress NVARCHAR(128) = ''

AS
BEGIN

	SELECT Id
	FROM Users
	WHERE UPPER(EmailAddress) = UPPER(@EmailAddress)

END