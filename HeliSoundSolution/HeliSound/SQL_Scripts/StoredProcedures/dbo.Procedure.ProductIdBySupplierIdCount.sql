﻿CREATE PROCEDURE [dbo].[ProductIdBySupplierIdCount]

	@SupplierId INT = 0

AS
BEGIN

	SELECT COUNT([Id])
	FROM [Products]
	WHERE [SupplierId] = @SupplierId

END