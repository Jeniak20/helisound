﻿CREATE PROCEDURE [dbo].[UserSecretAnswerCheck]

	@EmailAddress NVARCHAR(128) = '',
	@Answer NVARCHAR(MAX) = ''

AS
BEGIN

	SELECT sa.Id
	FROM [Users] u 
	INNER JOIN [SecretAnswers] sa ON u.Id = sa.UserId
	INNER JOIN [SecretQuestions] sq ON sa.QuestionId = sq.Id
	WHERE UPPER(u.EmailAddress) = UPPER(@EmailAddress) 
	AND sa.Answer = @Answer

END