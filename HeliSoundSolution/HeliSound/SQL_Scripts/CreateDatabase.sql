---------------------------------------------------------------------------------
--                              CREATE DATABASE
---------------------------------------------------------------------------------

USE [master]
GO

CREATE DATABASE [HeliSound]
GO

ALTER DATABASE [HeliSound]
MODIFY FILE
( NAME = N'HeliSound2', SIZE = 8192KB, MAXSIZE = 81920KB, FILEGROWTH = 1024KB )
GO

ALTER DATABASE [HeliSound]
MODIFY FILE
( NAME = N'HeliSound2_log', SIZE = 8192KB, MAXSIZE = 81920KB, FILEGROWTH = 1024KB )
GO

USE [HeliSound]
GO

---------------------------------------------------------------------------------
--                              CREATE TABLES
---------------------------------------------------------------------------------

CREATE TABLE [dbo].[Categories] (
    [Id]                           INT            IDENTITY (1, 1) NOT NULL,
    [CategoryDescription]          NVARCHAR (64)  NOT NULL,
    [Status]                       BIT            DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([CategoryDescription] ASC)
);
GO

CREATE TABLE [dbo].[InvoiceItems] (
    [Id]							INT				IDENTITY (1, 1) NOT NULL,
    [InvoiceId]						INT				NOT NULL,
    [ProductId]						INT				NOT NULL,
    [Quantity]						INT				DEFAULT ((1)) NOT NULL,
    [RetailPrice]					DECIMAL(18,4)	NOT NULL,
    [Status]						BIT				DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

CREATE TABLE [dbo].[Invoices] (
    [Id]                         INT             IDENTITY (1, 1) NOT NULL,
    [DateCreation]               DATETIME        DEFAULT ((GETDATE())) NOT NULL,
    [UserId]                     INT             NOT NULL,
    [CardTypeId]                 INT             NOT NULL,
    [CardHolderName]             NVARCHAR (128)  NOT NULL,
    [CardNumber]                 NVARCHAR (32)   NOT NULL,
    [Card3Secutity]              NVARCHAR (4)    NOT NULL,
    [CardExpirationMonth]        INT             NOT NULL,
    [CardExpirationYear]         INT             NOT NULL,
    [AddressHouseNumber]         NVARCHAR (16)   NOT NULL,
    [AddressStreetName]          NVARCHAR (128)  NOT NULL,
    [AddressStreetDesignationId] INT             NOT NULL,
    [AddressAppartmentNumber]    NVARCHAR (16)   NULL,
    [AddressCity]                NVARCHAR (64)   NOT NULL,
    [AddressProvince]            NVARCHAR (64)   NOT NULL,
    [AddressPostalCode]          NVARCHAR (8)    NOT NULL,
    [BeforeTax]                  DECIMAL (18, 4) NOT NULL,
    [Tax]                        DECIMAL (18, 4) NOT NULL,
    [Total]                      DECIMAL (18, 4) NOT NULL,
    [ShippingUserId]             INT             NULL,
    [ShippingStatus]             INT             DEFAULT ((0)) NOT NULL,
    [ShippingDate]               DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

CREATE TABLE [dbo].[Products] (
    [Id]							INT				IDENTITY (1, 1) NOT NULL,
    [ProductName]					NVARCHAR (128)	NOT NULL,
    [SupplierId]					INT				NOT NULL,
    [CategoryId]					INT				NOT NULL,
    [RetailPrice]					DECIMAL(18,4)	NOT NULL,
    [Status]						BIT				DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

CREATE TABLE [dbo].[Roles] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [Role]        NVARCHAR (64)   NOT NULL,
    [Description] NVARCHAR (1024) NULL,
    [Status]      BIT             DEFAULT (0) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

CREATE TABLE [dbo].[SecretAnswers]
(
	[Id] INT NOT NULL IDENTITY, 
	[UserId] INT NOT NULL, 
	[QuestionId] INT NOT NULL, 
    [Answer] NVARCHAR(MAX) NOT NULL, 
    CONSTRAINT [PK_SecretAnswers] PRIMARY KEY ([Id]) 
)
GO

CREATE TABLE [dbo].[SecretQuestions]
(
	[Id] INT NOT NULL IDENTITY, 
    [Question] NVARCHAR(MAX) NOT NULL, 
    CONSTRAINT [PK_SecretQuestions] PRIMARY KEY ([Id]) 
)
GO

CREATE TABLE [dbo].[Suppliers] (
    [Id]                           INT            IDENTITY (1, 1) NOT NULL,
    [CompanyName]                  NVARCHAR (64)  NOT NULL,
    [CompanyAddress]                NVARCHAR (256) NOT NULL,
    [ContactPersonName]            NVARCHAR (64)  NOT NULL,
    [ContactPersonTelephoneNumber] NVARCHAR (64)  NOT NULL,
    [Status]                       BIT            DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([CompanyName] ASC)
);
GO

CREATE TABLE [dbo].[Users] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]    NVARCHAR (64)  NULL,
    [LastName]     NVARCHAR (64)  NULL,
    [EmailAddress] NVARCHAR (128) NOT NULL UNIQUE,
    [PhoneNumber]  NVARCHAR (64)  NULL,
    [Password]     NVARCHAR (128) NOT NULL,
    [RoleId]       INT            NOT NULL,
    [Status]       BIT            NOT NULL DEFAULT 0,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([EmailAddress] ASC)
);
GO

---------------------------------------------------------------------------------
--                         CREATE STORED PROCEDURES
---------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[CategoryIdByCategoryDescriptionGet]

	@CategoryDescription NVARCHAR(64) = ''

AS
BEGIN

	SELECT [Id]
	FROM [Categories]
	WHERE UPPER([CategoryDescription]) = UPPER(@CategoryDescription)

END
GO

CREATE PROCEDURE [dbo].[ProductIdByCategoryIdCount]

	@CategoryId INT = 0

AS
BEGIN

	SELECT COUNT([Id])
	FROM [Products]
	WHERE [CategoryId] = @CategoryId

END
GO

CREATE PROCEDURE [dbo].[ProductIdByProductNameSupplierIdCategoryIdGet]

	@ProductName NVARCHAR(128) = '',
	@SupplierId INT = 0,
	@CategoryId INT = 0

AS
BEGIN

	SELECT [Id]
	FROM [Products]
	WHERE UPPER([ProductName]) = UPPER(@ProductName)
	AND [SupplierId] = @SupplierId
	AND [CategoryId] = @CategoryId

END
GO

CREATE PROCEDURE [dbo].[ProductIdBySupplierIdCount]

	@SupplierId INT = 0

AS
BEGIN

	SELECT COUNT([Id])
	FROM [Products]
	WHERE [SupplierId] = @SupplierId

END
GO

CREATE PROCEDURE [dbo].[RoleIdByRoleNameGet]

	@Role NVARCHAR(64) = ''

AS
BEGIN

	SELECT [Id]
	FROM [Roles]
	WHERE UPPER([Role]) = UPPER(@Role)

END
GO

CREATE PROCEDURE [dbo].[SecretAnswerAdd]

	@UserId INT = 0,
	@QuestionId INT = 0,
	@Answer NVARCHAR (MAX) = ''

AS
BEGIN

	INSERT INTO [dbo].[SecretAnswers]
			   ([UserId]
			   ,[QuestionId]
			   ,[Answer])
		 VALUES
			   (@UserId
			   ,@QuestionId
			   ,@Answer);

	SELECT SCOPE_IDENTITY() 'Id';

END
GO

CREATE PROCEDURE [dbo].[SecretQuestionByUserEmailAddressGet]

	@EmailAddress NVARCHAR(128) = ''

AS
BEGIN

	SELECT sq.Question
	FROM [Users] u 
	INNER JOIN [SecretAnswers] sa ON u.Id = sa.UserId
	INNER JOIN [SecretQuestions] sq ON sa.QuestionId = sq.Id
	WHERE UPPER(u.EmailAddress) = UPPER(@EmailAddress)

END
GO

CREATE PROCEDURE [dbo].[SupplierIdByCompanyNameGet]

	@CompanyName NVARCHAR(64) = ''

AS
BEGIN

	SELECT [Id]
	FROM [Suppliers]
	WHERE UPPER([CompanyName]) = UPPER(@CompanyName)

END
GO

CREATE PROCEDURE [dbo].[UserAdd]

	@FirstName NVARCHAR (64) = '',
	@LastName NVARCHAR (64) = '',
	@EmailAddress NVARCHAR (128) = '',
	@PhoneNumber NVARCHAR (64) = '',
	@Password NVARCHAR (128) = '',
	@RoleId int = 0,
	@Status bit = 0

AS
BEGIN

	INSERT INTO [dbo].[Users]
			   ([FirstName]
			   ,[LastName]
			   ,[EmailAddress]
			   ,[PhoneNumber]
			   ,[Password]
			   ,[RoleId]
			   ,[Status])
		 VALUES
			   (@FirstName
			   ,@LastName
			   ,@EmailAddress
			   ,@PhoneNumber
			   ,@Password
			   ,@RoleId
			   ,@Status);

	SELECT SCOPE_IDENTITY() 'Id';

END
GO

CREATE PROCEDURE [dbo].[UserByEmailAddressGet]

	@EmailAddress NVARCHAR(128) = ''

AS
BEGIN

	SELECT u.Id, u.FirstName, u.LastName, u.EmailAddress, u.PhoneNumber, u.RoleId, u.[Status], r.[Role]
	FROM Users u INNER JOIN Roles r ON u.RoleId = r.Id
	WHERE UPPER(u.EmailAddress) = UPPER(@EmailAddress) AND u.[Status] = 1

END
GO

CREATE PROCEDURE [dbo].[UserIdByEmailAddressGet]

	@EmailAddress NVARCHAR(128) = ''

AS
BEGIN

	SELECT Id
	FROM Users
	WHERE UPPER(EmailAddress) = UPPER(@EmailAddress)

END
GO

CREATE PROCEDURE [dbo].[UserIdentityCheck]

	@EmailAddress NVARCHAR(128) = '',
	@Password NVARCHAR(128) = ''

AS
BEGIN

	SELECT u.Id, u.FirstName, u.LastName, u.EmailAddress, u.PhoneNumber, u.RoleId, u.[Status], r.[Role]
	FROM Users u INNER JOIN Roles r ON u.RoleId = r.Id
	WHERE UPPER(u.EmailAddress) = UPPER(@EmailAddress) AND u.[Password] = @Password AND u.[Status] = 1

END
GO

CREATE PROCEDURE [dbo].[UserPasswordUpdate]

	@EmailAddress NVARCHAR(128) = '',
	@Password NVARCHAR(128) = ''

AS
BEGIN

	UPDATE [dbo].[Users]
	SET [Password] = ISNULL(@Password, [Password])
	WHERE UPPER(EmailAddress) = UPPER(@EmailAddress);

	SELECT [Id]
	FROM [Users]
	WHERE UPPER(EmailAddress) = UPPER(@EmailAddress);

END
GO

CREATE PROCEDURE [dbo].[UserSecretAnswerCheck]

	@EmailAddress NVARCHAR(128) = '',
	@Answer NVARCHAR(MAX) = ''

AS
BEGIN

	SELECT sa.Id
	FROM [Users] u 
	INNER JOIN [SecretAnswers] sa ON u.Id = sa.UserId
	INNER JOIN [SecretQuestions] sq ON sa.QuestionId = sq.Id
	WHERE UPPER(u.EmailAddress) = UPPER(@EmailAddress) 
	AND sa.Answer = @Answer

END
GO

---------------------------------------------------------------------------------
--                         ADD INITIAL AND TEST DATA
---------------------------------------------------------------------------------


SET IDENTITY_INSERT [dbo].[Categories] ON
INSERT INTO [dbo].[Categories] ([Id], [CategoryDescription], [Status]) VALUES (1, N'Televisions', 1)
INSERT INTO [dbo].[Categories] ([Id], [CategoryDescription], [Status]) VALUES (2, N'Home Theatres', 1)
INSERT INTO [dbo].[Categories] ([Id], [CategoryDescription], [Status]) VALUES (3, N'Speakers', 1)
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO

SET IDENTITY_INSERT [dbo].[Products] ON
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (1, N'Sony 48" 1080p LED Linux Smart TV (KDL48W650D)', 1, 1, CAST(749.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (2, N'Sony 60" 1080p LED TV (KDL60W630B/2)', 1, 1, CAST(1099.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (3, N'Sony 75" 4K UHD HDR LED Android Smart TV (XBR75X850D)', 1, 1, CAST(4799.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (4, N'Elite x2 1012 G1 Tablet PC - 12&quot; - In-plane Switching (IPS) Technology, BrightView - Wireless LAN - Intel Core M m3-6Y30', 1, 2, CAST(1409.0000 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (5, N'Elite x2 1012 G1 Tablet PC - 12&quot; - In-plane Switching (IPS) Technology, BrightView - Wireless LAN - Intel Core M m5-6Y57', 1, 2, CAST(2137.0000 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (6, N'ZBook Studio G3 15.6&quot; (In-plane Switching (IPS) Technology) Mobile Workstation Ultrabook - Intel Core i7 (6th Gen) i7-67', 1, 2, CAST(3162.0000 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (7, N'CS-N755 Mini Hi-Fi Network System', 1, 3, CAST(649.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (8, N'Mini CD HiFi System with Bluetooth', 1, 3, CAST(129.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (9, N'XC-HM82-K Bluetooth Wi-Fi CD Receiver', 1, 3, CAST(499.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (10, N'55" 4K Ultra HD Curved LED Tizen Smart TV (UN55KU6500FXZC) - Black', 2, 1, CAST(1799.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (11, N'50" 1080p LED Smart Hub Smart TV (UN50J5200AFXZC)', 2, 1, CAST(799.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (12, N'65" 4K Ultra HD Curved LED Tizen Smart TV (UN65KU6500FXZC) - Black', 2, 1, CAST(2599.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (13, N'Aspire E 14" Touchscreen Laptop - Black/Iron (Intel Ci5-5200U / 1TB HDD / 8GB RAM / Windows 10)', 2, 2, CAST(599.9500 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (14, N'Aspire ES 15.6" Laptop - Black (Intel Celeron 2840 / 320GB HDD / 2GB RAM / Windows 8.1)', 2, 2, CAST(349.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (15, N'Aspire V Nitro 15.6" Laptop -Black (Intel Core i7-6700HQ/1TB HDD/256GB SSD/16GB RAM/Win 10)-Eng', 2, 2, CAST(1582.9800 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (16, N'HT-J4530 500-Watt 5.1-Channel Blu-ray & DVD Home Theatre System', 2, 3, CAST(299.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (17, N'BDVE3100 1000-Watt 5.1 Channel 3D Blu-ray Home Theatre System', 2, 3, CAST(399.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (18, N'SoundTouch 520 Home Theatre Speaker System', 2, 3, CAST(1649.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (19, N'40" 1080p HD LED TV (NS-40D510NA17) - Black - Only at Best Buy', 3, 1, CAST(299.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (20, N'32" 720p HD LED Smart TV (NS-32DR310CA17) - Only at Best Buy', 3, 1, CAST(219.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (21, N'50" 4K UHD LED Roku Smart TV (NS-50DR710CA17) - Only at Best Buy', 3, 1, CAST(599.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (22, N'X456UV 14" Laptop - Dark Brown (Intel Dual-Core i7-6500U/1TB HDD/12GB RAM/Windows 10)', 3, 2, CAST(1049.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (23, N'15.6" Laptop - Black/Gold (Intel Core i5-5200U/1TB HDD/8GB RAM/Windows 10)', 3, 2, CAST(749.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (24, N'Zenbook UX303 13.3" Touch Laptop-Brown (Intel Core i5-6200U/256GB SSD/8GB RAM)-Eng-Open Box', 3, 2, CAST(1242.9700 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (25, N'CS-265 Mini CD Hi-Fi System with Bluetooth - Black', 3, 3, CAST(649.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (26, N'Sony HT-XT2 35-Watt 2.1 Channel Sound Base', 3, 3, CAST(399.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[Products] ([Id], [ProductName], [SupplierId], [CategoryId], [RetailPrice], [Status]) VALUES (27, N'CS-265 Mini CD Hi-Fi System with Bluetooth - Red', 3, 3, CAST(649.9900 AS Decimal(18, 4)), 1)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO

SET IDENTITY_INSERT [dbo].[Roles] ON
INSERT INTO [dbo].[Roles] ([Id], [Role], [Description], [Status]) VALUES (1, N'Administrators', N'This role allows logged in users to access pages in Administration scope', 1)
INSERT INTO [dbo].[Roles] ([Id], [Role], [Description], [Status]) VALUES (2, N'Shipping', N'This role allows logged in users to access pages in Shipping scope', 1)
INSERT INTO [dbo].[Roles] ([Id], [Role], [Description], [Status]) VALUES (3, N'Customers', N'This role allows logged in users to access pages in Customers scope', 1)
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO

SET IDENTITY_INSERT [dbo].[SecretAnswers] ON
INSERT INTO [dbo].[SecretAnswers] ([Id], [UserId], [QuestionId], [Answer]) VALUES (1, 1, 4, N'64B080EC54462510ECB373067DF486F0B4AFD725')
INSERT INTO [dbo].[SecretAnswers] ([Id], [UserId], [QuestionId], [Answer]) VALUES (3, 2, 3, N'64B080EC54462510ECB373067DF486F0B4AFD725')
INSERT INTO [dbo].[SecretAnswers] ([Id], [UserId], [QuestionId], [Answer]) VALUES (5, 3, 5, N'64B080EC54462510ECB373067DF486F0B4AFD725')
SET IDENTITY_INSERT [dbo].[SecretAnswers] OFF
GO

SET IDENTITY_INSERT [dbo].[SecretQuestions] ON
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (1, N'What was your childhood nickname?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (2, N'What is the name of your favorite childhood friend?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (3, N'In what city or town did your mother and father meet?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (4, N'What is the middle name of your oldest child?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (5, N'What is your favorite team?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (6, N'What is your favorite movie?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (7, N'What was your favorite sport in high school?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (8, N'What was your favorite food as a child?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (9, N'What is the first name of the boy or girl that you first kissed?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (10, N'What was the make and model of your first car?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (11, N'What was the name of the hospital where you were born?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (12, N'Who is your childhood sports hero?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (13, N'What school did you attend for sixth grade?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (14, N'What was the last name of your third grade teacher?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (15, N'In what town was your first job?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (16, N'What was the name of the company where you had your first job?')
SET IDENTITY_INSERT [dbo].[SecretQuestions] OFF
GO

SET IDENTITY_INSERT [dbo].[Suppliers] ON
INSERT INTO [dbo].[Suppliers] ([Id], [CompanyName], [CompanyAddress], [ContactPersonName], [ContactPersonTelephoneNumber], [Status]) VALUES (1, N'Sony', N'Sony Str, Sony Tow', N'Sony Man', N'(111) 111-1111', 1)
INSERT INTO [dbo].[Suppliers] ([Id], [CompanyName], [CompanyAddress], [ContactPersonName], [ContactPersonTelephoneNumber], [Status]) VALUES (2, N'RCA', N'RCA Ave, RCA City', N'RCA Man', N'(222) 222-2222', 1)
INSERT INTO [dbo].[Suppliers] ([Id], [CompanyName], [CompanyAddress], [ContactPersonName], [ContactPersonTelephoneNumber], [Status]) VALUES (3, N'Pioneer', N'Pioneer Rd, Pioneer Villee', N'Pioneer Man', N'(333) 333-3333', 1)
SET IDENTITY_INSERT [dbo].[Suppliers] OFF
GO

SET IDENTITY_INSERT [dbo].[Users] ON
INSERT INTO [dbo].[Users] ([Id], [FirstName], [LastName], [EmailAddress], [PhoneNumber], [Password], [RoleId], [Status]) VALUES (1, N'Admin', N'Smith', N'admin.smith@helisound.com', N'(647) 888-7777', N'5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8', 1, 1)
INSERT INTO [dbo].[Users] ([Id], [FirstName], [LastName], [EmailAddress], [PhoneNumber], [Password], [RoleId], [Status]) VALUES (2, N'Shipping', N'Brown', N'shipping.brown@helisound.com', N'(647) 888-6666', N'5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8', 2, 1)
INSERT INTO [dbo].[Users] ([Id], [FirstName], [LastName], [EmailAddress], [PhoneNumber], [Password], [RoleId], [Status]) VALUES (3, N'Customer', N'Jones', N'customer.jones@gmail.com', N'(647) 456-1234', N'5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8', 3, 1)
SET IDENTITY_INSERT [dbo].[Users] OFF
GO

SET IDENTITY_INSERT [dbo].[Invoices] ON
INSERT INTO [dbo].[Invoices] ([Id], [DateCreation], [UserId], [CardTypeId], [CardHolderName], [CardNumber], [Card3Secutity], [CardExpirationMonth], [CardExpirationYear], [AddressHouseNumber], [AddressStreetName], [AddressStreetDesignationId], [AddressAppartmentNumber], [AddressCity], [AddressProvince], [AddressPostalCode], [BeforeTax], [Tax], [Total], [ShippingUserId], [ShippingStatus], [ShippingDate]) VALUES (1, N'2017-07-17 17:57:19', 3, 1, N'Customer Jones', N'1111222233334444', N'123', 5, 2020, N'5840', N'Yonge', 1, NULL, N'North York', N'Ontario', N'M2M 3T3', CAST(1099.9900 AS Decimal(18, 4)), CAST(142.9987 AS Decimal(18, 4)), CAST(1242.9887 AS Decimal(18, 4)), 2, 1, N'2017-07-19 14:57:19')
INSERT INTO [dbo].[Invoices] ([Id], [DateCreation], [UserId], [CardTypeId], [CardHolderName], [CardNumber], [Card3Secutity], [CardExpirationMonth], [CardExpirationYear], [AddressHouseNumber], [AddressStreetName], [AddressStreetDesignationId], [AddressAppartmentNumber], [AddressCity], [AddressProvince], [AddressPostalCode], [BeforeTax], [Tax], [Total], [ShippingUserId], [ShippingStatus], [ShippingDate]) VALUES (2, N'2017-07-31 16:57:52', 3, 2, N'Customer Jones', N'5555999955559999', N'456', 6, 2021, N'80', N'Antibes', 5, N'904', N'North York', N'Ontario', N'M2R 3K5', CAST(129.9900 AS Decimal(18, 4)), CAST(16.8987 AS Decimal(18, 4)), CAST(146.8887 AS Decimal(18, 4)), NULL, 0, NULL)
INSERT INTO [dbo].[Invoices] ([Id], [DateCreation], [UserId], [CardTypeId], [CardHolderName], [CardNumber], [Card3Secutity], [CardExpirationMonth], [CardExpirationYear], [AddressHouseNumber], [AddressStreetName], [AddressStreetDesignationId], [AddressAppartmentNumber], [AddressCity], [AddressProvince], [AddressPostalCode], [BeforeTax], [Tax], [Total], [ShippingUserId], [ShippingStatus], [ShippingDate]) VALUES (3, N'2017-08-09 13:58:23', 3, 3, N'Customer Jones', N'1010101010101010', N'789', 7, 2022, N'80', N'Antibes', 5, N'904', N'North York', N'Ontario', N'M2R 3K5', CAST(1582.9800 AS Decimal(18, 4)), CAST(205.7874 AS Decimal(18, 4)), CAST(1788.7674 AS Decimal(18, 4)), NULL, 0, NULL)
INSERT INTO [dbo].[Invoices] ([Id], [DateCreation], [UserId], [CardTypeId], [CardHolderName], [CardNumber], [Card3Secutity], [CardExpirationMonth], [CardExpirationYear], [AddressHouseNumber], [AddressStreetName], [AddressStreetDesignationId], [AddressAppartmentNumber], [AddressCity], [AddressProvince], [AddressPostalCode], [BeforeTax], [Tax], [Total], [ShippingUserId], [ShippingStatus], [ShippingDate]) VALUES (4, N'2017-08-19 21:58:51', 3, 1, N'Customer Jones', N'1111222233334444', N'123', 5, 2020, N'5840', N'Yonge', 1, NULL, N'North York', N'Ontario', N'M2M 3T3', CAST(1049.9900 AS Decimal(18, 4)), CAST(136.4987 AS Decimal(18, 4)), CAST(1186.4887 AS Decimal(18, 4)), NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[Invoices] OFF
GO

SET IDENTITY_INSERT [dbo].[InvoiceItems] ON
INSERT INTO [dbo].[InvoiceItems] ([Id], [InvoiceId], [ProductId], [Quantity], [RetailPrice], [Status]) VALUES (1, 1, 2, 1, CAST(1099.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[InvoiceItems] ([Id], [InvoiceId], [ProductId], [Quantity], [RetailPrice], [Status]) VALUES (2, 2, 8, 1, CAST(129.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[InvoiceItems] ([Id], [InvoiceId], [ProductId], [Quantity], [RetailPrice], [Status]) VALUES (3, 3, 15, 1, CAST(1582.9800 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[InvoiceItems] ([Id], [InvoiceId], [ProductId], [Quantity], [RetailPrice], [Status]) VALUES (4, 4, 22, 1, CAST(1049.9900 AS Decimal(18, 4)), 1)
SET IDENTITY_INSERT [dbo].[InvoiceItems] OFF
GO

---------------------------------------------------------------------------------
--                               END OF SCRIPT
---------------------------------------------------------------------------------
