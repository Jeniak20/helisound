﻿SET IDENTITY_INSERT [dbo].[SecretQuestions] ON
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (1, N'What was your childhood nickname?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (2, N'What is the name of your favorite childhood friend?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (3, N'In what city or town did your mother and father meet?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (4, N'What is the middle name of your oldest child?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (5, N'What is your favorite team?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (6, N'What is your favorite movie?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (7, N'What was your favorite sport in high school?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (8, N'What was your favorite food as a child?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (9, N'What is the first name of the boy or girl that you first kissed?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (10, N'What was the make and model of your first car?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (11, N'What was the name of the hospital where you were born?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (12, N'Who is your childhood sports hero?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (13, N'What school did you attend for sixth grade?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (14, N'What was the last name of your third grade teacher?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (15, N'In what town was your first job?')
INSERT INTO [dbo].[SecretQuestions] ([Id], [Question]) VALUES (16, N'What was the name of the company where you had your first job?')
SET IDENTITY_INSERT [dbo].[SecretQuestions] OFF
