SET IDENTITY_INSERT [dbo].[Roles] ON
INSERT INTO [dbo].[Roles] ([Id], [Role], [Description], [Status]) VALUES (1, N'Administrators', N'This role allows logged in users to access pages in Administration scope', 1)
INSERT INTO [dbo].[Roles] ([Id], [Role], [Description], [Status]) VALUES (2, N'Shipping', N'This role allows logged in users to access pages in Shipping scope', 1)
INSERT INTO [dbo].[Roles] ([Id], [Role], [Description], [Status]) VALUES (3, N'Customers', N'This role allows logged in users to access pages in Customers scope', 1)
SET IDENTITY_INSERT [dbo].[Roles] OFF
