﻿SET IDENTITY_INSERT [dbo].[Suppliers] ON
INSERT INTO [dbo].[Suppliers] ([Id], [CompanyName], [CompanyAddress], [ContactPersonName], [ContactPersonTelephoneNumber], [Status]) VALUES (1, N'Sony', N'Sony Str, Sony Tow', N'Sony Man', N'(111) 111-1111', 1)
INSERT INTO [dbo].[Suppliers] ([Id], [CompanyName], [CompanyAddress], [ContactPersonName], [ContactPersonTelephoneNumber], [Status]) VALUES (2, N'RCA', N'RCA Ave, RCA City', N'RCA Man', N'(222) 222-2222', 1)
INSERT INTO [dbo].[Suppliers] ([Id], [CompanyName], [CompanyAddress], [ContactPersonName], [ContactPersonTelephoneNumber], [Status]) VALUES (3, N'Pioneer', N'Pioneer Rd, Pioneer Villee', N'Pioneer Man', N'(333) 333-3333', 1)
SET IDENTITY_INSERT [dbo].[Suppliers] OFF
