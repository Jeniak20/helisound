SET IDENTITY_INSERT [dbo].[Users] ON
INSERT INTO [dbo].[Users] ([Id], [FirstName], [LastName], [EmailAddress], [PhoneNumber], [Password], [RoleId], [Status]) VALUES (1, N'Admin', N'Smith', N'admin.smith@helisound.com', N'(647) 888-7777', N'5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8', 1, 1)
INSERT INTO [dbo].[Users] ([Id], [FirstName], [LastName], [EmailAddress], [PhoneNumber], [Password], [RoleId], [Status]) VALUES (2, N'Shipping', N'Brown', N'shipping.brown@helisound.com', N'(647) 888-6666', N'5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8', 2, 1)
INSERT INTO [dbo].[Users] ([Id], [FirstName], [LastName], [EmailAddress], [PhoneNumber], [Password], [RoleId], [Status]) VALUES (3, N'Customer', N'Jones', N'customer.jones@gmail.com', N'(647) 456-1234', N'5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8', 3, 1)
SET IDENTITY_INSERT [dbo].[Users] OFF
