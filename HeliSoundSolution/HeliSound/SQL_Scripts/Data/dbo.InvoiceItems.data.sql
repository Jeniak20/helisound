﻿SET IDENTITY_INSERT [dbo].[InvoiceItems] ON
INSERT INTO [dbo].[InvoiceItems] ([Id], [InvoiceId], [ProductId], [Quantity], [RetailPrice], [Status]) VALUES (1, 1, 2, 1, CAST(1099.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[InvoiceItems] ([Id], [InvoiceId], [ProductId], [Quantity], [RetailPrice], [Status]) VALUES (2, 2, 8, 1, CAST(129.9900 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[InvoiceItems] ([Id], [InvoiceId], [ProductId], [Quantity], [RetailPrice], [Status]) VALUES (3, 3, 15, 1, CAST(1582.9800 AS Decimal(18, 4)), 1)
INSERT INTO [dbo].[InvoiceItems] ([Id], [InvoiceId], [ProductId], [Quantity], [RetailPrice], [Status]) VALUES (4, 4, 22, 1, CAST(1049.9900 AS Decimal(18, 4)), 1)
SET IDENTITY_INSERT [dbo].[InvoiceItems] OFF
