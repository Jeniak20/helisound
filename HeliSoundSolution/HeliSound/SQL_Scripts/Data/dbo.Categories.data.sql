﻿SET IDENTITY_INSERT [dbo].[Categories] ON
INSERT INTO [dbo].[Categories] ([Id], [CategoryDescription], [Status]) VALUES (1, N'Televisions', 1)
INSERT INTO [dbo].[Categories] ([Id], [CategoryDescription], [Status]) VALUES (2, N'Home Theatres', 1)
INSERT INTO [dbo].[Categories] ([Id], [CategoryDescription], [Status]) VALUES (3, N'Speakers', 1)
SET IDENTITY_INSERT [dbo].[Categories] OFF
