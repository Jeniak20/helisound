# ReadMe #

## About ##

This is the final assignment project for **"Design Web Application"** course of the **Gorge Brown College**.

### Used technologies, frameworks and software: ###

* Visual Studio Community 2017
* ASP.NET with C#
* .NET Framework 4.5.2
* SQL Server 2016 SP1 Express

### Solution: ### 

* HeliSoudSolution

### Projects: ###

* HeliSound (ASP.NET with C#)

	
## Projectinformation ##

Following pages on the project contains comments visible for the users:

* Login
* Order Product (customer page)
* Orders (administrator page)
* Roles (administrator page)


## Database information ##

**HeliSound** project includes database file in the **App_data** folder, so it can work as it is in Visual Studio without using any external SQL servers. However you can copy that **HeliSound.mdf** (and **HeliSound_log.ldf**) files, place them in appropriate folder, then **attach** database to SQL server (SQL 2016 is required) and change Web.config connection string to refer to the appropriate SQLserver-database. 

Alternatively you can create a database from the scratch using provided **CreateDatabase.sql** script. This script will create database with name HeliSound, create all necessary tables and stored procedures and populate tables with initial test data.

Example of the connection strings in Web.config:

* To local database files in project's App_data folder:

```
#!config
<connectionStrings>
	<add name="HeliSound" 
		connectionString="Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|HeliSound.mdf;Integrated Security=True"
		providerName="System.Data.SqlClient" />
</connectionStrings>
```

* To the database in external SQL server :

```
#!config
<connectionStrings>
	<add name="HeliSound" 
		connectionString="Data Source=MAIN;Initial Catalog=HeliSound;Integrated Security=True"
		providerName="System.Data.SqlClient" />
</connectionStrings>
```
	